<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="https://upload.wikimedia.org/wikipedia/commons/thumb/7/79/Aktenzeichen_XY_Logo_2014.svg/1024px-Aktenzeichen_XY_Logo_2014.svg.png" type="image/ico" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>ຮ້ານ: ຂາຍເຄື່ອງໄຊໂຢ</title>
 <!-- data table -->
 <link rel="stylesheet" href="{{ asset('Backend/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
 <link rel="stylesheet" href="{{ asset('Backend/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
 <link rel="stylesheet" href="{{ asset('Backend/plugins/datatables-buttons/css/buttons.bootstrap4.min.css') }}">
    <!-- Bootstrap -->
    <link href="Frontend/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="Frontend/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    {{-- <link rel="stylesheet" href="{{ asset('Backend/plugins/fontawesome-free/css/all.min.css"> --}}

    <!-- NProgress -->
    <link href="Frontend/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="Frontend/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- summernote -->
    <link rel="stylesheet" href="{{ asset('Backend/plugins/summernote/summernote-bs4.min.css') }}">
    <!-- Toastr -->
    <link rel="stylesheet" href="{{ asset('Backend/plugins/toastr/toastr.min.css') }}">
    <!-- sweetalert2 -->
    <link rel="stylesheet" href="{{ asset('Backend/plugins/sweetalert2/sweetalert2.css') }}">
    <link rel="stylesheet" href="{{ asset('Backend/plugins/sweetalert2/sweetalert2.min.css') }}">
    <!-- Select2 -->
    <link rel="stylesheet" href="{{ asset('Backend/plugins/flag-icon-css/css/flag-icon.min.css') }}">
    <link rel="stylesheet" href="{{ asset('Backend/plugins/select2/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('Backend/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">
    <!-- bootstrap-progressbar -->
    <link href="Frontend/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
    <!-- JQVMap -->
    <link href="Frontend/jqvmap/dist/jqvmap.min.css" rel="stylesheet">
    <!-- bootstrap-daterangepicker -->
    <link href="Frontend/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('Backend/css/style.css') }}" />
    <!-- Custom Theme Style -->
    <link href="Frontend/build/css/custom.min.css" rel="stylesheet">
    <style>
        nav svg {
            height: 20px;
        }

        @font-face {
            font-family: Lao_Modern2;
            src: url('{{ asset('fonts/Lao_Modern2.ttf') }}');
        }
    </style>
    @livewireStyles
</head>
{{-- ==================================================================== --}}

<body class="nav-md" style="font-family: 'Lao_Modern2'">
    <div class="container body">
        <div class="main_container">
            @include('layouts.backend.leftmenu')
            @include('layouts.backend.topmenu')
                {{ $slot }}
            <!-- footer content -->
            <footer class="main-footer">
                <div class="pull-right">
                    ອ້າງອີງຈາກເວບໄຊ <a href="https://colorlib.com">( Colorlib )</a> ສືບຕໍ່ພັດທະນາໂດຍນັກສຶກສາ ຄວທ ມະຫາວິທະຍາໄລແຫ່ງຊາດລາວ 2022 - 2023
                </div>
                <div class="clearfix"></div>
            </footer>
            <!-- /footer content -->
        </div>
    </div>
    {{-- ==================================================================== --}}
    <!-- jQuery -->
    <script src="Frontend/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="Frontend/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
    <!-- FastClick -->
    <script src="Frontend/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="Frontend/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="Frontend/Chart.js/dist/Chart.min.js"></script>
    <!-- gauge.js -->
    <script src="Frontend/gauge.js/dist/gauge.min.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="Frontend/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- iCheck -->
    <script src="Frontend/iCheck/icheck.min.js"></script>
    <!-- Skycons -->
    <script src="Frontend/skycons/skycons.js"></script>
    <!-- Flot -->
    <script src="Frontend/Flot/jquery.flot.js"></script>
    <script src="Frontend/Flot/jquery.flot.pie.js"></script>
    <script src="Frontend/Flot/jquery.flot.time.js"></script>
    <script src="Frontend/Flot/jquery.flot.stack.js"></script>
    <script src="Frontend/Flot/jquery.flot.resize.js"></script>
    <!-- Flot plugins -->
    <script src="Frontend/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="Frontend/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="Frontend/flot.curvedlines/curvedLines.js"></script>
    <!-- DateJS -->
    <script src="Frontend/DateJS/build/date.js"></script>
    <!-- JQVMap -->
    <script src="Frontend/jqvmap/dist/jquery.vmap.js"></script>
    <script src="Frontend/jqvmap/dist/maps/jquery.vmap.world.js"></script>
    <script src="Frontend/jqvmap/examples/js/jquery.vmap.sampledata.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="Frontend/moment/min/moment.min.js"></script>
    <script src="Frontend/bootstrap-daterangepicker/daterangepicker.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="Frontend/build/js/custom.min.js"></script>
        <!-- Toastr -->
        <script src="Backend/plugins/toastr/toastr.min.js"></script>
    <!-- SweetAlert2 -->
    <script src="Backend/plugins/sweetalert2/sweetalert2.all.js"></script>
    <script src="Backend/plugins/sweetalert2/sweetalert2.all.min.js"></script>
    <!-- Select2 -->
    <script src="Backend/plugins/select2/js/select2.full.min.js"></script>
     <!-- DataTables  & Plugins -->
     <script src="Backend/plugins/datatables/jquery.dataTables.min.js"></script>
     <script src="Backend/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
     <script src="Backend/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
     <script src="Backend/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
     <script src="Backend/plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
     <script src="Backend/plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
    <!-- Summernote -->
    <script src="Backend/plugins/summernote/summernote-bs4.min.js"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    @livewireScripts
    @include('layouts.backend.script')
    @stack('scripts')
</body>

</html>

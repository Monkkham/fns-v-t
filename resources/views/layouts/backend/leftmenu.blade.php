<div class="col-md-3 left_col">
    <div class="left_col scroll-view">
        <div class="navbar nav_title" style="border: 0;">
            <ul class="nav">
                <li class="nav-item text-white">
                    <img height="50"
                        src="https://upload.wikimedia.org/wikipedia/commons/thumb/7/79/Aktenzeichen_XY_Logo_2014.svg/1024px-Aktenzeichen_XY_Logo_2014.svg.png"
                        class="rounded-circle" alt="..."> ຮ້ານ: ຂາຍເຄື່ອງໄຊໂຢ
                </li>
            </ul>
        </div>
        <div class="clearfix"></div>
        <!-- sidebar menu -->
        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
            <div class="menu_section">
                <ul class="nav side-menu">
                    <li><a href="{{ route('dashboard') }}"><i class="fa fa-home"></i> ຫນ້າຫຼັກ </a></li>
                    <li><a><i class="fa fa-database"></i> ຈັດການຂໍ້ມູນ <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            @foreach ($rolepermissions as $items)
                                @if ($items->permissionname->name == 'action_1')
                                    <li><a href="{{ route('backend.employee') }}">ພະນັກງານ</a></li>
                                @endif
                            @endforeach
                            @foreach ($rolepermissions as $items)
                                @if ($items->permissionname->name == 'action_2')
                                    <li><a href="{{ route('backend.customer') }}">ລູກຄ້າ</a></li>
                                @endif
                            @endforeach
                            @foreach ($rolepermissions as $items)
                                @if ($items->permissionname->name == 'action_3')
                                    <li><a href="{{ route('backend.customerType') }}">ປະເພດລູກຄ້າ</a></li>
                                @endif
                            @endforeach
                            @foreach ($rolepermissions as $items)
                                @if ($items->permissionname->name == 'action_4')
                                    <li><a href="{{ route('backend.supplier') }}">ຜູ້ສະຫນອງ</a></li>
                                @endif
                            @endforeach
                            @foreach ($rolepermissions as $items)
                                @if ($items->permissionname->name == 'action_5')
                                    <li><a href="{{ route('backend.product') }}">ສິນຄ້າ</a></li>
                                @endif
                            @endforeach
                            @foreach ($rolepermissions as $items)
                                @if ($items->permissionname->name == 'action_6')
                                    <li><a href="{{ route('backend.productType') }}">ປະເພດສິນຄ້າ</a></li>
                                @endif
                            @endforeach
                            @foreach ($rolepermissions as $items)
                                @if ($items->permissionname->name == 'action_7')
                                    <li><a href="{{ route('backend.Unit') }}">ຫົວຫນ່ວຍ</a></li>
                                @endif
                            @endforeach
                            @foreach ($rolepermissions as $items)
                                @if ($items->permissionname->name == 'action_8')
                                    <li><a href="{{ route('backend.coupon') }}">ສ່ວນຫຼດ</a></li>
                                @endif
                            @endforeach
                            {{-- <li><a href="{{ route('backend.tax') }}">ອາກອນ</a></li> --}}
                            @foreach ($rolepermissions as $items)
                                @if ($items->permissionname->name == 'action_9')
                                    <li><a href="{{ route('backend.notebook') }}">ບັນທຶກປະຈຳວັນ</a></li>
                                @endif
                            @endforeach
                        </ul>
                    </li>
                    @foreach ($rolepermissions as $items)
                        @if ($items->permissionname->name == 'action_11')
                            <li><a href="{{ route('backend.orderProducts') }}"><i class="fa fa-cart-plus"></i>
                                    ສັ່ງຊື້ສິນຄ້າ
                                </a></li>
                        @endif
                    @endforeach
                    @foreach ($rolepermissions as $items)
                        @if ($items->permissionname->name == 'action_12')
                            <li><a href="{{ route('backend.icomeProducts') }}"><i class="fa fa-cart-arrow-down"></i>
                                    ນຳເຂົາສິນຄ້າ </a></li>
                        @endif
                    @endforeach
                    @foreach ($rolepermissions as $items)
                        @if ($items->permissionname->name == 'action_13')
                            <li><a href="{{ route('backend.saleProducts') }}"><i class="fa fa-home"></i> ຂາຍຫນ້າຮ້ານ
                                </a></li>
                        @endif
                    @endforeach
                    @foreach ($rolepermissions as $items)
                        @if ($items->permissionname->name == 'action_14')
                            <li><a href="{{ route('backend.salePendings') }}"><i class="fa fa-list"></i>
                                    ລາຍການຂາຍອອນລາຍ </a>
                            </li>
                        @endif
                    @endforeach
                    <li><a><i class="fa fa-bar-chart-o"></i> ລາຍງານ <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            @foreach ($rolepermissions as $items)
                                @if ($items->permissionname->name == 'action_15')
                                    <li><a href="{{ route('backend.report-order') }}">ລາຍງານການສັ່ງຊື້</a></li>
                                @endif
                            @endforeach
                            @foreach ($rolepermissions as $items)
                                @if ($items->permissionname->name == 'action_16')
                                    <li><a href="{{ route('backend.report-sale') }}">ລາຍງານການຂາຍ</a></li>
                                @endif
                            @endforeach
                            @foreach ($rolepermissions as $items)
                                @if ($items->permissionname->name == 'action_17')
                                    <li><a href="{{ route('backend.report-product') }}">ລາຍງານຂໍ້ມູນສິນຄ້າ</a></li>
                                @endif
                            @endforeach
                            @foreach ($rolepermissions as $items)
                                @if ($items->permissionname->name == 'action_18')
                                    <li><a href="{{ route('backend.report-income') }}">ລາຍງານລາຍຮັບ</a></li>
                                @endif
                            @endforeach
                            @foreach ($rolepermissions as $items)
                                @if ($items->permissionname->name == 'action_19')
                                    <li><a href="{{ route('backend.report-expend') }}">ລາຍງານລາຍຈ່າຍ</a></li>
                                @endif
                            @endforeach
                        </ul>
                    </li>
                    <li><a><i class="fa fa-users"></i> ຜູ້ໃຊ້ ເເລະ ສິດທິ <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            @foreach ($rolepermissions as $items)
                                @if ($items->permissionname->name == 'action_20')
                                    <li><a href="{{ route('backend.user') }}">ຜູ້ໃຊ້</a></li>
                                @endif
                            @endforeach
                            @foreach ($rolepermissions as $items)
                                @if ($items->permissionname->name == 'action_21')
                                    <li><a href="{{ route('backend.role') }}">ສິດທິ</a></li>
                                @endif
                            @endforeach
                        </ul>
                    </li>
                    <li><a><i class="fa fa-table"></i> ຂໍ້ມູນເວບໄຊ <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            @foreach ($rolepermissions as $items)
                                @if ($items->permissionname->name == 'action_22')
                                    <li><a href="{{ route('backend.slide') }}">ສະໄລຮູບພາບ</a></li>
                                @endif
                            @endforeach
                            @foreach ($rolepermissions as $items)
                                @if ($items->permissionname->name == 'action_23')
                                    <li><a href="{{ route('backend.post') }}">ໂພດສາທາລະນະ</a></li>
                                @endif
                            @endforeach
                        </ul>
                    </li>
                    @foreach ($rolepermissions as $items)
                        @if ($items->permissionname->name == 'action_24')
                            <li><a href="{{ route('about-company') }}"><i class="fa fa-building"></i> ຂໍ້ມູນກ່ຽວກັບຮ້ານ
                                </a>
                            </li>
                        @endif
                    @endforeach
                </ul>
            </div>
        </div>
        <!-- /sidebar menu -->

        <!-- /menu footer buttons -->
        <div class="sidebar-footer hidden-small">
            <a data-toggle="tooltip" data-placement="top" title="Settings">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="Lock">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="Logout" href="login.html">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
            </a>
        </div>
        <!-- /menu footer buttons -->
    </div>
</div>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>ເຂົ້າສູ່ລະບົບ|ຮ້ານໄຊໂຢ</title>
  <link rel="stylesheet" href="{{ asset('Backend/plugins/flag-icon-css/css/flag-icon.min.css') }}">
  <link rel="icon" type="image/png" href="https://upload.wikimedia.org/wikipedia/commons/thumb/7/79/Aktenzeichen_XY_Logo_2014.svg/1024px-Aktenzeichen_XY_Logo_2014.svg.png" />
  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ asset('Backend/plugins/fontawesome-free/css/all.min.css')}}">
  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="{{ asset('Backend/plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('Backend/dist/css/adminlte.min.css')}}">
  <!-- Toastr -->
  <link rel="stylesheet" href="{{ asset('Backend/plugins/toastr/toastr.min.css')}}">
   <!-- sweetalert2 -->
   <link rel="stylesheet" href="{{ asset('Backend/plugins/sweetalert2/sweetalert2.css')}}">
   <link rel="stylesheet" href="{{ asset('Backend/plugins/sweetalert2/sweetalert2.min.css')}}">
   <!-- Select2 -->
   <link rel="stylesheet" href="{{ asset('Backend/plugins/select2/css/select2.min.css')}}">
   <link rel="stylesheet" href="{{ asset('Backend/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css')}}">
  <style>
    nav svg{ height: 20px; }
    @font-face{
    font-family: Noto Sans Lao Medium;
    src: url('{{asset('fonts/NotoSansLao-Medium.ttf')}}');
    }
    body {
      background-image: linear-gradient(#246dea, #11dfcb);
      background-color: #cccccc;
}
</style>
@livewireStyles
</head>
<body class="hold-transition login-page" style="font-family: 'Noto Sans Lao Medium'">
  {{ $slot }}
<!-- jQuery -->
<script src="{{ asset('Backend/plugins/jquery/jquery.min.js')}}"></script>
<!-- Bootstrap 4 -->
<script src="{{ asset('Backend/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('Backend/dist/js/adminlte.min.js')}}"></script>
<!-- Toastr -->
<script src="{{ asset('Backend/plugins/toastr/toastr.min.js')}}"></script>
<!-- SweetAlert2 -->
<script src="{{ asset('Backend/plugins/sweetalert2/sweetalert2.all.js')}}"></script>
<script src="{{ asset('Backend/plugins/sweetalert2/sweetalert2.all.min.js')}}"></script>
<script src="{{ asset('Backend/plugins/sweetalert2/sweetalert2.js')}}"></script>
<script src="{{ asset('Backend/plugins/sweetalert2/sweetalert2.min.js')}}"></script>
@livewireScripts
@include('layouts.backend.script')

@stack('scripts')
</body>
</html>

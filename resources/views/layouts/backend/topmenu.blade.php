<div wire:poll>
     <!-- top navigation -->
 <div class="top_nav">
    <div class="nav_menu">
        <div class="nav toggle">
            <a id="menu_toggle"><i class="fa fa-bars"></i></a>
        </div>
        <nav class="nav navbar-nav">
            <ul class="navbar-right">
                @auth
                    @if (Auth::guard('admin')->user()->id != 1)
                        <li class="nav-item dropdown open" style="padding-left: 15px;">
                            <a href="javascript:;" class="user-profile dropdown-toggle" aria-haspopup="true"
                                id="navbarDropdown" data-toggle="dropdown" aria-expanded="false">
                                @if (!empty(Auth::guard('admin')->user()->image))
                                    <img src="https://cdn.pixabay.com/photo/2020/07/01/12/58/icon-5359554_640.png" alt="">{{ Auth::guard('admin')->user()->name }}
                                @else
                                    <img src="{{ asset('public/logo/profile.png') }}">{{ Auth::guard('admin')->user()->name }}
                                @endif
                            </a>
                            <div class="dropdown-menu dropdown-usermenu pull-right" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ route('backend.profile', auth()->user()->id) }}"> <i
                                        class="fa fa-user"></i> ໂປຣຟາຍ</a>
                                <a class="dropdown-item text-danger" href="{{ route('logout') }}"><i
                                        class="fa fa-sign-out pull-right"></i> ອອກຈາກລະບົບ</a>
                            </div>
                        </li>
                    @endif
                @endauth
                <li role="presentation" class="nav-item dropdown open pl-2">
                    <a href="javascript:;" class="dropdown-toggle info-number" id="navbarDropdown1"
                        data-toggle="dropdown" aria-expanded="false">
                        <i class="fa fa-bell"></i>
                        <span class="badge bg-danger text-white">{{ $neworder_count }}</span>
                    </a>
                    <ul class="dropdown-menu list-unstyled msg_list" role="menu" aria-labelledby="navbarDropdown1">
                        @foreach ($neworders as $item)
                        <li class="nav-item">
                           <a href="{{ route('backend.salePendings') }}" class="dropdown-item">
                               <span class="image"><img src="https://img.freepik.com/premium-vector/vector-shopping-cart-icon-paper-sticker-with-shadow-colored-shopping-symbol-isolated_118339-1774.jpg?w=2000" /></span>
                               <span>
                                   <span>ລະຫັດສັ່ງຊື້: {{ $item->code }}</span>
                                   <span class="time">
                                    {{ $item->created_at->diffForHumans() }}
                                   </span>
                               </span>
                               <span class="message">
                              ເປັນເງິນ: {{ number_format($item->total) }} Kip
                               </span>
                           </a>
                       </li>
                        @endforeach
                    </ul>
                </li>
                <li role="presentation" class="nav-item dropdown open">
                   <a href="javascript:;" class="dropdown-toggle info-number" id="navbarDropdown1"
                       data-toggle="dropdown" aria-expanded="false">
                       <i class="fa fa-envelope-o"></i>
                       <span class="badge bg-green">{{ $contact_count }}</span>
                   </a>
                   <ul class="dropdown-menu list-unstyled msg_list" role="menu" aria-labelledby="navbarDropdown1">
                       @foreach ($contact as $item)
                       <li class="nav-item">
                          <a class="dropdown-item">
                              <span class="image"><img src="https://icon-library.com/images/user-icon-jpg/user-icon-jpg-28.jpg" /></span>
                              <span>
                                  <span>{{ $item->name }}, ໂທ:{{ $item->phone }}</span>
                                  <span class="time"> {{ $item->created_at->diffForHumans() }}</span>
                              </span>
                              <span class="message">
                              {{ $item->note }}
                              </span>
                          </a>
                      </li>
                       @endforeach
                   </ul>
               </li>
            </ul>
        </nav>
    </div>
</div>
<!-- /top navigation -->

</div>
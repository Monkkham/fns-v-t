<header class="header header-10 header-intro-clearance">
    <div class="header-top">
        <div class="container">
            <div class="header-left">
                <a href="tel:#"><i class="icon-phone"></i>ຕິດຕໍ່: +0123 456 789</a>
            </div><!-- End .header-left -->

            <div class="header-right">

                <ul class="top-menu">
                    <li>
                        <a href="#">Links</a>
                        <ul>
                            {{-- <li>
                                <div class="header-dropdown">
                                    <a href="#">USD</a>
                                    <div class="header-menu">
                                        <ul>
                                            <li><a href="#">Eur</a></li>
                                            <li><a href="#">Usd</a></li>
                                        </ul>
                                    </div><!-- End .header-menu -->
                                </div><!-- End .header-dropdown -->
                            </li> --}}
                            {{-- <li>
                                <div class="header-dropdown">
                                    <a href="#">Engligh</a>
                                    <div class="header-menu">
                                        <ul>
                                            <li><a href="#">English</a></li>
                                            <li><a href="#">French</a></li>
                                            <li><a href="#">Spanish</a></li>
                                        </ul>
                                    </div><!-- End .header-menu -->
                                </div><!-- End .header-dropdown -->
                            </li> --}}
                        </ul>
                    </li>
                </ul><!-- End .top-menu -->
            </div><!-- End .header-right -->
        </div><!-- End .container -->
    </div><!-- End .header-top -->

    <div class="header-middle">
        <div class="container">
            <div class="header-left">
                <button class="mobile-menu-toggler">
                    <span class="sr-only">Toggle mobile menu</span>
                    <i class="icon-bars"></i>
                </button>

                <a href="{{ route('home') }}" class="logo">
                    <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/7/79/Aktenzeichen_XY_Logo_2014.svg/1024px-Aktenzeichen_XY_Logo_2014.svg.png" alt="Molla Logo" style="height: 50px;">
                </a>

                <p class="text-bold"> <h4>ຮ້ານ: ໄຊໂຢ</h4></p>
            </div><!-- End .header-left -->

          @livewire('frontend.header-content')

            <div class="header-right">
                <div class="header-dropdown-link">
                    <a href="{{ route('frontend.wishlist') }}" class="wishlist-link">
                        <i class="icon-heart-o"></i>
                        @livewire('frontend.wishlist-count-content')
                        <span class="wishlist-txt">ສິ່ງທີ່ມັກ</span>
                    </a>

                    <div class="dropdown cart-dropdown">
                        {{-- ==================cart count product ================ --}}
                        <a href="#" class="dropdown-toggle" role="button" data-toggle="dropdown"
                            aria-haspopup="true" aria-expanded="false" data-display="static">
                            <i class="icon-shopping-cart"></i>
                            @livewire('frontend.cart-count-content')
                            <span class="cart-txt">ກະຕ່າ</span>
                        </a>
                        {{-- ==================Dropdown cart product ================ --}}
                            @livewire('frontend.cart-list-content')
                    </div><!-- End .cart-dropdown -->

                    <div class="dropdown cart-dropdown">
                        @auth
                            @if (Auth::guard('web')->user()->id != 1)
                                <a href="{{ route('frontend.profile', auth()->user()->id) }}" title="ໂປຣຟາຍ"
                                    class="dropdown-toggle" role="button" aria-haspopup="true" aria-expanded="false"
                                    data-display="static">
                                    <div class="icon">
                                        <i class="icon-user"></i>
                                    </div>
                                    <p><b>Hi! {{ Auth::guard('web')->user()->name }}</b></p>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <ul class="compare-products text-center">
                                        <a href="{{ route('frontend.logout') }}" class="btn btn-danger"><span><i
                                                    class="fa fa-sign-out-alt"></i> {{ __('lang.logout') }}</span></a>
                                    </ul>
                                </div>
                                {{-- <div class="wishlist">
                                    <a href="{{ route('frontend.logout') }}" title="ອອກຈາກລະບົບ">
                                        <div class="icon">
                                            <i class="fas fa-sign-out-alt text-danger"></i>
                                        </div>
                                        <p>ອອກລະບົບ</p>
                                    </a>
                                </div><!-- End .compare-dropdown --> --}}
                            @endif
                        @else
                            <a href="{{ route('frontend.login') }}" class="dropdown-toggle" role="button"
                                aria-haspopup="true" aria-expanded="false" data-display="static">
                                <div class="icon">
                                    <i class="icon-unlock"></i>
                                </div>
                                <p>ເຂົ້າສູ່ລະບົບ</p>
                            </a>
                        @endauth
                    </div><!-- End .cart-dropdown -->
                </div>
            </div><!-- End .header-right -->
        </div><!-- End .container -->
    </div><!-- End .header-middle -->

    <div class="header-bottom sticky-header">
        <div class="container">
            @livewire('frontend.search-category')
            <div class="header-center">
                <nav class="main-nav">
                    <ul class="menu sf-arrows">
                        <li class="megamenu-container">
                            <a href="{{ route('home') }}"><i class="fas fa-home"></i> ຫນ້າຫຼັກ</a>
                        </li>
                        <li class="megamenu-container">
                            <a href="{{ route('frontend.shop') }}">ຮ້ານຄ້າ</a>
                        </li>
                        {{-- <li>
                            <a href="#" class="sf-with-ul">ປະເພດສິນຄ້າ</a>
                            <ul>
                                <li><a href="login.html">ອາຫານ</a></li>
                                <li><a href="faq.html">ເຄື່ອງໃຊ້</a></li>
                            </ul>
                        </li> --}}
                        <li class="megamenu-container">
                            <a href="{{ route('frontend.promotion') }}">ໂປຣໂມຊັ່ນ</a>
                        </li>
                        <li class="megamenu-container">
                            <a href="{{ route('frontend.blog') }}">ຂ່າວອັບເດດ</a>
                        </li>
                       
                        <li class="megamenu-container">
                            <a href="{{ route('frontend.about') }}">ກ່ຽວກັບພວກເຮົາ</a>
                        </li>
                    </ul><!-- End .menu -->
                </nav><!-- End .main-nav -->
            </div><!-- End .col-lg-9 -->
            <div class="header-right">
                {{-- <i class="la la-lightbulb-o"></i><p>Clearance Up to 30% Off</span></p> --}}
                <nav class="main-nav">
                    <ul class="menu sf-arrows">
                        <li class="megamenu-container">
                            <a href="{{ route('frontend.contact') }}"> ຕິດຕໍ່ຮ້ານຄ້າ</a>
                        </li>
                    </ul>
                </nav>
            </div>
        </div><!-- End .container -->
    </div><!-- End .header-bottom -->
</header><!-- End .header -->

<footer class="footer footer-2">
    <div class="icon-boxes-container">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-lg-4">
                    <div class="icon-box icon-box-side">
                        <span class="icon-box-icon">
                            <i class="icon-rocket"></i>
                        </span>

                        <div class="icon-box-content">
                            <h3 class="icon-box-title">ຂົນສົ່ງຟີຣທົ່ວປະເທດ</h3><!-- End .icon-box-title -->
                            <p>ສຳຫລັບລູກຄ້າຕ່າງແຂວງສັ່ງຊື້ສິນຄ້າ 300,000 ກີບຂື້ນໄປ</p>
                        </div><!-- End .icon-box-content -->
                    </div><!-- End .icon-box -->
                </div><!-- End .col-sm-6 col-lg-4 -->
                
                <div class="col-sm-6 col-lg-4">
                    <div class="icon-box icon-box-side">
                        <span class="icon-box-icon">
                            <i class="icon-rotate-left"></i>
                        </span>

                        <div class="icon-box-content">
                            <h3 class="icon-box-title">ເວລາການສົ່ງ</h3><!-- End .icon-box-title -->
                            <p>ຕ່າງແຂວງສິນຄ້າຮອດພາຍໃນ 1 ວັນ</p>
                        </div><!-- End .icon-box-content -->
                    </div><!-- End .icon-box -->
                </div><!-- End .col-sm-6 col-lg-4 -->

                {{-- <div class="col-sm-6 col-lg-4">
                    <div class="icon-box icon-box-side">
                        <span class="icon-box-icon">
                            <i class="icon-info-circle"></i>
                        </span>

                        <div class="icon-box-content">
                            <h3 class="icon-box-title">ຮັບສ່ວນຫລຸດ 5%</h3><!-- End .icon-box-title -->
                            <p>ເມື່ອທ່ານຊື້ສິນຄ້າລາຄາ 100,000 ກີບຂື້ນໄປ </p>
                        </div><!-- End .icon-box-content -->
                    </div><!-- End .icon-box -->
                </div><!-- End .col-sm-6 col-lg-4 --> --}}

                <div class="col-sm-6 col-lg-4">
                    <div class="icon-box icon-box-side">
                        <span class="icon-box-icon">
                            <i class="icon-life-ring"></i>
                        </span>

                        <div class="icon-box-content">
                            <h3 class="icon-box-title">ເປີດບໍລິການ</h3><!-- End .icon-box-title -->
                            <p>24 ຊົ່ວໂມງ</p>
                        </div><!-- End .icon-box-content -->
                    </div><!-- End .icon-box -->
                </div><!-- End .col-sm-6 col-lg-3 -->
            </div><!-- End .row -->
        </div><!-- End .container -->
    </div><!-- End .icon-boxes-container -->

    <div class="footer-middle border-0">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-lg-6">
                    <div class="widget widget-about">
                        <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/7/79/Aktenzeichen_XY_Logo_2014.svg/1024px-Aktenzeichen_XY_Logo_2014.svg.png" class="footer-logo" alt="Footer Logo" width="105" height="25">
                        <p>ຮ້ານຄ້າຕັ້ງຢູ່: ບ້ານຍອດງື່ມ, ເມືອງແປກ, ແຂວງຊຽງຂວາງ ປະເທດລາວ </p>
                        
                        <div class="widget-about-info">
                            <div class="row">
                                <div class="col-sm-6 col-md-4">
                                    <span class="widget-about-title">ສອບຖາມໄດ້ທີ່ເບີ</span>
                                    <a href="tel:123456789">020 52310555</a>
                                </div><!-- End .col-sm-6 -->
                                <div class="col-sm-6 col-md-8">
                                    <span class="widget-about-title">ວິທີຊຳລະເງິນ</span>
                                    <figure class="footer-payments">
                                        <img src="https://lirp.cdn-website.com/39441e05/dms3rep/multi/opt/OnePay-509x171-640w.png" alt="Payment methods" style="height: 50px;">
                                    </figure><!-- End .footer-payments -->
                                </div><!-- End .col-sm-6 -->
                            </div><!-- End .row -->
                        </div><!-- End .widget-about-info -->
                    </div><!-- End .widget about-widget -->
                </div><!-- End .col-sm-12 col-lg-3 -->

                <div class="col-sm-4 col-lg-2">
                    <div class="widget">
                        <h4 class="widget-title">ເມນູລະບົບ</h4><!-- End .widget-title -->

                        <ul class="widget-list">
                            <li><a href="{{ route('home') }}">ຫນ້າຫຼັກ</a></li>
                            <li><a href="{{ route('frontend.shop') }}">ຮ້ານຄ້າ</a></li>
                            <li><a href="">ໂປຣໂມຊັ່ນ</a></li>
                            <li><a href="{{ route('frontend.blog') }}">ຂ່າວອັບເດດ</a></li>
                            <li><a href="{{ route('frontend.about') }}">ກ່ຽວກັບພວກເຮົາ</a></li>
                            <li><a href="{{ route('frontend.contact') }}">ຕິດຕໍ່ຮ້ານຄ້າ</a></li>
                        </ul><!-- End .widget-list -->
                    </div><!-- End .widget -->
                </div><!-- End .col-sm-4 col-lg-3 -->

                <div class="col-sm-4 col-lg-2">
                    <div class="widget">
                        <h4 class="widget-title">ຊ່ອງທາງການຊຳລະ</h4><!-- End .widget-title -->

                        <ul class="widget-list">
                            <li><a href="#">ຊຳລະເງິນປາຍທາງ</a></li>
                            <li><a href="#">ຊຳລະເງິນໂອນ</a></li>
                            <li><a href="#">ເລກບັນຊີ: 600 4555 2156 6454</a></li>
                        </ul><!-- End .widget-list -->
                    </div><!-- End .widget -->
                </div><!-- End .col-sm-4 col-lg-3 -->

                <div class="col-sm-4 col-lg-2">
                    <div class="widget">
                        <h4 class="widget-title">ບັນຊີຂອງຂ້ອຍ</h4><!-- End .widget-title -->

                        <ul class="widget-list">
                            <li><a href="{{ route('frontend.register') }}">ລົງທະບຽນ</a></li>
                            <li><a href="{{ route('frontend.login') }}">ເຂົ້າສູ່ລະບົບ</a></li>
                            <li><a href="{{ route('frontend.cart') }}">ກະຕ່າສິນຄ້າ</a></li>
                            <li><a href="{{ route('frontend.wishlist') }}">ສິ່ງທີ່ມັກ</a></li>
                            <li><a href="#">ປະຫວັດການສັ່ງຊື້</a></li>
                        </ul><!-- End .widget-list -->
                    </div><!-- End .widget -->
                </div><!-- End .col-sm-64 col-lg-3 -->
            </div><!-- End .row -->
        </div><!-- End .container -->
    </div><!-- End .footer-middle -->

    <div class="footer-bottom">
        <div class="container">
            <p class="footer-copyright">ອ້າງອີງຈາກ Molla Store ສືບຕໍ່ພັດທະນາໂດຍ:ນັກສຶກສາ ຄະນະວິທະຍາສາດທຳມະຊາດ (ມຊ) ສ ປ ປ ລາວ</p><!-- End .footer-copyright -->
            <div class="social-icons social-icons-color">
                <span class="social-label">Social Media</span>
                <a href="#" class="social-icon social-facebook" title="Facebook" target="_blank"><i class="icon-facebook-f"></i></a>
                <a href="#" class="social-icon social-twitter" title="Twitter" target="_blank"><i class="icon-twitter"></i></a>
                <a href="#" class="social-icon social-instagram" title="Instagram" target="_blank"><i class="icon-instagram"></i></a>
                <a href="#" class="social-icon social-youtube" title="Youtube" target="_blank"><i class="icon-youtube"></i></a>
                <a href="#" class="social-icon social-pinterest" title="Pinterest" target="_blank"><i class="icon-pinterest"></i></a>
            </div><!-- End .soial-icons -->
        </div><!-- End .container -->
    </div><!-- End .footer-bottom -->
</footer><!-- End .footer -->
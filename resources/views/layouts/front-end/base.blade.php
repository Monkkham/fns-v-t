<!DOCTYPE html>
<html lang="en">
<!-- molla/index-13.html  22 Nov 2019 09:59:06 GMT -->

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" href="https://upload.wikimedia.org/wikipedia/commons/thumb/7/79/Aktenzeichen_XY_Logo_2014.svg/1024px-Aktenzeichen_XY_Logo_2014.svg.png" type="image/ico" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>ຮ້ານ: ຂາຍເຄື່ອງໄຊໂຢ</title>
    <meta name="keywords" content="HTML5 Template">
    <meta name="description" content="Molla - Bootstrap eCommerce Template">
    <meta name="author" content="p-themes">
    <!-- Favicon -->
    <link rel="apple-touch-icon" sizes="180x180"
        href="{{ asset('Frontend/assets/images/icons/apple-touch-icon.png') }}">
    <link rel="icon" type="image/png" sizes="32x32"
        href="{{ asset('Frontend/assets/images/icons/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="16x16"
        href="{{ asset('Frontend/assets/images/icons/favicon-16x16.png') }}">
    <link rel="manifest" href="{{ asset('Frontend/assets/images/icons/site.html') }}">
    <link rel="mask-icon" href="{{ asset('Frontend/assets/images/icons/safari-pinned-tab.svg" color="#666666') }}">
    <link rel="shortcut icon" href="{{ asset('Frontend/assets/images/icons/favicon.ico') }}">
    <meta name="apple-mobile-web-app-title" content="Molla">
    <meta name="application-name" content="Molla">
    <meta name="msapplication-TileColor" content="#cc9966">
    <meta name="msapplication-config" content="{{ asset('Frontend/assets/images/icons/browserconfig.xml') }}">
    <meta name="theme-color" content="#ffffff">
    <link rel="stylesheet"
        href="{{ asset('Frontend/assets/vendor/line-awesome/line-awesome/line-awesome/css/line-awesome.min.css') }}">
    <!-- sweetalert2 -->
    <link rel="stylesheet" href="{{ asset('Backend/plugins/sweetalert2/sweetalert2.css') }}">
    <!-- Plugins CSS File -->
    <link rel="stylesheet" href="{{ asset('Frontend/assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('Frontend/assets/css/plugins/owl-carousel/owl.carousel.css') }}">
    <link rel="stylesheet" href="{{ asset('Frontend/assets/css/plugins/magnific-popup/magnific-popup.css') }}">
    <link rel="stylesheet" href="{{ asset('Frontend/assets/css/plugins/jquery.countdown.css') }}">
    <!-- Main CSS File -->
    <link rel="stylesheet" href="{{ asset('Frontend/assets/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('Frontend/assets/css/skins/skin-demo-13.css') }}">
    <link rel="stylesheet" href="{{ asset('Frontend/assets/css/demos/demo-13.css') }}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('Backend/plugins/flag-icon-css/css/flag-icon.min.css') }}">
    <link rel="stylesheet" href="{{ asset('Backend/plugins/fontawesome-free/css/all.min.css') }}">
    <style>
        nav svg {
            height: 20px;
        }

        @font-face {
            font-family: Lao_Modern2;
            src: url('{{ asset('fonts/Lao_Modern2.ttf') }}');
        }

        a,
        p,
        * {
            font-family: 'Lao_Modern2';
        }

        /********** Template CSS **********/

        :root {
            --primary: #45b7fd;
            --secondary: #777777;
            --light: #F8F8F8;
            --dark: #252525;
        }

        .back-to-top {
            position: fixed;
            display: none;
            right: 50px;
            bottom: 50px;
            z-index: 99;
        }

        /*** Spinner ***/

        #spinner {
            opacity: 0;
            visibility: hidden;
            transition: opacity .5s ease-out, visibility 0s linear .5s;
            z-index: 99999;
        }

        #spinner.show {
            transition: opacity .5s ease-out, visibility 0s linear 0s;
            visibility: visible;
            opacity: 1;
        }
    </style>
    @livewireStyles
</head>

<body>
        <!-- Spinner Start -->
    <div id="spinner"
        class="show bg-white position-fixed translate-middle w-100 vh-100 top-50 start-50 d-flex align-items-center justify-content-center">
        <div class="spinner-grow text-primary" role="status">
            <img  src="https://loading.io/mod/spinner/shopping/sample.gif" alt="">
        </div>
    </div>
    <!-- Spinner End -->
    <div class="page-wrapper">
        @include('layouts.front-end.header')
        {{ $slot }}
        @include('layouts.front-end.footer')
    </div>
    @include('layouts.front-end.mobile-menu')
    <!-- Plugins JS File -->
    <script src="{{ asset('Frontend/assets/js/jquery.min.js') }}"></script>
    <script src="{{ asset('Frontend/assets/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('Frontend/assets/js/jquery.hoverIntent.min.js') }}"></script>
    <script src="{{ asset('Frontend/assets/js/jquery.waypoints.min.js') }}"></script>
    <script src="{{ asset('Frontend/assets/js/superfish.min.js') }}"></script>
    <script src="{{ asset('Frontend/assets/js/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('Frontend/assets/js/bootstrap-input-spinner.js') }}"></script>
    <script src="{{ asset('Frontend/assets/js/jquery.magnific-popup.min.js') }}"></script>
    <script src="{{ asset('Frontend/assets/js/jquery.plugin.min.js') }}"></script>
    <script src="{{ asset('Frontend/assets/js/jquery.countdown.min.js') }}"></script>
    <!-- Main JS File -->
    <script src="{{ asset('Frontend/assets/js/main.js') }}"></script>
    <script src="{{ asset('Frontend/assets/js/demos/demo-13.js') }}"></script>
    <!-- SweetAlert2 -->
    <script src="{{ asset('Backend/plugins/sweetalert2/sweetalert2.js') }}"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    @include('layouts.backend.script')
    @stack('scripts')
    @livewireScripts
</body>

</html>

<div wire:poll>
    <!-- page content -->
    <div class="right_col" role="main">
        <div class="">
            <div class="row">
                <div class="col-md-12">
                    <div class="x_panel">
                        <div class="animated flipInY col-md-4 col-sm-6 ">
                            <div class="tile-stats">
                                <div class="icon"><i class="fa fa-user"></i></div>
                                <div class="count">{{ $totalEmployee }}</div>
                                <h3>ພະນັກງານ</h3>
                            </div>
                        </div>
                    <div class="animated flipInY col-md-4 col-sm-6 ">
                        <div class="tile-stats">
                            <div class="icon"><i class="fa fa-users"></i></div>
                            <div class="count">{{ $totalCustomer }}</div>
                            <h3>ລູກຄ້າ</h3>
                        </div>
                    </div>
                    <div class="animated flipInY col-md-4 col-sm-6 ">
                        <div class="tile-stats">
                            <div class="icon"><i class="fa fa-users"></i></div>
                            <div class="count">{{ $this->supplier_count }}</div>
                            <h3>ຜູ້ສະຫນອງ</h3>
                        </div>
                    </div>
                          <div class="animated flipInY col-md-4 col-sm-6 ">
                        <div class="tile-stats">
                            <div class="icon"><i class="fa fa-file"></i></div>
                            <div class="count">{{ $totalProductItem }}</div>
                            <h3>ສິນຄ້າທັງຫມົດ</h3>
                        </div>
                    </div>
                    <div class="animated flipInY col-md-4 col-sm-6 ">
                        <div class="tile-stats">
                            <div class="icon"><i class="fa fa-cart-plus"></i></div>
                            <div class="count">{{ $this->orders_count }}</div>
                            <h3>ຈຳນວນການສັ່ງຊື້</h3>
                        </div>
                    </div>
                    <div class="animated flipInY col-md-4 col-sm-6 ">
                        <div class="tile-stats">
                            <div class="icon"><i class="fa fa-balance-scale"></i></div>
                            <div class="count">{{ $this->sales_count }}</div>
                            <h3>ຈຳນວນການຂາຍ</h3>
                        </div>
                    </div>

                    <div class="animated flipInY col-md-6 col-sm-6 ">
                        <div class="tile-stats">
                            <div class="icon"><i class="fa fa-cart-plus"></i></div>
                            <div class="count">{{ number_format($this->order_sum) }} ₭</div>
                            <h3>ລວມຍອດສັ່ງຊື້</h3>
                        </div>
                    </div>
                    <div class="animated flipInY col-md-6 col-sm-6 ">
                        <div class="tile-stats">
                            <div class="icon"><i class="fa fa-balance-scale"></i></div>
                            <div class="count">{{ number_format($this->sale_sum) }} ₭</div>
                            <h3>ລວມຍອດຂາຍ</h3>
                </div>
            </div>
            {{-- <div class="row">
                <div class="col-md-12">
                    <div class="x_panel">
                        
                    </div>
                </div>
            </div> --}}
        </div>
        {{-- ========================================== --}}
        <div class="row">
            <div class="col-md-12">
                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead class="bg-light">
                            <tr>
                                <th class="text-center" colspan="10">
                                    <h5><i class="fa fa-users"></i> ລາຍການສັ່ງຊື້ອອນລາຍໃຫມ່ລ່າສຸດ</h5>
                                </th>
                            </tr>
                            <tr class="text-center text-white bg-info">
                                <th>ລຳດັບ</th>
                                <th>ລະຫັດບິນ</th>
                                <th>ລວມເປັນເງິນ</th>
                                <th>ລູກຄ້າ</th>
                                <th>ເພດ</th>
                                <th>ເບີໂທ</th>
                                <th>ທຸລະກຳ</th>
                                <th>ວັນທີ່</th>
                                <th>ສະຖານະ</th>
                                {{-- @foreach ($rolepermissions as $items)
                                    @if ($items->permissionname->name == 'action_employee') --}}
                                <th>ຈັດການ</th>
                                {{-- @endif
                                    @endforeach --}}
                            </tr>
                        </thead>
                        @php
                            $num = 1;
                        @endphp
                        <tbody>

                            @foreach ($sales as $item)
                                <tr class="text-center">
                                    <td>{{ $num++ }}</td>
                                    <td>{{ $item->code }}</td>
                                    <td class="text-bold">{{ number_format($item->total) }} ₭</td>
                                    <td>
                                        @if (!empty($item->customer))
                                            {{ $item->customer->name }} {{ $item->customer->lastname }}
                                        @endif
                                    </td>
                                    <td>
                                        @if (!empty($item->customer))
                                            @if ($item->customer->gender == 1)
                                                <b class="text-success">ຍິງ</b>
                                            @elseif($item->customer->gender == 2)
                                                <b class="text-info">ຊາຍ</b>
                                            @endif
                                        @endif
                                    </td>
                                    <td>
                                        @if (!empty($item->customer))
                                            {{ $item->customer->phone }}
                                        @endif
                                    </td>
                                    <td>
                                        @if ($item->payment == 1)
                                            <button type="button" class="btn btn-sm btn-outline-danger"><i
                                                    class="fa fa-credit-card"></i>
                                                OnePay</button>
                                        @elseif($item->payment == 0)
                                            <p class="badge badge-warning p-2"><i class="fa fa-money"></i>
                                                COD</p>
                                        @endif
                                    </td>
                                    <td>{{ date('d/m/Y', strtotime($item->created_at)) }}</td>
                                    <td>
                                        @if ($item->status == 1)
                                            <p class="bg-success text-white text-center rounded"><i
                                                    class="fas fa-plus-circle"></i>
                                                ໃຫມ່ <span
                                                    class="spinner-grow mb-1 spinner-grow-sm text-white text-center"
                                                    role="status" aria-hidden="true"></span></p>
                                        @elseif($item->status == 2)
                                            <p class="bg-success text-center text-white rounded"><i
                                                    class="fa fa-check-circle"></i> ນຳເຂົ້າສຳເລັດ</p>
                                        @elseif($item->status == 0)
                                            <p class="bg-danger text-center text-white rounded"><i
                                                    class="fas fa-times-circle"></i> ຖືກຍົກເລີກ</p>
                                        @endif
                                    </td>
                                    {{-- <td>{{ date('d-m-Y', strtotime($item->created_at)) }}</td> --}}
                                    {{-- @foreach ($rolepermissions as $items)
                                        @if ($items->permissionname->name == 'action_preorders') --}}
                                    <td>
                                        {{-- @if ($item->payment != 1 && $item->status != 0) --}}
                                        <a href="{{ route('backend.salePendings') }}" class="btn btn-info btn-sm">
                                            ໄປທີ່ລາຍການ
                                        </a>
                                        {{-- @endif --}}
                                    </td>
                                    {{-- @endif
                                        @endforeach --}}
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div class="float-right">
                        {{ $sales->links() }}
                    </div>

                    <div class="d-flex justify-content-between">
                        <div>
                            {{-- {{$sale->links()}} --}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /page content -->
</div>

<div>
    {{-- ======================================== name page ====================================================== --}}
    <div class="right_col" role="main">
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h6><i class="fa fa-globe"></i> ຂໍ້ມູນເວບໄຊ <i class="fa fa-angle-double-right"></i>
                        ໂພດສາທາລະນະ</h6>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">ຫນ້າຫຼັກ</a></li>
                        <li class="breadcrumb-item active">ໂພດສາທາລະນະ</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <!--List users- table table-bordered table-striped -->
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="row">
                                        <div class="col-md-6">
                                            {{-- @foreach ($rolepermissions as $items)
                                            @if ($items->permissionname->name == 'action_post') --}}
                                            <a wire:click="create" class="btn btn-primary btn-sm"
                                                href="javascript:void(0)"><i class="fa fa-plus-circle"></i>
                                                ເພີ່ມໃຫມ່</a>
                                            {{-- @endif
                                             @endforeach --}}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-5">

                                </div>
                                <div class="row">
                                    <div class="col-md-8">
                                    </div>
                                    <div class="input-group input-group-sm" style="width: 230px;">
                                        <input wire:model="search" type="text" class="form-control"
                                            placeholder="ຄົ້ນຫາ..">

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead class="bg-light">
                                        <tr>
                                            
                                            <th>ລຳດັບ</th>
                                            <th>ຮູບພາບ</th>
                                            <th>ຄຳອະທິບາຍ</th>
                                            {{-- @foreach ($rolepermissions as $items)
                                            @if ($items->permissionname->name == 'action_post') --}}
                                            <th>ຈັດການ</th>
                                            {{-- @endif
                                            @endforeach --}}
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php
                                            $num = 1;
                                        @endphp

                                        @foreach ($post as $item)
                                            <tr>
                                               
                                                <td>{{ $num++ }}</td>
    
                                                <td>
                                                    @if (!empty($item->image))
                                                        <a href="{{ asset('public/'.$item->image) }}">
                                                            <img class="rounded"
                                                                src="{{ asset('public/'.$item->image) }}"
                                                                width="60px;" height="60px;">
                                                        </a>
                                                    @else
                                                        <img src="{{ asset('logo/noimage.jpg') }}" width="60px;"
                                                            height="60px;">
                                                    @endif
                                                </td>
                                                <td>{!! $item->note !!}</td>
                                        {{-- <td>{{ $item->address }}</td> --}}
                                        {{-- <td>{{ date('d-m-Y', strtotime($item->created_at)) }}</td> --}}
                                        {{-- @foreach ($rolepermissions as $items)
                                        @if ($items->permissionname->name == 'action_post') --}}
                                        <td>
                                            <div class="btn-group">
                                                <button wire:click="edit({{ $item->id }})" type="button"
                                                    class="btn btn-warning btn-sm"><i
                                                        class="fa fa-pencil"></i></button>
                                                <button wire:click="showDestroy({{ $item->id }})"
                                                    type="button" class="btn btn-danger btn-sm"><i
                                                        class="fa fa-trash"></i></button>
                                            </div>
                                        </td>
                                        {{-- @endif
                                        @endforeach --}}
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>

                                <div class="float-right">
                                    {{ $post->links() }}
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    </div>
    @include('livewire.backend.store.post-create')
    @include('livewire.backend.store.post-update')
    @include('livewire.backend.store.post-delete')
</div>

    <!-- /.modal-edit -->
    <div wire:ignore.self class="modal fade" id="modal-edit">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header bg-light">
                    <h5 class="modal-title"><i class="fa fa-edit text-warning"></i> ແກ້ໄຂ</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="container">
                            <div class="avatar-upload">
                                <div class="avatar-edit">
                                    <input type='file' wire:model="image" id="imageUpload2"
                                        accept=".png, .jpg, .jpeg" />
                                    <label for="imageUpload2"></label>
                                </div>
                                @if ($image)
                                    <div class="avatar-preview">
                                        <img id="imagePreview2" src="{{ $image->temporaryUrl() }}" alt="" width="120px;">
                                    </div>
                                @else
                                    @if ($newimage)
                                        <div class="avatar-preview">
                                            <img id="imagePreview2" src="{{ asset('employee') }}/{{ $newimage }}"
                                                alt="" width="120px;">
                                        </div>
                                    @else
                                        <div class="avatar-preview">
                                            <div id="imagePreview2"
                                                style="background-image: url({{ asset('logo/noimage.png') }});">
                                            </div>
                                        </div>
                                    @endif
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <label>ເລືອກເພດ</label>
                                <div class="form-group clearfix">
                                    <div class="icheck-success d-inline">
                                        <input type="radio" id="radioPrimary1" value="1" wire:model="status_sell"
                                            >
                                        <label for="radioPrimary1">ໃຫມ່
                                        </label>
                                    </div>
                                    {{-- </div>
                                <div class="form-group clearfix"> --}}
                                    <div class="icheck-success d-inline">
                                        <input type="radio" id="radioPrimary2" value="2" wire:model="status_sell">
                                        <label for="radioPrimary2">ຂາຍດີ
                                        </label>
                                    </div>
                                    <div class="icheck-success d-inline">
                                        <input type="radio" id="radioPrimary3" value="3" wire:model="status_sell" checked>
                                        <label for="radioPrimary3">ຄ່າວ່າງ
                                        </label>
                                    </div>
                                </div>
                                @error('status_sell')
                                    <span style="color: red" class="error">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>ເລືອກປະເພດສິນຄ້າ</label>
                                    <select wire:model="product_type_id" class="form-control">
                                        <option value="" selected>ເລືອກ</option>
                                        @foreach ($product_type as $item)
                                            <option value="{{ $item->id }}">{{ $item->name }}</option>
                                        @endforeach
                                    </select>
                                    @error('product_type_id')
                                        <span style="color: red" class="error">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>ເລືອກຫົວຫນ່ວຍ</label>
                                    <select wire:model="unit_id" class="form-control">
                                        <option value="" selected>ເລືອກ</option>
                                        @foreach ($units as $item)
                                            <option value="{{ $item->id }}">{{ $item->name }}</option>
                                        @endforeach
                                    </select>
                                    @error('unit_id')
                                        <span style="color: red" class="error">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>ຊື່</label>
                                    <input wire:model="name" type="text" placeholder="ປ້ອນຂໍ້ມູນ"
                                        class="form-control @error('name') is-invalid @enderror">
                                    @error('name')
                                        <span style="color: red" class="error">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>ລາຄາຂາຍ</label>
                                    <input wire:model="sell_price" min="1" type="number" placeholder="ປ້ອນຂໍ້ມູນ"
                                        class="form-control @error('sell_price') is-invalid @enderror">
                                    @error('sell_price')
                                        <span style="color: red" class="error">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>ລາຄາປົກກະຕິ</label>
                                    <input wire:model="promotion_price" min="1" type="number" placeholder="ປ້ອນຂໍ້ມູນ"
                                        class="form-control @error('promotion_price') is-invalid @enderror">
                                    @error('promotion_price')
                                        <span style="color: red" class="error">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            {{-- <div class="col-md-3">
                                <div class="form-group">
                                    <label>ຈຳນວນ</label>
                                    <input wire:model="qty" type="text" placeholder="ປ້ອນຂໍ້ມູນ"
                                        class="form-control @error('qty') is-invalid @enderror">
                                    @error('qty')
                                        <span style="color: red" class="error">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div> --}}
                            <div class="col-md-9">
                                <div class="form-group">
                                    <label for="note2">ລາຍລະອຽດສິນຄ້າ</label>
                                    <div wire:ignore>
                                        <textarea class="form-control" id="note2" wire:model="note">{{$note}}</textarea>
                                    </div>
                                    @error('note')
                                    <span style="color: red" class="error">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                </div>
                </form>
                <div class="modal-footer justify-content-between bg-light">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">ຍົກເລີກ</button>
                    <button wire:click="update" type="button" class="btn btn-success">ບັນທຶກ</button>
                </div>
            </div>
        </div>
    </div>

<div>
    <!-- page content -->
    <div class="right_col" role="main">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h6><i class="fa fa-database"></i> ຈັດການຂໍ້ມູນ <i class="fa fa-angle-double-right"></i>
                            ພະນັກງານ</h6>
                    </div>
                    {{-- <div class="col-sm-6">
                                <ol class="breadcrumb float-sm-right">
                                    <li class="breadcrumb-item"><a href="{{ route('dashboards') }}">{{ __('lang.home') }}</a></li>
                                    <li class="breadcrumb-item active">ພະນັກງານ</li>
                                </ol>
                            </div> --}}
                </div>
            </div>
        </section>
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12 col-sm-12 ">
                        <div class="x_panel">
                            <div class="x_content">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="row">
                                            <div class="col-md-6">
                                                {{-- @foreach ($rolepermissions as $items)
                                                        @if ($items->permissionname->name == 'action_expend') --}}
                                                <a wire:click="create" class="btn btn-primary btn-sm"
                                                    href="javascript:void(0)"><i class="fa fa-plus-circle"></i>
                                                    ເພີ່ມໃຫມ່</a>
                                                {{-- @endif
                                                         @endforeach --}}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-5">

                                    </div>
                                    <div class="row">
                                        <div class="col-md-8">
                                        </div>
                                        <div class="input-group input-group-sm" style="width: 230px;">
                                            <input wire:model="search" type="text" class="form-control"
                                                placeholder="ຄົ້ນຫາຂໍ້ມູນ">

                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="card-box table-responsive">
                                            <table id="example1"
                                                class="table table-striped"
                                                cellspacing="0" width="100%">
                                                <thead class="bg-green">
                                                    <tr>
                                                        <th>ລຳດັບ</th>
                                                        <th>ລະຫັດ</th>
                                                        <th>ຮູບພາບ</th>
                                                        <th>ຊື່</th>
                                                        <th>ນາມສະກຸນ</th>
                                                        <th>ເພດ</th>
                                                        <th>ເບີໂທ</th>
                                                        <th>ທີ່ຢູ່</th>
                                                        <th>ຈັດການ</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @php
                                                        $num = 1;
                                                    @endphp

                                                    @foreach ($employee as $item)
                                                        <tr>

                                                            <td>{{ $num++ }}</td>
                                                            <td>{{ $item->code }}</td>
                                                            <td>
                                                                @if (!empty($item->image))
                                                                    <a
                                                                        href="{{ asset('public/'.$item->image) }}">
                                                                        <img class="rounded"
                                                                            src="{{ asset('public/'.$item->image) }}"
                                                                            width="60px;" height="60px;">
                                                                    </a>
                                                                @else
                                                                    <img src="{{ ('logo/noimage.png') }}"
                                                                        width="60px;" height="60px;">
                                                                @endif
                                                            </td>
                                                            <td>{{ $item->name }}</td>
                                                            <td>{{ $item->lastname }}</td>
                                                            <td>
                                                                @if ($item->gender == 1)
                                                                    <b class="text-success">ຍິງ</b>
                                                                @elseif($item->gender == 2)
                                                                    <b class="text-info">ຊາຍ</b>
                                                                @else
                                                            <td></td>
                                                    @endif
                                                    </td>
                                                    <td>{{ $item->phone }}</td>
                                                    {{-- @if (!empty($item->roles))
                                                    <td class="text-bold text-center">
                                                        <p class="bg-success rounded">{{ $item->roles->name }}</p>
                                                    </td>
                                                @else
                                                    <td></td>
                                                @endif --}}
                                                    @if (!empty($item->province))
                                                        <td>{{ $item->village->name_la }},{{ $item->district->name_la }},{{ $item->province->name_la }}
                                                        </td>
                                                    @else
                                                        <td></td>
                                                    @endif
                                                    {{-- <td>{{ date('d-m-Y', strtotime($item->created_at)) }}</td> --}}
                                                    {{-- @foreach ($rolepermissions as $items)
                                                        @if ($items->permissionname->name == 'action_employee') --}}
                                                    <td>
                                                        <div class="btn-group">
                                                            <button wire:click="edit({{ $item->id }})"
                                                                type="button" class="btn btn-warning btn-sm"><i
                                                                    class="fa fa-pencil"></i></button>
                                                            <button wire:click="showDestroy({{ $item->id }})"
                                                                type="button" class="btn btn-danger btn-sm"><i
                                                                    class="fa fa-trash"></i></button>
                                                        </div>
                                                    </td>
                                                    {{-- @endif
                                                        @endforeach --}}
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                            <div class="float-right">
                                                {{ $employee->links() }}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>
    </section>
    @include('livewire.backend.data-store.employee-create')
    @include('livewire.backend.data-store.employee-update')
    @include('livewire.backend.data-store.employee-delete')
</div>

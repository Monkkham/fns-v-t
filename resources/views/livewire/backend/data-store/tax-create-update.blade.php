            <!--Foram add new-->
            <div class="col-md-4">
                <div class="card card-default">
                  <div class="card-header bg-green">
                    <label>ເພີ່ມໃຫມ່ / ແກ້ໄຂ</label>
                  </div>
                  <form>
                      <div class="card-body">
                          <input type="hidden" wire:model="ID" value="{{$ID}}">
                          <div class="row">
                              <div class="col-md-12">
                                <div class="form-group">
                                  <label>ຊື່</label>
                                  <input wire:model="name" type="text" class="form-control @error('name') is-invalid @enderror" placeholder="ປ້ອນຂໍ້ມູນ">
                                  @error('name') <span style="color: red" class="error">{{ $message }}</span> @enderror
                                </div>
                              </div>
                          </div>
                          <div class="row">
                            <div class="col-md-12">
                              <div class="form-group">
                                <label>ເປີເຊັນ</label>
                                <input wire:model="percent" type="text" class="form-control @error('percent') is-invalid @enderror" placeholder="ປ້ອນຂໍ້ມູນ">
                                @error('percent') <span style="color: red" class="error">{{ $message }}</span> @enderror
                              </div>
                            </div>
                        </div>
                      </div>
                      <div class="card-footer">
                          <div class="d-flex justify-content-between md-2">
                            {{-- @foreach ($rolepermissions as $items)
                            @if ($items->permissionname->name == 'action_customer_type') --}}
                                  <button type="button" wire:click="resetform" class="btn btn-warning">ຣີເຊັດ</button>
                                  <button type="button" wire:click="store" class="btn btn-success">ບັນທຶກ</button>
                                  {{-- @endif
                              @endforeach --}}
                          </div>
                      </div>
                  </form>
                </div>
              </div>

@push('scripts')
    <script>
      window.addEventListener('show-modal-delete', event => {
          $('#modal-delete').modal('show');
      })
      window.addEventListener('hide-modal-delete', event => {
          $('#modal-delete').modal('hide');
      })
    </script>
@endpush
<div>
    {{-- ======================================== name page ====================================================== --}}
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h6><i class="fas fa-users"></i> ຜູ້ໃຊ້ & ສິດທິ <i class="fa fa-angle-double-right"></i>
                        ສິດທິ <i class="fa fa-angle-double-right"></i> ແກ້ໄຂ</h6>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">{{ __('lang.home') }}</a></li>
                        <li class="breadcrumb-item active"> ແກ້ໄຂ</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>ຊື່ສິດທິ</label>
                                        <input wire:model="name" type="text"
                                            class="form-control @error('name') is-invalid @enderror"
                                            placeholder="ປ້ອນຂໍ້ມູນ">
                                        @error('name')
                                            <span style="color: red" class="error">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>ຄວາມຫມາຍ</label>
                                        <input wire:model="meaning" type="text"
                                            class="form-control @error('meaning') is-invalid @enderror"
                                            placeholder="ປ້ອນຂໍ້ມູນ">
                                        @error('meaning')
                                            <span style="color: red" class="error">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <!-- ========================= Start =========================== -->
                                <div class="col-md-3">
                                    <h6 class="bg-warning p-1"><i class="fas fa-lock"></i> ຈັດການຂໍ້ມູນ</h6>
                                </div>
                                <div class="col-md-10">
                                    <hr>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3"></div>
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <div class="icheck-success d-inline">
                                                <input id="1" wire:model="selectedtypes" type="checkbox" value="1">
                                                <label for="1" style="color:black">ພະນັກງານ</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <div class="icheck-success d-inline">
                                                <input id="2" wire:model="selectedtypes" type="checkbox" value="2">
                                                <label for="2" style="color:black">ລູກຄ້າ</label>
                                            </div>
                                        </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <div class="icheck-success d-inline">
                                                <input id="3" wire:model="selectedtypes" type="checkbox" value="3">
                                                <label for="3" style="color:black">ປະເພດລູກຄ້າ</label>
                                            </div>
                                        </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <div class="icheck-success d-inline">
                                                <input id="4" wire:model="selectedtypes" type="checkbox" value="4">
                                                <label for="4" style="color:black">ຜູ້ສະຫນອງ</label>
                                            </div>
                                        </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <div class="icheck-success d-inline">
                                                <input id="5" wire:model="selectedtypes" type="checkbox" value="5">
                                                <label for="5" style="color:black">ສິນຄ້າ</label>
                                            </div>
                                        </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <div class="icheck-success d-inline">
                                                <input id="6" wire:model="selectedtypes" type="checkbox" value="6">
                                                <label for="6" style="color:black">ປະເພດສິນຄ້າ</label>
                                            </div>
                                        </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <div class="icheck-success d-inline">
                                                <input id="7" wire:model="selectedtypes" type="checkbox" value="7">
                                                <label for="7" style="color:black">ຫົວຫນ່ວຍ</label>
                                            </div>
                                        </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <div class="icheck-success d-inline">
                                                <input id="8" wire:model="selectedtypes" type="checkbox" value="8">
                                                <label for="8" style="color:black">ສ່ວນຫຼຸດ</label>
                                            </div>
                                        </div>
                                        </div>
                                        {{-- <div class="col-md-3">
                                            <div class="form-group">
                                                <div class="icheck-success d-inline">
                                                <input id="9" wire:model="selectedtypes" type="checkbox" value="9">
                                                <label for="9" style="color:black">ສ່ວນຫຼຸດ</label>
                                            </div>
                                        </div>
                                        </div> --}}
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <div class="icheck-success d-inline">
                                                <input id="10" wire:model="selectedtypes" type="checkbox" value="9">
                                                <label for="10" style="color:black">ບັນທຶກປະຈຳວັນ</label>
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3"></div>
                            </div>
                            <!-- ========================= Start =========================== -->
                            <div class="row">
                                <div class="col-md-3">
                                    <h6 class="bg-warning p-1"><i class="fas fa-lock"></i> ສັ່ງຊື້-ນຳເຂົ້າ-ຂາຍສິນຄ້າ</h6>
                                </div>
                                <div class="col-md-10">
                                    <hr>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3"></div>
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <div class="icheck-success d-inline">
                                                <input id="11" wire:model="selectedtypes" type="checkbox" value="11">
                                                <label for="11" style="color:black">ສັ່ງຊື້ສິນຄ້າ</label>
                                            </div>
                                        </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <div class="icheck-success d-inline">
                                                <input id="12" wire:model="selectedtypes" type="checkbox" value="12">
                                                <label for="12" style="color:black">ນຳເຂົ້າສິນຄ້າ</label>
                                            </div>
                                        </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <div class="icheck-success d-inline">
                                                <input id="13" wire:model="selectedtypes" type="checkbox" value="13">
                                                <label for="13" style="color:black">ຂາຍຫນ້າຮ້ານ</label>
                                            </div>
                                        </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <div class="icheck-success d-inline">
                                                <input id="14" wire:model="selectedtypes" type="checkbox" value="14">
                                                <label for="14" style="color:black">ລາຍການຂາຍອອນລາຍ</label>
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3"></div>
                            </div>
                            <!-- ========================= Start =========================== -->
                            <div class="row">
                                <div class="col-md-3">
                                    <h6 class="bg-warning p-1"><i class="fas fa-lock"></i> ລາຍງານ</h6>
                                </div>
                                <div class="col-md-10">
                                    <hr>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3"></div>
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <div class="icheck-success d-inline">
                                                <input id="15" wire:model="selectedtypes" type="checkbox" value="15">
                                                <label for="15" style="color:black">ລາຍງານການສັ່ງຊື້</label>
                                            </div>
                                        </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <div class="icheck-success d-inline">
                                                <input id="16" wire:model="selectedtypes" type="checkbox" value="16">
                                                <label for="16" style="color:black">ລາຍງານການຂາຍ</label>
                                            </div>
                                        </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <div class="icheck-success d-inline">
                                                <input id="17" wire:model="selectedtypes" type="checkbox" value="17">
                                                <label for="17" style="color:black">ລາຍງານຂໍ້ມູນສິນຄ້າ</label>
                                            </div>
                                        </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <div class="icheck-success d-inline">
                                                <input id="18" wire:model="selectedtypes" type="checkbox" value="18">
                                                <label for="18" style="color:black">ລາຍງານລາຍຮັບ</label>
                                            </div>
                                        </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <div class="icheck-success d-inline">
                                                <input id="19" wire:model="selectedtypes" type="checkbox" value="19">
                                                <label for="19" style="color:black">ລາຍງານລາຍຈ່າຍ</label>
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3"></div>
                            </div>
                            <!-- ========================= Start =========================== -->
                            <div class="row">

                                <div class="col-md-3">
                                    <h6 class="bg-warning p-1"><i class="fas fa-lock"></i> ຜູ້ໃຊ້ & ສິດທິ</h6>
                                </div>
                                <div class="col-md-10">
                                    <hr>
                                </div>
                            </div>
                            {{-- @if ($users == 'true') --}}
                            <div class="row">
                                <div class="col-md-3"></div>
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <div class="icheck-success d-inline">
                                                <input id="20" wire:model="selectedtypes" type="checkbox" value="20">
                                                <label for="20">ຜູ້ໃຊ້</label>
                                            </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <div class="icheck-success d-inline">
                                                <input id="21" wire:model="selectedtypes" type="checkbox" value="21">
                                                <label for="21">ສິດທິ</label>
                                            </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                -
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                -
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3"></div>
                            </div>
                            {{-- @endif --}}

                            <!-- ========================= Start =========================== -->
                            <div class="row">
                                <div class="col-md-3">

                                    <h6 class="bg-warning p-1"><i class="fas fa-lock"></i> ຂໍ້ມູນເວບໄຊ & ກ່ຽວກັບ
                                    </h6>
                                </div>
                                <input id="" wire:model="users" type="checkbox" value="true"> ເພີ່ມເຕີມ
                                <div class="col-md-10">
                                    <hr>
                                </div>
                            </div>
                            @if ($users == 'true')
                                <div class="row">
                                    <div class="col-md-3"></div>
                                    <div class="col-md-8">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <div class="icheck-success d-inline">
                                                    <input id="22" wire:model="selectedtypes" type="checkbox" value="22">
                                                    <label for="22">ສະໄລຮູບພາບ</label>
                                                </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <div class="icheck-success d-inline">
                                                    <input id="23" wire:model="selectedtypes" type="checkbox" value="23">
                                                    <label for="23">ໂພດສາທາລະນະ</label>
                                                </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <div class="icheck-success d-inline">
                                                    <input id="24" wire:model="selectedtypes" type="checkbox" value="24">
                                                    <label for="24">ກ່ຽວກັບບໍລິສັດ</label>
                                                </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    -
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3"></div>
                                </div>
                            @endif
                        </div>
                        <div class="card-footer">
                            <div class="d-flex justify-content-between md-3">
                                <button type="button" wire:click="back" class="btn btn-warning"><i
                                        class="fas fa-reply-all"></i> ກັບຄືນ</button>
                                <button type="button" wire:click="update" class="btn btn-success"><i
                                        class="fas fa-save"></i> {{ __('lang.save') }}</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

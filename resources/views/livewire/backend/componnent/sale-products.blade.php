<div wire:poll>
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3> <small><i class="fa fa-laptop"></i> ຂາຍຫນ້າຮ້ານ</small> </h3>
                </div>
                <div class="title_right">
                    <div class="col-md-6 col-sm-5   form-group">
                        <select wire:model="product_type_id" class="form-control">
                            <option value="" selected>
                                ----- ຄົ້ນຫາປະເພດສິນຄ້າ -----
                            </option>
                            @foreach ($product_type as $item)
                                <option value="{{ $item->id }}">
                                    {{ $item->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-5 col-sm-5   form-group">
                        <div class="input-group">
                            <input wire:model='search' type="text" class="form-control" placeholder="ຊອກຫາ...">
                        </div>
                    </div>

                </div>
            </div>
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12">
                    <div class="x_panel">
                        <div class="x_title bg-info text-white">
                            <h2><i class="fa fa-cart-plus"></i> ສິນຄ້າທັງໝົດໃນສາງ</h2>
                            {{-- <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                        aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                        <a class="dropdown-item" href="#">Settings 1</a>
                                        <a class="dropdown-item" href="#">Settings 2</a>
                                    </div>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul> --}}
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">

                            <div class="row">
                                <div class="col-md-8 col-sm-8">
                                    @foreach ($products as $items)
                                        <div class="col-md-55">
                                            <div style="height: auto;" class="thumbnail">
                                                <div class="image view view-first">
                                                    <img style="width: 100%; display: block;" src="{{ asset('public/'.$items->image) }}"
                                                        alt="image" />
                                                    <div class="mask">
                                                        <p>{{ $items->name }} ({{ $items->code }})</p>
                                                        <div class="tools tools-bottom">
                                                            <a href="#"><i class="fa fa-link"></i></a>
                                                            <a href="#"><i class="fa fa-pencil"></i></a>
                                                            <a href="#"><i class="fa fa-times"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="caption">
                                                    <p>{{ $items->code }}</p>
                                                    <p class="text-bold">{{ $items->name }}</p>
                                                    @if ($items->qty > 0)
                                                        <p class="text-success">ສະຕ໋ອກ: {{ $items->qty }}</p>
                                                    @else
                                                        <p class="text-danger">ສະຕ໋ອກ: - {{ $items->qty }}</p>
                                                    @endif
                                                    <p class="text-danger">ລາຄາ: {{ number_format($items->sell_price) }}
                                                        ₭</p>
                                                    @if ($cartData->where('id', $items->id)->count() > 0)
                                                        <button type="buttonn" class="btn btn-warning btn-sm">
                                                            <i class="fa fa-check-circle"></i> ຢູ່ໃນກະຕ່າ
                                                        </button>
                                                    @else
                                                        @if ($items->qty > 0)
                                                            <button type="button"
                                                                wire:click='addCart({{ $items->id }})'
                                                                class="btn btn-success btn-sm">
                                                                <i class="fa fa-cart-plus"></i> ໃສ່ກະຕ່າ
                                                            </button>
                                                        @else
                                                            <button disabled type="button"
                                                                wire:click='addCart({{ $items->id }})'
                                                                class="btn btn-danger btn-sm">
                                                                <i class="fa fa-cart-plus"></i> ໃສ່ກະຕ່າ
                                                            </button>
                                                        @endif
                                                    @endif
                                                </div>


                                            </div>
                                        </div>
                                    @endforeach
                                    <div class="float-right">
                                        {{ $products->links() }}
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4">
                                    <div class="x_panel">
                                        <div class="x_title">
                                            <h2><i class="fa fa-cart-plus"></i> ລາຍການສິນຄ້າທີ່ເລືອກ</h2>
                                            {{-- <ul class="nav navbar-right panel_toolbox">
                                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                                </li>
                                                <li class="dropdown">
                                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"
                                                        role="button" aria-expanded="false"><i
                                                            class="fa fa-wrench"></i></a>
                                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                        <a class="dropdown-item" href="#">Settings 1</a>
                                                        <a class="dropdown-item" href="#">Settings 2</a>
                                                    </div>
                                                </li>
                                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                                </li>
                                            </ul> --}}
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="x_content">
                                            <table class="table table-borderless">
                                                <thead>
                                                    <tr class="bg-light">
                                                        <th>No</th>
                                                        <th>ສິນຄ້າ</th>
                                                        <th>ລາຄາ</th>
                                                        <th>ຈຳນວນ</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @if ($cartData == null)
                                                        <tr>
                                                            <td colspan="3" class="text-center">ບໍ່ມີຂໍ້ມູນ</td>
                                                        </tr>
                                                    @else
                                                        @foreach ($cartData as $item)
                                                            @if (isset($item->id))
                                                                <tr>
                                                                    <td>{{ $no1++ }}</td>
                                                                    <td>{{ $item->name }}</td>
                                                                    <td>{{ number_format($item->price) }} ₭</td>
                                                                    <td>
                                                                        <button
                                                                            wire:click="updateCart({{ $item->qty }},'{{ $item->rowId }}', {{ -1 }})"
                                                                            class="btn btn-primary btn-sm">
                                                                            <i class="fa fa-minus-square"></i>
                                                                        </button>
                                                                        {{ $item->qty }}
                                                                        <button
                                                                            wire:click="updateCart({{ $item->qty }},'{{ $item->rowId }}', {{ 1 }})"
                                                                            class="btn btn-primary btn-sm">
                                                                            <i class="fa fa-plus-square"></i>
                                                                        </button>
                                                                    </td>
                                                                </tr>
                                                            @endif
                                                        @endforeach
                                                    @endif
                                                </tbody>
                                            </table>

                                            <p class="float-right">
                                                <button wire:click='removeCart' type="button"
                                                    class="btn btn-warning btn-sm"><i class="fa fa-times-circle"></i>
                                                    ລຶບລ້າງ</button>
                                                <button wire:click='_checkout' type="button"
                                                    class="btn btn-success btn-sm">ຢືນຍັນຕໍ່ໄປ <i
                                                        class="fa fa-arrow-right"></i></button>
                                            </p>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div wire:ignore.self id="modala" class="modal fade bs-example-modal-lg" tabindex="-1"
                            role="dialog" aria-hidden="true">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title" id="myModalLabel"><i class="fa fa-cart-plus"></i>
                                            ລາຍລະອຽດການຂາຍ</h4>
                                        {{-- <button type="button" class="close" data-dismiss="modal"><span
                                                aria-hidden="true">×</span>
                                        </button> --}}
                                        <div class="row text-right">
                                            <div class="col-sm-12">
                                                {{-- <label><i class="fa fa-money-bill"></i> ປະເພດຊຳລະ</label> --}}
                                                <div class="form-group clearfix">
                                                    <div class="icheck-success d-inline">
                                                        <input type="radio" id="radioPrimary1" value="cod" wire:model="mode"
                                                            checked>
                                                        <label for="radioPrimary1" class="text-success">ເງິນສົດ
                                                        </label>
                                                    </div>
                                                    {{-- </div>
                                                <div class="form-group clearfix"> --}}
                                                    <div class="icheck-success d-inline">
                                                        <input type="radio" id="radioPrimary2" value="onepay" wire:model="mode">
                                                        <label for="radioPrimary2" class="text-danger">ເງິນໂອນ
                                                        </label>
                                                    </div>
                                                </div>
                                                @error('gender')
                                                    <span style="color: red" class="error">{{ $message }}</span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-md-12 col-sm-8">
                                                <div class="x_panel">
                                                    <div class="x_content">
                                                        <section class="content invoice">
                                                            <!-- title row -->
                                                            @php
                                                                $num = 1;
                                                            @endphp
                                                            <div class="row">
                                                                <div class=" table">
                                                                    <table class="table table-striped">
                                                                        <thead>
                                                                            <tr class="bg-success text-white">
                                                                                <th>ລຳດັບ</th>
                                                                                <th>ສິນຄ້າ</th>
                                                                                <th>ລາຄາ</th>
                                                                                <th>ຈຳນວນ</th>
                                                                                <th>ເປັນເງິນ</th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                            @foreach ($cartData as $item)
                                                                                @if (isset($item->id))
                                                                                    <tr>
                                                                                        <td>{{ $num++ }}</td>
                                                                                        <td>{{ $item->name }}</td>
                                                                                        <td>{{ number_format($item->price) }}
                                                                                            ₭</td>
                                                                                        <td>{{ $item->qty }}</td>
                                                                                        <td>{{ number_format($item->price * $item->qty) }}
                                                                                            ₭
                                                                                        </td>
                                                                                    </tr>
                                                                                @endif
                                                                            @endforeach
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                                <!-- /.col -->
                                                            </div>
                                                            <!-- /.row -->

                                                            <div class="row">
                                                                <div class="col-md-7">
                                                                    <div class="table-responsive">
                                                                        <table class="table">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <th>
                                                                                        <h4>ລວມຈຳນວນ:</h4>
                                                                                    </th>
                                                                                    <td>
                                                                                        <h4>{{ $cartCount }} ລາຍການ
                                                                                        </h4>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <th>
                                                                                        <h4>ລວມເປັນເງິນ:</h4>
                                                                                    </th>
                                                                                    <td>
                                                                                        <h4>{{ $cartTotal }} ₭</h4>
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                                <!-- /.col -->
                                                            </div>
                                                            <!-- /.row -->
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer justify-content-between bg-light">
                                        <button type="button" class="btn btn-danger" data-dismiss="modal"><i
                                                class="fa fa-times-circle"></i> ຍົກເລີກ</button>
                                        <button wire:click='_sale' class="btn btn-success pull-right"><i
                                                class="fa fa-credit-card"></i>
                                            ຍືນຍັນຊຳລະ</button>
                                    </div>
                                </div>     
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    @push('scripts')
        <script>
            window.addEventListener('showforma', event => {
                $('#modala').modal('show');
            });
            window.addEventListener('closeforma', event => {
                $('#modala').modal('hide');
            });
            $(function() {
                $('.select2').select2()
                $('#customerId').on('change', function(e) {
                    let data = $(this).val();
                    @this.set('customer', data);
                });
            });
        </script>
    @endpush

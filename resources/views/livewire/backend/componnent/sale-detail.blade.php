<!-- Main content -->
<div wire:ignore.self class="modal fade" id="modal-bill">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header bg-light">
                <h5 class="modal-title"><i class="fa fa-file"></i> <b>ລາຍລະອຽດໃບບິນ</b></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="invoice p-3 mb-3">
                    <!-- title row -->
                    <div class="row">
                        <div class="col-12">
                            <h4>
                                <small class="float-left">
                                    <p class="brand-link">
                                        <img style="width: 100px;"
                                            src="https://upload.wikimedia.org/wikipedia/commons/thumb/7/79/Aktenzeichen_XY_Logo_2014.svg/1024px-Aktenzeichen_XY_Logo_2014.svg.png"
                                            alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
                                            style="opacity: .8"> ຮ້ານ: ຂາຍເຄື່ອງໄຊໂຢ
                                    </p>
                                </small>
                                <small style="font-size: 16px" class="float-right text-sm"><b>ວັນທີ່:</b>
                                    {{ date('d-m-Y', strtotime($created_at)) }} <br> <b>ເວລາ:</b>
                                    {{ date('H:i:s', strtotime($created_at)) }}</small>
                            </h4>
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- info row -->
                    <div class="row invoice-info">
                        <div class="col-sm-6 invoice-col">
                            <address>
                                <strong><i class="fa fa-user"></i> ພະນັກງານ</strong><br>
                                @if (!empty($this->employee_data))
                                    <b>ຊື່:</b> {{ $this->employee_data->name }} {{ $this->employee_data->lastname }}
                                    <br>
                                    <b>ເບີໂທ:</b> {{ $this->employee_data->phone }} <br>
                                    <b>ອີເມວ:</b> {{ $this->employee_data->email }} <br>
                                    <b>ທີ່ຢູ່:</b>
                                    @if (!empty($this->employee_data->province->name_la))
                                        {{ $this->employee_data->village->name_la }},
                                        {{ $this->employee_data->district->name_la }},
                                        {{ $this->employee_data->province->name_la }} <br>
                                    @endif
                                @endif
                            </address>
                        </div>
                        <!-- /.col -->
                        <div class="col-sm-6 invoice-col">
                            <address>
                                <strong><i class="fa fa-user"></i> ລູກຄ້າ</strong><br>
                                @if (!empty($this->customer_data))
                                    <b>ຊື່:</b> {{ $this->customer_data->name }} {{ $this->customer_data->lastname }}
                                    <br>
                                    <b>ເບີໂທ:</b> {{ $this->customer_data->phone }} <br>
                                    <b>ອີເມວ:</b> {{ $this->customer_data->email }} <br>
                                    <b>ທີ່ຢູ່:</b>
                                    @if (!empty($this->customer_data->province->name_la))
                                        {{ $this->customer_data->village->name_la }},
                                        {{ $this->customer_data->district->name_la }},
                                        {{ $this->customer_data->province->name_la }} <br>
                                    @endif
                                @endif
                            </address>
                        </div>
                    </div>
                    <!-- /.row -->

                    <!-- Table row -->
                    <div class="row">
                        <div class="col-12 table-responsive">
                            <table class="table table-striped">
                                <thead class="bg-light">
                                    <tr>
                                        <th>ລຳດັບ</th>
                                        <th>ສິນຄ້າ</th>
                                        <th>ປະເພດ</th>
                                        <th>ລາຄາ</th>
                                        <th>ຈຳນວນ</th>
                                        <th>ເປັນເງິນ</th>
                                    </tr>
                                </thead>
                                @php
                                    $num = 1;
                                @endphp
                                <tbody>
                                    @foreach ($this->sale_detail_data as $item)
                                        @if (!empty($item->products))
                                            <tr>
                                                <td>{{ $num++ }}</td>
                                                <td class="text-bold">{{ $item->products->name }}</td>
                                                <td>
                                                    @if (!empty($item->products->product_type))
                                                        {{ $item->products->product_type->name }}
                                                    @endif
                                                </td>
                                                <td>{{ number_format($item->products->sell_price) }} ₭</td>
                                                <td>{{ $item->quantity }}</td>
                                                <td>{{ number_format($item->products->sell_price * $item->quantity) }} ₭</td>
                                            </tr>
                                        @endif
                                    @endforeach
                                    <tr class="bg-warning">
                                        <td>ຝາກຕົ້ນທາງ:</td>
                                        <td class="text-sm">{{ $this->origin_start }}
                                            <td colspan="1"></td>
                                            <td><i class="fa fa-arrow-right"></i></td>
                                            <td>ຮັບປາຍທາງ:</td>
                                        <td class="text-sm">{{ $this->origin_end }}
                                    </tr>
                                    <tr class="bg-light">
                                        <td colspan="4"></td>
                                        <td>ເປັນເງິນ:</td>
                                        <td class="text-sm">{{ number_format($this->subtotal) }} ₭
                                    </tr>

                                    <tr class="bg-light">
                                        <td colspan="4"></td>
                                        <td>ຄ່າຂົນສົ່ງ:</td>
                                        <td class="text-sm"> Free
                                    </tr>
                                    <tr class="bg-light">
                                        <td colspan="4"></td>
                                        <td>ອາກອນ:</td>
                                        <td class="text-sm">{{ number_format($this->tax) }} ₭
                                    </tr>
                                    <tr class="bg-light">
                                        <td colspan="4"></td>
                                        <td>ສ່ວນຫລຸດ:</td>
                                        <td class="text-sm">{{ number_format($this->discount) }} ₭
                                    </tr>
                                    <tr class="bg-light">
                                        <td colspan="4"></td>
                                        <td>ລວມເປັນເງິນ:</td>
                                        <td class="text-sm">{{ number_format($this->total) }} ₭
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /.invoice -->

<div wire:poll>
    <div class="right_col" role="main">
            {{-- ======================================== name page ====================================================== --}}
            <section class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h6><i class="fas fa-cart-plus"></i> ສັ່ງຊື້ <i class="fa fa-angle-double-right"></i>
                                ນຳເຂົ້າສິນຄ້າ</h6>
                        </div>
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">ຫນ້າຫຼັກ</a></li>
                                <li class="breadcrumb-item active">ນຳເຂົ້າສິນຄ້າ</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </section>
            <section class="content">
                <div class="container-fluid">
                    <div class="row">
                        <!--List users- table table-bordered table-striped -->
                        <div class="col-md-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    {{-- @foreach ($rolepermissions as $items)
                                                    @if ($items->permissionname->name == 'action_employee') --}}
                                                    {{-- <a wire:click="create" class="btn btn-primary btn-sm"
                                                        href="javascript:void(0)"><i class="fa fa-plus-circle"></i>
                                                        ເພີ່ມໃຫມ່</a> --}}
                                                    {{-- @endif
                                                     @endforeach --}}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-5">
        
                                        </div>
                                        <div class="row">
                                            <div class="col-md-9">
                                            </div>
                                            <div class="input-group input-group-sm" style="width: 225px;">
                                                <input wire:model="search" type="text" class="form-control"
                                                    placeholder="ຄົ້ນຫາ">
        
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table class="table table-hover">
                                            <thead class="bg-light">
                                                <tr>
                                                    <th>ລຳດັບ</th>
                                                    <th>ລະຫັດບິນ</th>
                                                    <th>ລວມເປັນເງິນ</th>
                                                    <th>ຜູ້ສະຫນອງ</th>
                                                    <th>ເພດ</th>
                                                    <th>ເບີໂທ</th>
                                                    <th>ຜູ້ສ້າງ</th>
                                                    <th>ວັນທີ່ສັ່ງຊື້</th>
                                                    <th>ສະຖານະ</th>
                                                    {{-- @foreach ($rolepermissions as $items)
                                                    @if ($items->permissionname->name == 'action_import') --}}
                                                    <th>ຈັດການ</th>
                                                    {{-- @endif
                                                    @endforeach --}}
                                                </tr>
                                            </thead>
                                            @php
                                            $num = 1;
                                        @endphp
                                            <tbody>
                                               
                                                @foreach ($imports_orders as $item)
                                                    <tr>
                                                        <td>{{ $num++ }}</td>
                                                        <td><a wire:click="show_bill({{ $item->id }})" href="#">{{ $item->code }}</a></td>
                                                        <td class="text-bold">{{ number_format($item->total_money) }} ₭</td>
                                                        <td>
                                                            @if (!empty($item->supplier))
                                                                {{ $item->supplier->name }} {{ $item->supplier->lastname }}
                                                            @endif
                                                        </td>
                                                        <td>
                                                            @if (!empty($item->supplier))
                                                            @if ($item->supplier->gender == 1)
                                                                <b class="text-success">ຍິງ</b>
                                                            @elseif($item->supplier->gender == 2)
                                                                <b class="text-info">ຊາຍ</b>
                                                            @else
                                                        <td></td>
                                                @endif
                                                @endif
                                                </td>
                                                <td>
                                                    @if (!empty($item->supplier))
                                                        {{ $item->supplier->phone }}
                                                    @endif
                                                </td>
                                                <td>
                                                    @if (!empty($item->employee))
                                                        {{ $item->employee->name }}
                                                    @endif
                                                </td>
                                                <td>{{ date('d/m/Y', strtotime($item->updated_at)) }}</td>
                                                <td>
                                                    @if ($item->status == 1)
                                                        <p class="bg-warning text-center rounded"><i class="fa fa-search"></i>
                                                            ລໍຖ້າກວດສອບ</p>
                                                    @elseif($item->status == 2)
                                                        <p class="bg-success text-center text-white rounded"><i
                                                                class="fa fa-check-circle"></i> ນຳເຂົ້າສຳເລັດ</p>
                                                                @elseif($item->status == 3)
                                                        <p class="bg-danger text-center text-white rounded"><i
                                                                class="fas fa-times-circle"></i> ຖືກຍົກເລີກ</p>
                                                    @endif
                                                </td>
                                                {{-- <td>{{ date('d-m-Y', strtotime($item->created_at)) }}</td> --}}
                                                {{-- @foreach ($rolepermissions as $items)
                                                        @if ($items->permissionname->name == 'action_import') --}}
                                                <td>
                                                      @if($item->status != 2 && $item->status != 3)
                                                      <button wire:click="showorder({{ $item->id }})" type="button"
                                                        class="btn btn-primary btn-sm"><i
                                                            class="fa fa-cart-arrow-down"></i> ນຳເຂົ້າສິນຄ້າ
                                                    </button>
                                                      @endif
                                                </td>
                                                {{-- @endif
                                                        @endforeach --}}
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                        <div class="float-right">
                                            {{ $imports_orders->links() }}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            @include('livewire.backend.componnent.order-confirm-import')
            @include('livewire.backend.componnent.order-bill')
        </div>
        @push('scripts')
            <script>
                window.addEventListener('show-modal-bill', event => {
                    $('#modal-bill').modal('show');
                });
                window.addEventListener('closeforma', event => {
                    $('#modala').modal('hide');
                });
                window.addEventListener('showforma', event => {
                    $('#modala').modal('show');
                });
                window.addEventListener('closeforma', event => {
                    $('#modala').modal('hide');
                });
                $(function() {
                    $('.select2').select2()
                    $('#suplyerId').on('change', function(e) {
                        let data = $(this).val();
                        @this.set('suplyer', data);
                    });
                });
            </script>
        @endpush
    </div>
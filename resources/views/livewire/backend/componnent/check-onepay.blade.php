  <!-- /.modal-payment -->
  <div wire:ignore.self class="modal fade" id="onepay">
      <div class="modal-dialog">
          <div class="modal-content">
              <div class="modal-header bg-danger">
                  <h4 class="modal-title text-center text-white"><i class="fa fa-credit-card"></i> ກວດສອບ OnePay</h4>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span style="font-size:35px" aria-hidden="true">&times;</span>
                  </button>
              </div>
              <div class="modal-body">
                  <div class="detailpayment">
                      <div class="container-fluid">
                          <div class="row">
                              {{-- @if ($detailpayments->order->image == '0')
                @else --}}
                              <div class="col-md-12">
                                  <!-- Profile Image -->
                                  <div class="card card-primary card-outline">
                                      <img src="{{ asset('public/'.$onepay_image) }}">
                                      <!-- /.card-body -->
                                  </div>
                                  <!-- /.card -->
                                  <div class="card card-primary">
                                      <div class="card-header">
                                          <h5 class="text-center"><i class="fa fa-compare"></i> ປຽບທຽບຂໍ້ມູນລະບົບ</h5>
                                      </div>
                                      <!-- /.card-header -->
                                      <table class="table table-borderless">
                                          <tr>
                                              <td><i class="fas fa-clock mr-1"></i> ວັນທີ່ຊຳລະ</td>
                                              <td>
                                                  {{ date('d/m/Y', strtotime($this->created_at)) }}
                                                  {{ date('H:i:s', strtotime($this->created_at)) }}
                                                  {{-- @endforeach --}}
                                              </td>
                                          </tr>
                                          <tr>
                                              <td><i class="fas fa-address-card"></i> ລະຫັດສັ່ງຊື້</td>
                                              <td>
                                                  {{-- @foreach ($sales as $item) --}}
                                                  {{ $this->code }}
                                              </td>
                                          </tr>
                                          <tr>
                                              <td><i class="fas fa-money-bill"></i> ລວມເປັນເງິນ</td>
                                              <td>
                                                  {{ number_format($this->total) }} ກີບ
                                              </td>
                                          </tr>
                                          <tr>
                                              <td><i class="fa fa-check"></i> {{ __('lang.status') }}</td>
                                              <td class="text-success"> ຊໍາລະເງິນແລ້ວ <i class="fa fa-check"></i></td>
                                          </tr>
                                      </table>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>

              </div>
              <div class="modal-footer justify-content-between">
                  <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times-circle"></i>
                      ຍົກເລີກ</button>
                  <button wire:click="confirm_onepay({{ $ID }})" type="button" class="btn btn-success"><i
                          class="fa fa-check-circle"></i> ຍືນຍັນ</button>
              </div>
          </div>
      </div>
  </div>

  <!-- /.modal-success -->
  <div wire:ignore.self class="modal fade" id="modal-success">
      <div class="modal-dialog">
          <div class="modal-content">
              <div class="modal-header bg-success">
                  <h4 class="modal-title"><i class="fa fa-truck text-white"></i></h4>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                  </button>
              </div>
              <div class="modal-body text-center">
                  <input type="hidden" wire:model="ID">
                  <h4 class="text-center"><i class="fa fa-people-carry"></i> ຢືນຍັນສົ່ງສິນຄ້າເຖິງມືລູກຄ້າສຳເລັດເເລ້ວ
                  </h4>
              </div>
              <div class="modal-footer justify-content-between">
                  <button type="button" class="btn btn-danger" data-dismiss="modal">ຍົກເລີກ</button>
                  <button wire:click="UpdateSuccess({{ $ID }})" type="button"
                      class="btn btn-success">ຕົກລົງ</button>
              </div>
          </div>
      </div>
  </div>

  <!-- /.modal-cancle -->
  <div wire:ignore.self class="modal fade" id="modal-cancle">
      <div class="modal-dialog">
          <div class="modal-content">
              <div class="modal-header bg-danger">
                  <h4 class="modal-title"><i class="fa fa-times-circle text-white"></i></h4>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                  </button>
              </div>
              <div class="modal-body">
                  <input type="hidden" wire:model="ID">
                  <h4 class="text-center"><i class="fa fa-cart-plus"></i> ຍົກເລີກລາຍການສັ່ງຊື້ນີ້ {{ $this->code }}
                  </h4>
                  <div class="col-md-12">
                      <div class="form-group">
                          <input wire:model="note" type="text" placeholder="ໃສ່ເຫດຜົນທີ່ທ່ານຍົກເລີກ (ຖ້າມີ)"
                              class="form-control @error('note') is-invalid @enderror">
                          @error('note')
                              <span style="color: red" class="error">{{ $message }}</span>
                          @enderror
                      </div>
                  </div>
              </div>

              <div class="modal-footer justify-content-between">
                  <button type="button" class="btn btn-danger" data-dismiss="modal">ຍົກເລີກ</button>
                  <button wire:click="UpdateCancle({{ $ID }})" type="button"
                      class="btn btn-success">ຕົກລົງ</button>
              </div>
          </div>
      </div>
  </div>
  {{-- ===================================================== --}}
  <!-- /.modal-small-bill -->
  <div wire:ignore.self class="modal fade" id="modal-small-bill">
      <div class="modal-dialog modal-lg">
          <div class="modal-content">
              <div class="modal-header">
                <button type="button" id="print"
                class="btn btn-info btn-sm"><i class="fas fa-print"></i> ປິ່ຣນ
            </button>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                  </button>
              </div>
              <div class="modal-body text-center right_content">
                <div class="container-fluid pl-4 pr-4" style="">
                    <div class="card">
                        <div class="card-body">
                            <p>ສາທາລະນະລັດ ປະຊາທິປະໄຕ ປະຊາຊົນລາວ</p>
                            <p>ສັນຕິພາບ ເອກະລາດ ປະຊາທິປະໄຕ ເອກະພາບ ວັດທະນາ ຖາວອນ</p>
                            <ul class="nav">
                                <li class="nav-item">
                                    <img height="50"
                                        src="https://upload.wikimedia.org/wikipedia/commons/thumb/7/79/Aktenzeichen_XY_Logo_2014.svg/1024px-Aktenzeichen_XY_Logo_2014.svg.png"
                                        class="rounded-circle" alt="..."> ຮ້ານ: ຂາຍເຄື່ອງໄຊໂຢ
                                </li>
                            </ul>
                            <div class="row">
                              <div class="col-xl-5">
                                <ul class="list-unstyled">
                                  <li class="text-left"><i class="fa fa-home"></i> ບ້ານ: ຍອດງື່ມ ເມືຶອງ: ແປກ ແຂວງ: ຊຽງຂວາງ</li>
                                  <li class="text-left"><i class="fas fa-phone"></i> ໂທ: 020 55667788</li>
                                </ul>
                              </div>
                              <div class="col-xl-4">
                                <ul class="list-unstyled">
                                    @if(!empty($this->customer_data))
                                    <li class="text-left"> ລູກຄ້າ: {{ $this->customer_data->name }}</li>
                                    <li class="text-left"> ໂທ: {{ $this->customer_data->phone }}</li>
                                    @if(!empty($this->customer_data->village))
                                    <li class="text-left"> ທີ່ຢູ່: {{ $this->customer_data->village->name_la }},{{ $this->customer_data->district->name_la }},{{ $this->customer_data->province->name_la }},</li>
                                    @endif
                                    
                                    @endif
                                </ul>
                              </div>
                              <div class="col-xl-3">
                                <ul class="list-unstyled">
                                  <li class="text-right"> <span
                                    class="fw-bold">ວັນທີ່: </span>{{ date('d/m/Y', strtotime($this->created_at)) }}</li>
                                  <li class="text-right"> <span
                                      class="fw-bold">ເລກທີ່ບິນ: </span>{{ $this->code }}</li>
                                  <li class="text-right"> <span
                                      class="me-1 fw-bold">ທຸລະກຳ:</span><span class="badge bg-warning text-black fw-bold">
                                      ຈ່າຍເເລ້ວ</span></li>
                                </ul>
                              </div>
                            </div>
                            <table class="table table table-borderless">
                                <thead>
                                    <tr>
                                        <th style="text-align:center; font-size:20px;" colspan="7"
                                            scope="col">
                                            <h4><u>ໃບບິນ-ຮັບເງິນ</u></h4></th>
                                    </tr>
                                    <div class="row my-2 mx-1 justify-content-center">
                                      <table class="table table-striped table-borderless">
                                        <thead style="background-color:#84B0CA ;" class="text-white">
                                          <tr>
                                            <th scope="col">ລຳດັບ</th>
                                            <th scope="col">ສິນຄ້າ</th>
                                            <th scope="col">ລາຄາ</th>
                                            <th scope="col">ຈຳນວນ</th>
                                            <th scope="col">ເປັນເງິນ</th>
                                          </tr>
                                        </thead>
                                        <tbody>
                                          @php
                                          $i = 1;
                                      @endphp
                                      @if (!empty($this->sales_detail))
                                          @foreach ($this->sales_detail as $item)
                                              <tr>
                                                <td>{{ $i++ }}</td>
                                                  @if (!empty($item->products))
                                                      <td class="text-left">
                                                          {{ $item->products->name }}
                                                      </td>
                                                  @else
                                                      <td></td>
                                                  @endif
                                                  @if (!empty($item->products))
                                                      <td>
                                                          {{ number_format($item->products->sell_price) }} ₭
                                                      </td>
                                                  @else
                                                      <td></td>
                                                  @endif
                                                  @if (!empty($item))
                                                      <td>
                                                          {{ $item->quantity }}
                                                      </td>
                                                  @else
                                                      <td></td>
                                                  @endif
                                                  @if (!empty($item->products))
                                                      <td>
                                                          {{ number_format($item->products->sell_price * $item->quantity )}} ₭
                                                      </td>
                                                  @else
                                                      <td></td>
                                                  @endif
                                              </tr>
                                          @endforeach
                                      @endif
                                        </tbody>
                            
                                      </table>
                                    </div>
                                    <div class="row">
                                      <div class="col-xl-8">
                            
                                      </div>
                                      <div class="col-xl-3">
                                        <ul class="list-unstyled">
                                          <li class="text-left ms-3"><span class="text-black me-4">ເປັນເງິນ: </span>
                                            @if (!empty($this->subtotal))
                                            <th class="border border-1" scope="col">
                                                {{ number_format($this->subtotal) }}
                                                ₭
                                            </th>
                                        @else
                                            <th class="border border-1" scope="col">
                                                ₭
                                            </th>
                                        @endif
                                          </li>
                                          <li class="text-left ms-3 mt-2"><span class="text-black me-4">ອາກອນ: </span>
                                            @if (!empty($this->tax))
                                            <th class="border border-1" scope="col">
                                                {{ number_format($this->tax) }}
                                                ₭
                                            </th>
                                        @else
                                            <th class="border border-1" scope="col">
                                                0 ₭
                                            </th>
                                        @endif
                                          </li>
                                          <li class="text-left ms-3 mt-2"><span class="text-black me-4">ສ່ວນຫຼຸດ: </span>
                                            @if (!empty($this->discount))
                                            <th class="border border-1" scope="col">
                                               - {{ number_format($this->discount) }}
                                                ₭
                                            </th>
                                        @else
                                            <th class="border border-1" scope="col">
                                               - 0 ₭
                                            </th>
                                        @endif
                                          </li>
                                        </ul>
                                        <p class="text-left float-start"><span class="text-black me-3"> ລວມເປັນເງິນ: </span><span
                                            style="font-size: 16px;">
                                            @if (!empty($this->total))
                                            <th class="border border-1" scope="col">
                                                {{ number_format($this->total) }}
                                                ₭
                                            </th>
                                        @else
                                            <th class="border border-1" scope="col">
                                                ₭
                                            </th>
                                        @endif
                                          </span></p>
                                      </div>
                                    </div>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
          </div>
      </div>
  </div>

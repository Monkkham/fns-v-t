<div wire:poll>

    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3><small> <i class="fa fa-chart-bar"></i> ລາຍງານ <i class="fa fa-angle-double-right"></i> ຂາຍສິນຄ້າ</small> </h3>
                </div>

                <div class="title_right">
                    <div class="col-md-5 col-sm-5   form-group pull-right top_search">
                        <div class="input-group row">
                            <select wire:model='status' class="form-control">
                                <option selected value="1">ຕາມເວລາ</option>
                                <option value="day">today</option>
                                <option value="week">week</option>
                                <option value="month">month</option>
                                <option value="6month">6month</option>
                                <option value="year">year</option>
                            </select>
                            <span class="input-group-btn">
                                <button class="btn btn-default" id="print" type="button">Print!</button>
                            </span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12 col-sm-12  ">
                    <div class="x_panel">
                        <div class="x_title">
                            {{-- <h2>ລາຍງານການຂາຍສິນຄ້າ</h2> --}}
                            {{-- <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                        aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                        <a class="dropdown-item" href="#">Settings 1</a>
                                        <a class="dropdown-item" href="#">Settings 2</a>
                                    </div>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul> --}}
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <div class="row">
                                <div class="col-md-12 text-center">
                                    <h6>{{ __('lang.headding1') }}</h6>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 text-center">
                                    <h6>{{ __('lang.headding2') }}</h6>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 text-center">
                                    ======================***======================
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3 text-center">
                                    <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/7/79/Aktenzeichen_XY_Logo_2014.svg/1024px-Aktenzeichen_XY_Logo_2014.svg.png"
                                        class="brand-image-xl img-circle elevation-2" height="80" width="80">
                                </div>
                                <div class="col-md-3"></div>
                                <div class="col-md-6 text-right">
                                    <h6>ວັນທີ່ພິມ: {{ date('d/m/Y') }}</h6>
                                    <h6>ເວລາ: {{ date('H:i:s') }}</h6>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <h5 class="text-center">ຮ້ານ: ຂາຍເຄື່ອງໄຊໂຍ</h5>
                                    <h6 class="text-sm" style="font-size: 12px;"><i class="fas fa-hospital"></i> ທີ່ຕັ້ງ: ຍອດງື່ມ ເມືຶອງ: ແປກ ແຂວງ: ຊຽງຂວາງ</h6>
                                    <h6 class="text-sm" style="font-size: 13px;"><i class="fas fa-phone-alt"></i> ຕິດຕໍ່: 2078914908</h6>
                                </div>
                                <div class="col-md-3"></div>
                                <div class="col-md-6">

                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 text-center">
                                    <h3><u><b>ລາຍງານ-ຂາຍສິນຄ້າ</b></u></h3>
                                    <h5>
                                        ຕາມເວລາ:
                                        @if ($status == 'day')
                                            {{ date('d-m-Y') }}
                                        @elseif($status == 'week')
                                            {{ date('d-m-Y') }}
                                        @elseif($status == 'month')
                                            {{ date('m-Y') }}
                                        @elseif($status == 'year')
                                            {{ date('Y') }}
                                        @else
                                            {{ date('d-m-Y') }}
                                        @endif
                                    </h5>
                                </div>
                            </div>
                            <br>
                            <table class="table table-striped">
                                <thead class="bg-info text-white">
                                    <tr>
                                        <th>ລຳດັບ</th>
                                        <th>ວັນທີ່</th>
                                        <th>ເລກທີ່ບິນ</th>
                                        {{-- <th>ລາຍລະອຽດ</th> --}}
                                        <th>ຜູ້ສ້າງ</th>
                                        <th>ເປັນເງິນ</th>
                                        <th>ຮູບແບບຂາຍ</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($reportSaleProducts as $item)
                                        <tr>
                                            <th scope="row">{{ $no1++ }}</th>
                                            <td>{{ \Carbon\Carbon::parse($item->created_at)->format('d/m/Y') }}</td>
                                            <td>{{ $item->code }}</td>
                                            {{-- <td>
                                                @foreach ($reportSaleProductsItems as $item2)
                                                    @if ($item->id == $item2->sales_id)
                                                        {{ $item2->products->name }} ({{ $item2->quantity }}) <br>
                                                    @endif
                                                @endforeach
                                            </td> --}}
                                            <td>
                                                @if(!empty($item->employee))
                                                {{ $item->employee->name }}
                                                @endif
                                            </td>
                                            <td>{{ number_format($item->total) }} LAK</td>
                                            <td>
                                                @if($item->type_sales == 1)
                                                    <p class="text-danger">ຫນ້າຮ້ານ</p>
                                                    @elseif($item->type_sales == 2)
                                                    <p class="text-success">ອອນລາຍ</p>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                    <tr class="text-center">
                                        <td class="bg-light text-bold text-right" colspan="5">
                                            <i><h6>ລວມສິນຄ້າ:</h6></i>
                                        </td>
                                        <td class="text-bold bg-light text-left">
                                            <h6>{{ number_format($reportSaleProductsItems->count()) }} ລາຍການ</h6>
                                        </td>
                                    </tr>
                                    <tr class="text-center">
                                        <td class="bg-light text-bold text-right" colspan="5">
                                            <i><h6>ລວມຍອດເງິນ:</h6></i>
                                        </td>
                                        <td class="text-bold bg-light text-left">
                                            <h6>{{ number_format($reportSaleProducts->sum('total'), 2) }} LAK</h6>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@push('scripts')
    <script>
        $(document).ready(function() {
            $('#print').click(function() {
                printDiv();

                function printDiv() {
                    var printContents = $(".x_content").html();
                    var originalContents = document.body.innerHTML;
                    document.body.innerHTML = printContents;
                    window.print();
                    document.body.innerHTML = originalContents;
                }
                location.reload();
            });
        });
    </script>
@endpush

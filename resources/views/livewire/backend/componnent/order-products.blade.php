<div wire:poll>

    <div class="right_col" role="main">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h6><i class="fa fa-cart-plus"></i> ສັ່ງຊື້ສິນຄ້າ</h6>
                    </div>
                    {{-- <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">{{ __('lang.home') }}</a>
                            </li>
                            <li class="breadcrumb-item active">{{ __('lang.units') }}</li>
                        </ol>
                    </div> --}}
                </div>
            </div>
        </section>
        <div class="content">
            {{-- <div class="page-title">
                <div class="col-sm-6">
                    <h6><i class="fa fa-database"></i> ຈັດການຂໍ້ມູນ <i class="fa fa-angle-double-right"></i>
                        ສ່ວນຫຼຸດ</h6>
                </div>

                <div class="title_right">
                    <div class="col-md-5 col-sm-5   form-group pull-right top_search">
                        <div class="input-group">
                            <input wire:model='search' type="text" class="form-control" placeholder="ຊອກຫາ...">
                            <span class="input-group-btn">
                                <button class="btn btn-default" type="button">Go!</button>
                            </span>
                        </div>
                    </div>
                </div>
            </div> --}}

            <div class="row">
                <div class="col-md-12">
                    <div class="x_panel">
                        <div class="x_title">
                            {{-- <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                        aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                        <a class="dropdown-item" href="#">Settings 1</a>
                                        <a class="dropdown-item" href="#">Settings 2</a>
                                    </div>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul> --}}
                            <div class="card-header bg-light">
                                <div class="row">
                                    <div class="col-md-8">
                                        <div
                                            class="input-group input-group-sm col-md-4 col-sm-4 form-group pull-right top_search">
                                            <select wire:model="product_type_id" class="form-control">
                                                <option value="" selected>
                                                    ----- ຄົ້ນຫາປະເພດສິນຄ້າ -----
                                                </option>
                                                @foreach ($product_type as $item)
                                                <option value="{{ $item->id }}">
                                                    {{ $item->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="input-group input-group-sm col-md-4 col-sm-4 pl-2">
                                            <input wire:model="search" type="text" class="form-control"
                                                placeholder="ຄົ້ນຫາ: ຊື່, ລະຫັດ ສິນຄ້າ">
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="x_content">

                            <div class="row">
                                <div class="col-md-8 col-sm-8">
                                    @foreach ($products as $items)
                                    <div class="col-md-55">
                                        <div style="height: auto;" class="thumbnail">
                                            <div class="image view view-first">
                                                <img style="width: 100%; display: block;" src="{{ asset('public/'.$items->image) }}"
                                                    alt="image" />
                                                <div class="mask">
                                                    <p>{{ $items->name }} ({{ $items->code }})</p>
                                                    <div class="tools tools-bottom">
                                                        <a href="#"><i class="fa fa-link"></i></a>
                                                        <a href="#"><i class="fa fa-pencil"></i></a>
                                                        <a href="#"><i class="fa fa-times"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="caption">
                                                <p>{{ $items->code }}</p>
                                                <p class="text-bold">{{ $items->name }}</p>
                                                @if ($items->qty > 0)
                                                <p class="text-success">ສະຕ໋ອກ: {{ $items->qty }}</p>
                                                @else
                                                <p class="text-danger">ສະຕ໋ອກ: - {{ $items->qty }}</p>
                                                @endif
                                                <p class="text-danger">ລາຄາ: {{ number_format($items->sell_price) }}
                                                    ₭</p>
                                                @if ($cartData->where('id', $items->id)->count() > 0)
                                                <button type="buttonn" class="btn btn-warning btn-sm">
                                                    <i class="fa fa-check-circle"></i> ຢູ່ໃນກະຕ່າ
                                                </button>
                                                @else
                                                <button type="button" wire:click='addCart({{ $items->id }})'
                                                    class="btn btn-success btn-sm">
                                                    <i class="fa fa-cart-plus"></i> ເກັບໃສ່ກະຕ່າ
                                                </button>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                    <div class="float-right">
                                        {{ $products->links() }}
                                    </div>
                                </div>

                                <div class="col-md-4 col-sm-4">
                                    <div class="x_panel">
                                        <div class="x_title bg-light">
                                            <h2><i class="fa fa-cart-plus"></i> ລາຍການສິນຄ້າທີ່ເລືອກ</h2>
                                            {{-- <ul class="nav navbar-right panel_toolbox">
                                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                                </li>
                                                <li class="dropdown">
                                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"
                                                        role="button" aria-expanded="false"><i
                                                            class="fa fa-wrench"></i></a>
                                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                        <a class="dropdown-item" href="#">Settings 1</a>
                                                        <a class="dropdown-item" href="#">Settings 2</a>
                                                    </div>
                                                </li>
                                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                                </li>
                                            </ul> --}}
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="x_content">

                                            <table class="table">
                                                <thead class="bg-light">
                                                    <tr>
                                                        <th>No</th>
                                                        <th>ຊື່</th>
                                                        <th>ລາຄາ</th>
                                                        <th>ຈຳນວນ</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @if ($cartData == null)
                                                    <tr>
                                                        <td colspan="3" class="text-center">ບໍ່ມີຂໍ້ມູນ</td>
                                                    </tr>
                                                    @else
                                                    @foreach ($cartData as $item)
                                                    @if (isset($item->id))
                                                    <tr>
                                                        <td>{{ $no1++ }}</td>
                                                        <td>{{ $item->name }}</td>
                                                        <td>{{ number_format($item->price) }} ₭</td>
                                                        <td>
                                                            <button
                                                                wire:click="updateCart({{ $item->qty }},'{{ $item->rowId }}', {{ -1 }})"
                                                                class="btn btn-primary btn-sm">
                                                                <i class="fa fa-minus-square"></i>
                                                            </button>
                                                            {{ $item->qty }}
                                                            <button
                                                                wire:click="updateCart({{ $item->qty }},'{{ $item->rowId }}', {{ 1 }})"
                                                                class="btn btn-primary btn-sm">
                                                                <i class="fa fa-plus-square"></i>
                                                            </button>
                                                        </td>
                                                    </tr>
                                                    @endif
                                                    @endforeach
                                                    @endif
                                                </tbody>
                                            </table>
                                            <p class="float-right">
                                                <button wire:click='removeCart' type="button"
                                                    class="btn btn-warning btn-sm"><i class="fa fa-times-circle"></i>
                                                    ລຶບລ້າງ</button>
                                                <button wire:click='_checkout' type="button"
                                                    class="btn btn-success btn-sm">ຢືນຍັນຕໍ່ໄປ <i
                                                        class="fa fa-arrow-right"></i></button>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div wire:ignore.self id="modala" class="modal fade bs-example-modal-lg" tabindex="-1"
                            role="dialog" aria-hidden="true">
                            <div class="modal-dialog modal-xl">
                                <div class="modal-content">
                                    <div class="modal-header bg-light">
                                        <h5 class="modal-title" id="myModalLabel"><i class="fa fa-cart-plus"></i>
                                            ລາຍການສັ່ງຊື້ສິນຄ້າ</h5>
                                        <button type="button" class="close text-white" data-dismiss="modal"><span
                                                aria-hidden="true">×</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-md-7 col-sm-8">
                                                <div class="x_panel">
                                                    <div class="x_content">
                                                        <section class="content invoice">
                                                            <!-- title row -->
                                                            {{-- <div class="row">
                                                                <div class="invoice-header">
                                                                    <h4>
                                                                        <i class="fa fa-globe"></i>
                                                                        ຮ້ານ: ອາຫານດາວອັງຄານ
                                                                    </h4>
                                                                </div>
                                                                <!-- /.col -->
                                                            </div> --}}
                                                            <!-- info row -->
                                                            {{-- <div class="row invoice-info">
                                                                <div class="col-sm-12 invoice-col">
                                                                    <b>ຜູ້ພິມບິນສັ່ງຊື້: {{
                                                                        Auth::guard('admin')->user()->name }}</b>
                                                                    <br>
                                                                    <b>ວັນທີ່:</b> {{ date('d-m-Y') }}
                                                                </div>
                                                                <!-- /.col -->
                                                            </div> --}}
                                                            <!-- Table row -->
                                                            <div class="table">
                                                                <table class="table table-striped">
                                                                    <thead class="bg-success">
                                                                        <tr>
                                                                            <th>ລຳດັບ</th>
                                                                            <th>ສິນຄ້າ</th>
                                                                            <th>ລາຄາຊື້</th>
                                                                            <th>ຈຳນວນ</th>
                                                                            <th>ເປັນເງິນ</th>
                                                                        </tr>
                                                                    </thead>
                                                                    @php
                                                                    $num = 1;
                                                                    @endphp
                                                                    <tbody>
                                                                        @foreach ($cartData as $item)
                                                                        @if (isset($item->id))
                                                                        <tr>
                                                                            <td>{{ $num++ }}</td>
                                                                            <td>{{ $item->name }}</td>
                                                                            <td>{{ number_format($item->price) }}
                                                                                ₭</td>
                                                                            <td>
                                                                                <input
                                                                                    wire:change="_updateQty('{{ $item->rowId }}', {{ $item->qty }})"
                                                                                    type="number" wire:model='nas'>
                                                                            </td>
                                                                            <td>{{ number_format($item->price *
                                                                                $item->qty) }}
                                                                                ₭
                                                                            </td>
                                                                        </tr>
                                                                        @endif
                                                                        @endforeach
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                            <!-- /.col -->
                                                            {{--
                                                    </div> --}}
                                                    <!-- /.row -->

                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="table-responsive">
                                                                <table class="table">
                                                                    <tbody>
                                                                        <tr>
                                                                            <th>
                                                                                <h6 class="text-bold">ລວມຈຳນວນ:
                                                                                </h6>
                                                                            </th>
                                                                            <td>
                                                                                <h6 class="text-bold">
                                                                                    {{ $cartCount }} ລາຍການ
                                                                                </h6>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <th>
                                                                                <h5 class="text-bold">
                                                                                    ລວມເປັນເງິນ:</h5>
                                                                            </th>
                                                                            <td>
                                                                                <h5 class="text-bold">
                                                                                    {{ $cartTotal }} ₭</h5>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                        <!-- /.col -->
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-5 col-sm-4">
                                            <form class="form-horizontal">
                                                <div class="form-group row">
                                                    {{-- <div class="col-md-9 col-sm-9 "> --}}
                                                        <select wire:model='supplier_id' id="suplyerId"
                                                            class="form-control">
                                                            <option selected>----- ເລືອກຜູ້ສະຫນອງ -----</option>
                                                            @foreach ($suppliers as $item)
                                                            <option value="{{ $item->id }}">
                                                                {{ $item->name }} {{ $item->lastname }}
                                                            </option>
                                                            @endforeach
                                                        </select>
                                                        @error('supplier_id')
                                                        <span style="color: red" class="error">{{ $message }}</span>
                                                        @enderror
                                                        {{--
                                                    </div> --}}
                                                </div>
                                                <div class=" table">
                                                    <table class="table table-striped">
                                                        <thead>
                                                            @if ($suppiler_data)
                                                            @if (!empty($suppiler_data))
                                                            <tr>
                                                                <th class="bg-light">ຊື່ ນາມສະກຸນ:</th>
                                                                <th>{{ $suppiler_data->name }}
                                                                    {{ $suppiler_data->lastname }}</th>
                                                            </tr>
                                                            <tr>
                                                                <th class="bg-light">ເພດ:</th>
                                                                <th>
                                                                    @if ($suppiler_data->gender == 1)
                                                                    <span>ຍິງ</span>
                                                                    @elseif($suppiler_data->gender == 2)
                                                                    <span>ຊາຍ</span>
                                                                    @endif
                                                                </th>
                                                            </tr>
                                                            <tr>
                                                                <th class="bg-light">ເບີໂທ:</th>
                                                                <th>{{ $suppiler_data->phone }}</th>
                                                            </tr>
                                                            <tr>
                                                                <th class="bg-light">ອີເມວ:</th>
                                                                <th>{{ $suppiler_data->email }}</th>
                                                            </tr>
                                                            <tr>
                                                                <th class="bg-light">ທີ່ຢູ່:</th>
                                                                @if (!empty($suppiler_data->village))
                                                                <th>
                                                                    {{ $suppiler_data->village->name_la }},
                                                                    {{ $suppiler_data->district->name_la }},
                                                                    {{ $suppiler_data->province->name_la }}
                                                                </th>
                                                                @endif

                                                            </tr>
                                                            @endif
                                                            @endif
                                                        </thead>
                                                    </table>
                                                </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer justify-content-between bg-light">
                                    <button type="button" class="btn btn-danger" data-dismiss="modal"><i
                                            class="fa fa-times-circle"></i> ຍົກເລີກ</button>
                                    <button wire:click='_sale' type="button" class="btn btn-success"><i
                                            class="fa fa-check-circle"></i> ຢືນຍັນການຊື້</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @push('scripts')
    <script>
        window.addEventListener('showforma', event => {
                    $('#modala').modal('show');
                });
                window.addEventListener('closeforma', event => {
                    $('#modala').modal('hide');
                });
                $(function() {
                    $('.select2').select2()
                    $('#suplyerId').on('change', function(e) {
                        let data = $(this).val();
                        @this.set('suplyer', data);
                    });
                });
    </script>
    @endpush

<div wire.poll>
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3><small><i class="fa fa-list"></i> ລາຍການຂາຍອອນລາຍ</small> </h3>
                </div>
                <div class="title_right">
                    <div class="col-md-5 col-sm-5   form-group pull-right top_search">
                        <div class="input-group">
                            <input wire:model='search' type="text" class="form-control" placeholder="ຊອກຫາ...">
                            <span class="input-group-btn">
                                <button class="btn btn-default" type="button">Go!</button>
                            </span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12 col-sm-12  ">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2><i class="fa fa-users"></i> ລູກຄ້າສັ່ງຊື້ຜ່ານເວບໄຊ</h2>
                            {{-- <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li class="dropdown">
                                    <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                        aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                        <a class="dropdown-item" href="javascript:void(0)">Settings 1</a>
                                        <a class="dropdown-item" href="javascript:void(0)">Settings 2</a>
                                    </div>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul> --}}
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <table class="table table-hover">
                                <thead class="bg-light">
                                    <tr class="text-center">
                                        <th>ລຳດັບ</th>
                                        <th>ລະຫັດບິນ</th>
                                        <th>ລວມເປັນເງິນ</th>
                                        <th>ລູກຄ້າ</th>
                                        <th>ທຸລະກຳ</th>
                                        <th>ສະຖານະ</th>
                                        <th>ຮູບແບບຂາຍ</th>
                                        <th>ຈັດການ</th>
                                        <th>ວັນທີ່</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($salePendingsList as $item)
                                        <tr class="text-center">
                                            <th scope="row">{{ $no1++ }}</th>
                                            <td><a href="#"
                                                    wire:click='showorder({{ $item->id }})'>{{ $item->code }}</a>
                                            </td>
                                            <td>{{ number_format($item->total) }} ₭</td>
                                            @if (!empty($item->customer))
                                                <td><i class="fa fa-user"></i> {{ $item->customer->name }} ໂທ:
                                                    {{ $item->customer->phone }}</td>
                                            @else
                                                <td></td>
                                            @endif
                                            <td>
                                                @if ($item->mode == 'cod')
                                                    {{-- <p class="text-white rounded text-center bg-warning"><i class="fa fa-hand-holding-usd"></i> COD</p> --}}
                                                    <p class="badge badge-warning p-2"><i
                                                            class="fa fa-hand-holding-usd"></i>ເກັບ COD</p>
                                                    {{-- <button type="button" class="btn btn-sm btn-warning" title="ຊຳລະປາຍທາງ"><i class="fa fa-hand-holding-usd"></i> COD</button> --}}
                                                @elseif($item->mode == 'onepay' && $item->payment == 1)
                                                    {{-- <p class="text-white rounded text-center bg-danger">OnePay</p> --}}
                                                    <button type="button" wire:click="show_onepay({{ $item->id }})"
                                                        class="btn btn-sm btn-outline-danger"><i
                                                            class="fa fa-credit-card"></i> OnePay</button>
                                                @elseif($item->mode == 'onepay' && $item->payment == 0)
                                                    <p class="badge badge-success p-2"><i
                                                            class="fa fa-check-circle"></i> ຈ່າຍເເລ້ວ</p>
                                                @endif
                                            </td>
                                            <td>
                                                @if ($item->status == 1)
                                                    <p class="badge badge-success p-2"><i class="fa fa-plus-circle"></i>
                                                        ໃຫມ່
                                                        <span
                                                            class="spinner-grow spinner-grow-sm text-white text-center"
                                                            role="status" aria-hidden="true"></span>
                                                    </p>
                                                @elseif($item->status == 2)
                                                    <p class="badge badge-warning p-2"><i class="fa fa-truck"></i>
                                                        ກຳລັງສົ່ງ<span
                                                        class="spinner-grow spinner-grow-sm text-white text-center"
                                                        role="status" aria-hidden="true"></span></p>
                                                @elseif($item->status == 3)
                                                    <p class="badge badge-info p-2"><i class="fa fa-check-circle"></i>
                                                        ສົ່ງສຳເລັດ</p>
                                                @elseif($item->status == 4)
                                                    <p class="badge badge-danger p-2"><i class="fa fa-times-circle"></i>
                                                        ຍົກເລີກ</p>
                                                @endif
                                            </td>
                                            <td>
                                                @if ($item->type_sales == 1)
                                                    <p class="text-danger">ຫນ້າຮ້ານ</p>
                                                @elseif($item->type_sales == 2)
                                                    <p class="text-success">ຜ່ານເວບໄຊ</p>
                                                @endif
                                            </td>
                                            <td>
                                                <div class="btn-group">
                                                    <button type="button"
                                                        class="btn btn-info btn-sm dropdown-toggle dropdown-icon"
                                                        data-toggle="dropdown">
                                                        {{-- {{__('lang.button')}} --}} <i class="fa fa-ellipsis-h"></i>
                                                    </button>
                                                    <div class="dropdown-menu" role="menu">
                                                        @if ($item->payment != 1 && $item->status != 2 && $item->status != 3 && $item->status != 4)
                                                            <li><a class="dropdown-item" wire:click="ShowSending({{ $item->id }})" href="javascript:void(0)"><i
                                                                        class="fa fa-truck text-warning"></i>
                                                                    ຍືນຍັນຈັດສົ່ງ</a></li>
                                                            <li>
                                                                <hr class="dropdown-divider">
                                                            </li>
                                                        @endif
                                                        @if ($item->status != 1 && $item->status != 3 && $item->status != 4)
                                                            <li><a wire:click="ConfirmSuccess({{ $item->id }})"
                                                                    class="dropdown-item" href="javascript:void(0)"><i
                                                                        class="fa fa-check-circle text-success"></i>
                                                                    ຍືນຍັນສົ່ງສຳເລັດ</a></li>
                                                            <li>
                                                                <hr class="dropdown-divider">
                                                            </li>
                                                        @endif
                                                        @if ($item->status != 2 && $item->status != 3 && $item->status != 4)
                                                            <li><a wire:click="showCancle({{ $item->id }})"
                                                                    class="dropdown-item" href="javascript:void(0)"><i
                                                                        class="fa fa-times-circle text-danger"></i>
                                                                    ຍົກເລີກ</a></li>
                                                            <li>
                                                                <hr class="dropdown-divider">
                                                            </li>
                                                        @endif
                                                        <li><a wire:click='show_small_bill({{ $item->id }})'
                                                                class="dropdown-item" href="javascript:void(0)"><i
                                                                    class="fa fa-print text-info"></i> ພິມໃບບິນ</a></li>
                                                        {{-- <li><a wire:click='showorder({{ $item->id }})'
                                                                class="dropdown-item" href="javascript:void(0)"><i
                                                                    class="fa fa-print text-info"></i> ລາຍລະອຽດ</a></li> --}}
                                                    </div>
                                                </div>
                                            </td>
                                            <td>{{ date('d/m/Y', strtotime($item->created_at)) }}
                                                {{ date('h:i:s', strtotime($item->created_at)) }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <div class="float-right">
                                {{ $salePendingsList->links() }}
                            </div>

                            @include('livewire.backend.componnent.sale-detail')
                            @include('livewire.backend.componnent.sale-send-product')


                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    @include('livewire.backend.componnent.check-onepay')
</div>
@push('scripts')
    <script>
        window.addEventListener('show-modal-bill', event => {
            $('#modal-bill').modal('show');
        });
        window.addEventListener('showforma', event => {
            $('#modala').modal('show');
        });
        window.addEventListener('closeforma', event => {
            $('#modala').modal('hide');
        });
        //============
        window.addEventListener('show-onepay', event => {
            $('#onepay').modal('show');
        });
        window.addEventListener('hide-onepay', event => {
            $('#onepay').modal('hide');
        });
        window.addEventListener('show-modal-success', event => {
            $('#modal-success').modal('show');
        });
        window.addEventListener('hide-modal-success', event => {
            $('#modal-success').modal('hide');
        });
        window.addEventListener('show-modal-cancle', event => {
            $('#modal-cancle').modal('show');
        });
        window.addEventListener('hide-modal-cancle', event => {
            $('#modal-cancle').modal('hide');
        });
        // =============
        window.addEventListener('show-modal-small-bill', event => {
            $('#modal-small-bill').modal('show');
        });
        // =====================
        window.addEventListener('show-modal-send-product', event => {
            $('#modal-send-product').modal('show');
        });
        window.addEventListener('hide-modal-send-product', event => {
            $('#modal-send-product').modal('hide');
        });
        // ======== print ========= //
        $(document).ready(function() {

            $('#print').click(function() {
                printDiv();

                function printDiv() {
                    var printContents = $(".right_content").html();
                    var originalContents = document.body.innerHTML;
                    document.body.innerHTML = printContents;
                    window.print();
                    document.body.innerHTML = originalContents;
                }
                location.reload();
            });
        });
    </script>
@endpush

    <!-- /.modal-send-product -->
    <div wire:ignore.self class="modal fade" id="modal-send-product">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header bg-danger">
                    <h4 class="text-white"><i class="fa fa-truck text-white"></i> ຈັດສົ່ງສິນຄ້າໃຫ້ລູກຄ້າ</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label><i class="fa fa-truck"></i> ຝາກຕົ້ນທາງ (ບໍລິສັດຂົນສົ່ງ ເເລະ ສາຂາ)</label>
                            <input wire:model="origin_start" type="text" placeholder="ປ້ອນຂໍ້ມູນ"
                                class="form-control @error('origin_start') is-invalid @enderror">
                            @error('origin_start')
                                <span style="color: red" class="error">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label><i class="fa fa-home"></i> ຮັບປາຍທາງ (ບໍລິສັດຂົນສົ່ງ ເເລະ ສາຂາ)</label>
                            <input wire:model="origin_end" type="text" placeholder="ປ້ອນຂໍ້ມູນ"
                                class="form-control @error('origin_end') is-invalid @enderror">
                            @error('origin_end')
                                <span style="color: red" class="error">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">ຍົກເລີກ</button>
                    <button wire:click="ConfirmSending({{ $ID }})" type="button"
                        class="btn btn-success">ຍືນຍັນຈັດສົ່ງ</button>
                </div>
            </div>
        </div>
    </div>
<div wire:poll>
    <div class="right_col" role="main">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h6><i class="fa fa-bar-chart-o"></i>
                            ລາຍງານ
                            <i class="fa fa-angle-double-right"></i>
                            ລາຍງານການຊື້
                        </h6>
                    </div>
                    {{-- <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">ຫນ້າຫຼັກ</a></li>
                        <li class="breadcrumb-item active">ການຊື້</li>
                    </ol>
                </div> --}}
                </div>
            </div>
        </section>
        <section class="content pb-2">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <input type="date" wire:model="start" class="form-control">
                                        </div>
                                    </div><!-- end div-col -->
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <input type="date" wire:model="end" class="form-control">
                                        </div>
                                    </div><!-- end div-col -->
                                    {{-- @foreach ($rolepermissions as $items)
                                @if ($items->permissionname->name == 'action_report_order') --}}
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <button wire:click="sub()" class="btn btn-primary"><i
                                                    class="fas fa-file-pdf"></i> ສະເເດງ</button>
                                            <button class="btn btn-info" id="print"><i class="fas fa-print"></i>
                                                ປິ່ຣນ</button>
                                        </div>
                                    </div><!-- end div-col -->
                                    {{-- @endif
                                @endforeach --}}
                                </div><!-- end div-row -->
                                <hr>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="right_content">
                                            <div class="row">
                                                <div class="col-md-12 text-center">
                                                    <h6>ສາທາລະນະລັດ ປະຊາທິປະໄຕ ປະຊາຊົນລາວ ສັນຕິພາບ ເອກະລາດ</h6>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12 text-center">
                                                    <h6>ປະຊາທິປະໄຕ ເອກະພາບ ວັດທະນາ ຖາວອນ</h6>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12 text-center">
                                                    ===========***===========
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-3 text-center">
                                                    <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/7/79/Aktenzeichen_XY_Logo_2014.svg/1024px-Aktenzeichen_XY_Logo_2014.svg.png"
                                                        class="brand-image-xl img-circle elevation-2" height="80"
                                                        width="80">
                                                </div>
                                                <div class="col-md-3"></div>
                                                <div class="col-md-6 text-right">
                                                <h6>ເລກທີ: {{ $this->num_code }}</h6>
                                                    <h6>ວັນທີ່ພິມ: {{ date('d/m/Y') }}</h6>
                                                    <h6>ເວລາ: {{ date('H:i:s') }}</h6>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <h5 class="text-center">ຮ້ານ ຂາຍເຄື່ອງໄຊໂຢ</h5>
                                                    <h6 class="text-sm"><i class="fa fa-phone"></i> ຕິດຕໍ່:
                                                        020 97241895
                                                        <h6 class="text-sm"><i class="fa fa-envelope"></i> ອີເມວ:
                                                            xaiyo@gmail.com
                                                            <h6 class="text-sm"><i class="fa fa-home"></i>
                                                                ບ້ານຍອດງື່ມ, ເມືອງແປກ, ແຂວງຊຽງຂວາງ</h6>
                                                </div>
                                                <div class="col-md-3"></div>
                                                <div class="col-md-6">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12 text-center">
                                                    <h4><u><b>ລາຍງານ-ການຊື້</b></u></h4>
                                                    <h4><b>ວັນທີ່:
                                                            @if (!empty($starts))
                                                                {{ date('d-m-Y', strtotime($starts)) }}
                                                            @endif
                                                            ຫາ ວັນທີ່:
                                                            @if (!empty($ends))
                                                                {{ date('d-m-Y', strtotime($ends)) }}
                                                            @endif
                                                        </b></h4>
                                                </div>
                                            </div>
                                            <br>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <table class="table table-striped table-hover">
                                                        <thead>
                                                            <tr class="text-center text-white bg-info text-bold">
                                                                <th>ລຳດັບ</th>
                                                                <th>ວັນທີ່</th>
                                                                <th>ລະຫັດ</th>
                                                                <th>ຜູ້ສະຫນອງ</th>
                                                                <th>ຜູ້ຈັດຊື້</th>
                                                                <th>ເປັນເງິນ</th>
                                                                <th>ສະຖານະ</th>
                                                                <th>ການຊຳລະ</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            @php $no = 1 @endphp
                                                            @foreach ($orders as $item)
                                                                <tr class="text-center">
                                                                    <td>{{ $no++ }}</td>
                                                                    <td>{{ date('d/m/Y', strtotime($item->created_at)) }}
                                                                    </td>
                                                                    <td>
                                                                        <a href="javascript:void(0)"
                                                                            wire:click="ShowDetail({{ $item->id }})">
                                                                            {{ $item->code }}</a>
                                                                    </td>
                                                                    <td>
                                                                        @if (!empty($item->supplier))
                                                                            {{ $item->supplier->name }}
                                                                            {{ $item->supplier->lastname }}
                                                                        @endif
                                                                    </td>
                                                                    <td>
                                                                        @if (!empty($item->employee))
                                                                            {{ $item->employee->name }}
                                                                            {{ $item->employee->lastname }}
                                                                        @endif
                                                                    </td>
                                                                    <td>{{ number_format($item->total_money) }} ₭</td>
                                                                    <td>
                                                                        @if ($item->status == 1)
                                                                            <p class="text-center text-warning">
                                                                                ລໍຖ້າກວດສອບ </p>
                                                                        @elseif($item->status == 2)
                                                                            <p class="text-success text-center">
                                                                                ນຳເຂົ້າສຳເລັດ</p>
                                                                        @elseif($item->status == 3)
                                                                            <p class="text-danger text-center">
                                                                                ຖືກຍົກເລີກ</p>
                                                                        @endif
                                                                    </td>
                                                                    <td>
                                                                        @if ($item->payment == 1)
                                                                            <p class="text-center text-danger">
                                                                                ບໍ່ທັນຊຳລະ </p>
                                                                        @elseif($item->payment == 2)
                                                                            <p class="text-success text-center">
                                                                                ຊຳລະເເລ້ວ</p>
                                                                        @endif
                                                                    </td>
                                                                </tr>
                                                            @endforeach
                                                            <tr class="text-center">
                                                                <td class="bg-light text-bold text-right"
                                                                    colspan="5">
                                                                    <i><h6>ລວມເງິນທັງໝົດ</h6></i>
                                                                </td>
                                                                <td class="bg-light text-bold">
                                                                   <h6> {{ number_format($sum_total) }} ₭</h6>
                                                                </td>
                                                                <td class="bg-light text-bold text-right"
                                                                    colspan="5">
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div><!-- end card body-->
                        </div><!-- end card -->
                    </div>
                </div>
            </div>
        </section>
        @include('livewire.backend.reports.order-detail')
    </div>
</div>
@push('scripts')
    <script>
        window.addEventListener('show-modal-detail', event => {
            $('#modal-detail').modal('show');
        });
    </script>
    <script>
        $(document).ready(function() {
            $('#print').click(function() {
                printDiv();

                function printDiv() {
                    var printContents = $(".right_content").html();
                    var originalContents = document.body.innerHTML;
                    document.body.innerHTML = printContents;
                    window.print();
                    document.body.innerHTML = originalContents;
                }
                location.reload();
            });
        });
    </script>
@endpush

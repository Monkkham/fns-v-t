<main class="main">
    <div class="page-header text-center" style="background-image: linear-gradient(#246dea, #11dfcb">
        <div class="container">
            <h3 class="page-title text-white"><i class="fas fa-store"></i> ຮ້ານຄ້າ</h3>
        </div><!-- End .container -->
    </div><!-- End .page-header -->
    <nav aria-label="breadcrumb" class="breadcrumb-nav mb-2">
        <div class="container">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="index.html">ຫນ້າຫຼັກ</a></li>
                <li class="breadcrumb-item"><a href="#">ຮ້ານຄ້າ</a></li>
                <li class="breadcrumb-item active" aria-current="page">ສິນຄ້າ</li>
            </ol>
        </div><!-- End .container -->
    </nav><!-- End .breadcrumb-nav -->

    <div class="page-content">
        <div class="container">
            <div class="row">
                <div class="col-lg-9">
                    <div class="toolbox">
                        <div class="toolbox-left">
                            <div class="toolbox-info">
                                Showing <span>9 of 56</span> Products
                            </div><!-- End .toolbox-info -->
                        </div><!-- End .toolbox-left -->

                        <div class="toolbox-right">
                            <div class="toolbox-sort">
                                <label for="sortby">Sort by:</label>
                                <div class="select-custom">
                                    <select name="sortby" id="sortby" class="form-control">
                                        <option value="popularity" selected="selected">Most Popular</option>
                                        <option value="rating">Most Rated</option>
                                        <option value="date">Date</option>
                                    </select>
                                </div>
                            </div><!-- End .toolbox-sort -->
                            <div class="toolbox-layout">
                                <a href="category-list.html" class="btn-layout active">
                                    <svg width="16" height="10">
                                        <rect x="0" y="0" width="4" height="4" />
                                        <rect x="6" y="0" width="10" height="4" />
                                        <rect x="0" y="6" width="4" height="4" />
                                        <rect x="6" y="6" width="10" height="4" />
                                    </svg>
                                </a>

                                <a href="category-2cols.html" class="btn-layout">
                                    <svg width="10" height="10">
                                        <rect x="0" y="0" width="4" height="4" />
                                        <rect x="6" y="0" width="4" height="4" />
                                        <rect x="0" y="6" width="4" height="4" />
                                        <rect x="6" y="6" width="4" height="4" />
                                    </svg>
                                </a>

                                <a href="category.html" class="btn-layout">
                                    <svg width="16" height="10">
                                        <rect x="0" y="0" width="4" height="4" />
                                        <rect x="6" y="0" width="4" height="4" />
                                        <rect x="12" y="0" width="4" height="4" />
                                        <rect x="0" y="6" width="4" height="4" />
                                        <rect x="6" y="6" width="4" height="4" />
                                        <rect x="12" y="6" width="4" height="4" />
                                    </svg>
                                </a>

                                <a href="category-4cols.html" class="btn-layout">
                                    <svg width="22" height="10">
                                        <rect x="0" y="0" width="4" height="4" />
                                        <rect x="6" y="0" width="4" height="4" />
                                        <rect x="12" y="0" width="4" height="4" />
                                        <rect x="18" y="0" width="4" height="4" />
                                        <rect x="0" y="6" width="4" height="4" />
                                        <rect x="6" y="6" width="4" height="4" />
                                        <rect x="12" y="6" width="4" height="4" />
                                        <rect x="18" y="6" width="4" height="4" />
                                    </svg>
                                </a>
                            </div><!-- End .toolbox-layout -->
                        </div><!-- End .toolbox-right -->
                    </div><!-- End .toolbox -->
                    @if($product_all->count() > 0)
                    
                    @foreach ($product_all as $item)
                    <div class="products mb-3">
                        <div class="product product-list">
                            <div class="row">
                                <div class="col-4 col-lg-2">
                                    <figure class="product-media">
                                        @if ($item->status_sell == 1)
                                        <span class="product-label label-new">
                                            ໃຫມ່
                                        </span>
                                        @elseif($item->status_sell == 2)
                                        <span class="product-label label-sale">
                                            ຂາຍດີ
                                        </span>
                                        @else
                                        @endif
                                        <a href="javascript:void(0)" wire:click="ViewProductDetail({{ $item->id }})">
                                            <img src="{{ asset('public/'.$item->image) }}" alt="Product image" class="product-image">
                                        </a>
                                    </figure><!-- End .product-media -->
                                </div><!-- End .col-sm-6 col-lg-3 -->

                                <div class="col-6 col-lg-3 order-lg-last">
                                    <div class="product-list-action">
                                        <div class="product-price">
                                         {{ number_format($item->sell_price) }} .00 ₭
                                        </div><!-- End .product-price -->
                                        <div class="ratings-container">
                                            <div class="ratings">
                                                <div class="ratings-val" style="width: 20%;"></div><!-- End .ratings-val -->
                                            </div><!-- End .ratings -->
                                            <span class="ratings-text">( 2 Reviews )</span>
                                        </div><!-- End .rating-container -->

                                        {{-- <div class="product-action">
                                            <a href="popup/quickView.html" class="btn-product btn-quickview" title="Quick view"><span>quick view</span></a>
                                            <a href="#" class="btn-product btn-compare" title="Compare"><span>compare</span></a>
                                        </div><!-- End .product-action --> --}}
                                        @if ($this->cartData->where('id', $item->id)->count('id') > 0)
                                        <h4>added</h4>
                                        @else
                                       @if($item->qty > 0)
                                       <button type="button" wire:click="addtoCart({{$item->id}},'{{$item->name}}',{{$item->sell_price}})"  class="btn btn-success rounded"><i class="fas fa-cart-plus"></i> ເກັບໃສ່ກະຕ່າ</button>
                                       @else
                                       <button disabled type="button" wire:click="addtoCart({{$item->id}},'{{$item->name}}',{{$item->sell_price}})"  class="btn btn-success rounded"><i class="fas fa-cart-plus"></i> ເກັບໃສ່ກະຕ່າ</button>
                                       @endif
                                        @endif
                                       
                                        
                                    </div><!-- End .product-list-action -->
                                </div><!-- End .col-sm-6 col-lg-3 -->

                                <div class="col-lg-6">
                                    <div class="product-body product-action-inner">
                                        <a href="javascript:void(0)" wire:click.prevent="addToWishlist({{$item->id}},'{{$item->name}}',{{$item->sell_price}})" class="btn-product btn-wishlist" title="ເພີ່ມໃສ່ລາຍການທີ່ມັກ"></a>
                                        <div class="text-bold">
                                            <a href="javascript:void(0)">ລະຫັດ: {{ $item->code }}</a>
                                        </div><!-- End .product-cat -->
                                        @if(!empty($item->product_type))
                                        <div class="product-cat">
                                            <a href="#">ປະເພດ: {{ $item->product_type->name }}</a>
                                        </div><!-- End .product-cat -->
                                        @endif
                                        <h3 class="product-title"><a href="javascript:void(0)" wire:click="ViewProductDetail({{ $item->id }})">{{ $item->name }}</a></h3><!-- End .product-title -->

                                        <div class="product-content">
                                            <p>
                                                @if ($item->qty > 0)
                                                    <p class="text-success">In Stock</p>
                                                    @else
                                                    <p class="text-danger"><i class="fas fa-box-open"></i> ສິນຄ້າຫມົດ!</p>
                                                @endif
                                            </p>
                                        </div><!-- End .product-content -->
                                        
                                        {{-- <div class="product-nav product-nav-thumbs">
                                            <a href="#" class="active">
                                                <img src="{{ asset('public/'.$item->image) }}" alt="product desc">
                                            </a>
                                            <a href="#">
                                                <img src="assets/images/products/product-4-2-thumb.jpg" alt="product desc">
                                            </a>

                                            <a href="#">
                                                <img src="assets/images/products/product-4-3-thumb.jpg" alt="product desc">
                                            </a>
                                        </div><!-- End .product-nav --> --}}
                                    </div><!-- End .product-body -->
                                </div><!-- End .col-lg-6 -->
                            </div><!-- End .row -->
                        </div><!-- End .product -->
                    </div><!-- End .products -->
                    @endforeach
                    @else
                    <style>
                        @import url(http://fonts.googleapis.com/css?family=Calibri:400,300,700);
        
                        body {
                            background-color: #eee;
                            font-family: 'Calibri', sans-serif !important;
                        }
        
                        .mt-100 {
                            margin-top: 10px;
        
                        }
        
        
                        .card {
                            margin-bottom: 30px;
                            border: 0;
                            -webkit-transition: all .3s ease;
                            transition: all .3s ease;
                            letter-spacing: .5px;
                            border-radius: 8px;
                            -webkit-box-shadow: 1px 5px 24px 0 rgba(68, 102, 242, .05);
                            box-shadow: 1px 5px 24px 0 rgba(68, 102, 242, .05);
                        }
        
                        .card .card-header {
                            background-color: #fff;
                            border-bottom: none;
                            padding: 24px;
                            border-bottom: 1px solid #f6f7fb;
                            border-top-left-radius: 8px;
                            border-top-right-radius: 8px;
                        }
        
                        .card-header:first-child {
                            border-radius: calc(.25rem - 1px) calc(.25rem - 1px) 0 0;
                        }
        
        
        
                        .card .card-body {
                            padding: 30px;
                            background-color: transparent;
                        }
        
                        .btn-primary,
                        .btn-primary.disabled,
                        .btn-primary:disabled {
                            background-color: #4466f2 !important;
                            border-color: #4466f2 !important;
                        }
                    </style>
                    <div class="container-fluid text-center">
                        <div class="row">
        
                            <div class="col-md-12">
        
                                <div class="card">
                                    <div class="card-body cart">
                                        <div class="col-sm-12 empty-cart-cls text-center">
                                            <img src="https://gifdb.com/images/high/sealing-a-box-stop-motion-ssquj3xtrj9ndkry.gif"
                                                style="width: auto; height:100px; margin-left: 46%">
                                            <h3><strong><i class="fas fa-search"></i> ບໍ່ພົບສິນຄ້າທີ່ທ່ານຄົ້ນຫາ!</strong></h3>
                                        
                                        </div>
                                    </div>
                                </div>
        
        
                            </div>
        
                        </div>
        
                    </div>
                    @endif
                    <nav aria-label="Page navigation">
                        <div class="float-right">
                            {{ $product_all->links() }}
                        </div>
                    </nav>
                </div><!-- End .col-lg-9 -->
                <aside class="col-lg-3 order-lg-first">
                    <div class="sidebar sidebar-shop">
                        <div class="widget widget-clean">
                            <label>Filters:</label>
                            <a href="#" class="sidebar-filter-clear">Clean All</a>
                        </div><!-- End .widget widget-clean -->

                        <div class="widget widget-collapsible">
                            <h3 class="widget-title">
                                <a data-toggle="collapse" href="#widget-1" role="button" aria-expanded="true" aria-controls="widget-1">
                                    Category
                                </a>
                            </h3><!-- End .widget-title -->

                            <div class="collapse show" id="widget-1">
                                <div class="widget-body">
                                    <div class="filter-items filter-items-count">
                                        <div class="filter-item">
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="cat-1">
                                                <label class="custom-control-label" for="cat-1">Dresses</label>
                                            </div><!-- End .custom-checkbox -->
                                            <span class="item-count">3</span>
                                        </div><!-- End .filter-item -->

                                        <div class="filter-item">
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="cat-2">
                                                <label class="custom-control-label" for="cat-2">T-shirts</label>
                                            </div><!-- End .custom-checkbox -->
                                            <span class="item-count">0</span>
                                        </div><!-- End .filter-item -->

                                        <div class="filter-item">
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="cat-3">
                                                <label class="custom-control-label" for="cat-3">Bags</label>
                                            </div><!-- End .custom-checkbox -->
                                            <span class="item-count">4</span>
                                        </div><!-- End .filter-item -->

                                        <div class="filter-item">
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="cat-4">
                                                <label class="custom-control-label" for="cat-4">Jackets</label>
                                            </div><!-- End .custom-checkbox -->
                                            <span class="item-count">2</span>
                                        </div><!-- End .filter-item -->

                                        <div class="filter-item">
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="cat-5">
                                                <label class="custom-control-label" for="cat-5">Shoes</label>
                                            </div><!-- End .custom-checkbox -->
                                            <span class="item-count">2</span>
                                        </div><!-- End .filter-item -->

                                        <div class="filter-item">
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="cat-6">
                                                <label class="custom-control-label" for="cat-6">Jumpers</label>
                                            </div><!-- End .custom-checkbox -->
                                            <span class="item-count">1</span>
                                        </div><!-- End .filter-item -->

                                        <div class="filter-item">
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="cat-7">
                                                <label class="custom-control-label" for="cat-7">Jeans</label>
                                            </div><!-- End .custom-checkbox -->
                                            <span class="item-count">1</span>
                                        </div><!-- End .filter-item -->

                                        <div class="filter-item">
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="cat-8">
                                                <label class="custom-control-label" for="cat-8">Sportwear</label>
                                            </div><!-- End .custom-checkbox -->
                                            <span class="item-count">0</span>
                                        </div><!-- End .filter-item -->
                                    </div><!-- End .filter-items -->
                                </div><!-- End .widget-body -->
                            </div><!-- End .collapse -->
                        </div><!-- End .widget -->
                    </div><!-- End .sidebar sidebar-shop -->
                </aside><!-- End .col-lg-3 -->
            </div><!-- End .row -->
        </div><!-- End .container -->
    </div><!-- End .page-content -->
</main><!-- End .main -->
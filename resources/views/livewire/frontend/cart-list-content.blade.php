@if (!empty(Cart::instance('cart')->count()))
<div class="dropdown-menu dropdown-menu-right">
    @foreach (Cart::instance('cart')->content() as $item)
        <div class="dropdown-cart-products">
            <div class="product">
                <div class="product-cart-details">
                    <h4 class="product-title">
                        <a href="product.html">{{ $item->name }}</a>
                    </h4>

                    <span class="cart-product-info">
                        <span class="cart-product-qty">{{ $item->qty }}</span>
                        x {{ $item->model->sell_price }}
                    </span>
                </div><!-- End .product-cart-details -->

                <figure class="product-image-container">
                    <a href="product.html" class="product-image">
                        <img src="{{ asset('public/'.$item->model->image) }}" title="{{$item->model->name}}" alt="Product image" style="width: 60px; height: 60px;">
                    </a>
                </figure>
                <button type="button" class="btn-remove text-danger" wire:click="destroy('{{$item->rowId}}')"><i class="icon-close"></i></button>
            </div><!-- End .product -->
        </div><!-- End .cart-product -->
        @endforeach
        <div class="dropdown-cart-total">
            <span>ເປັນເງິນ</span>
            <span class="cart-total-price">{{ Cart::instance('cart')->subtotal() }} ₭</span>
        </div><!-- End .dropdown-cart-total -->

        <div class="dropdown-cart-action">
            <a href="{{ route('frontend.cart') }}" class="btn btn-primary"><i class="fas fa-eye"></i> ເບິ່ງກະຕ່າ</a>
            <a href="{{ route('frontend.cart') }}" class="btn btn-success"><span>ຊຳລະເງິນ</span><i class="icon-long-arrow-right"></i></a>
        </div><!-- End .dropdown-cart-total -->
    </div><!-- End .dropdown-menu -->
    @else
    <div class="dropdown-menu dropdown-menu-right text-center">
        <h5 class="text-danger"><i class="fas fa-shopping-cart"></i> ບໍ່ມີສິນຄ້າໃນກະຕ່າ!</h5>
    </div><!-- End .dropdown-menu -->
@endif
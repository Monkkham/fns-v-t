<main class="main">
    <nav aria-label="breadcrumb" class="breadcrumb-nav border-0 mb-0">
        <div class="container">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('home') }}">ຫນ້າຫຼັກ</a></li>
                <li class="breadcrumb-item active" aria-current="page">ຕິດຕໍ່ພວກເຮົາ</li>
            </ol>
            <hr>
        </div><!-- End .container -->
    </nav><!-- End .breadcrumb-nav -->
    {{-- <div class="container">
        <div class="page-header page-header-big text-center" style="background-image: url('assets/images/contact-header-bg.jpg')">
            <h1 class="page-title text-white">Contact us<span class="text-white">keep in touch with us</span></h1>
        </div><!-- End .page-header -->
    </div><!-- End .container --> --}}
    <div class="page-content pb-0">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 mb-2 mb-lg-0">
                    <h2 class="title mb-1">ຊ່ອງທາງການຕິດຕໍ່</h2><!-- End .title mb-2 -->
                    <p class="mb-3">ຕີດຕໍ່ພົ່ວພັນເຈົ້າຂອງຮ້ານ ຫລື ພະນັກງານໄດ້ຕາມຂໍ້ມູນຕິດຕໍ່ຂ້າງລຸ່ມນີ້:
                    </p>
                    <div class="row">
                        <div class="col-sm-7">
                            <div class="contact-info">
                                <h3>ຮ້ານຂາຍເຄື່ອງໄຊໂຍ</h3>
                                <ul class="contact-list">
                                    <li>
                                        <i class="icon-map-marker"></i>
                                        ບ້ານຍອດງື່ມ ເມືອງແປກ ແຂວງຊຽງຂວາງ ປະເທດລາວ
                                    </li>
                                    <li>
                                        <i class="icon-phone"></i>
                                        <a href="tel:#">+856 58974545</a>
                                    </li>
                                    <li>
                                        <i class="icon-envelope"></i>
                                        <a href="mailto:#">xaiyo@gmail.com</a>
                                    </li>
                                </ul><!-- End .contact-list -->
                            </div><!-- End .contact-info -->
                        </div><!-- End .col-sm-7 -->

                        <div class="col-sm-5">
                            <div class="contact-info">
                                <h3>ເປີດຮ້ານອອນລາຍ 24 ຊົ່ວໂມງ</h3>

                                <ul class="contact-list">
                                    <li>
                                        <i class="icon-clock-o"></i>
                                        <span class="text-dark">ຈັນ-ອາທິດ</span> <br>8:00 AM - 17:00 PM
                                    </li>
                                    <li>
                                        <i class="icon-calendar"></i>
                                        <span class="text-dark">Sunday</span> <br>11am-6pm ET
                                    </li>
                                </ul><!-- End .contact-list -->
                            </div><!-- End .contact-info -->
                        </div><!-- End .col-sm-5 -->
                    </div><!-- End .row -->
                </div><!-- End .col-lg-6 -->
                <div class="col-lg-6">
                    <h2 class="title mb-1">ທ່ານມີຄຳຖາມຫຍັງທີ່ສົງໄສບໍ່?</h2><!-- End .title mb-2 -->
                    <p class="mb-2">ສາມາດສອບຖາມພະນັກງານ ຫລື ສະເເດງຄຳເຫັນກ່ຽວກັບຮ້ານຄ້າຂອງພວກເຮົາ</p>

                    <form class="contact-form mb-3">
                        <div class="row">
                            <div class="col-sm-6">
                                <label for="cname" class="sr-only">Name</label>
                                <input type="text" wire:model="name"
                                    class="form-control  @error('name') is-invalid @enderror" id="cname"
                                    placeholder="ຊື່ລູກຄ້າ *">
                                @error('name')
                                    <span style="color: red" class="error">{{ $message }}</span>
                                @enderror
                            </div><!-- End .col-sm-6 -->
                            <div class="col-sm-6">
                                <label for="cphone" class="sr-only">Phone</label>
                                <input type="tel" wire:model="phone"
                                    class="form-control  @error('phone') is-invalid @enderror" id="cphone"
                                    placeholder="ເບີໂທ *">
                                @error('phone')
                                    <span style="color: red" class="error">{{ $message }}</span>
                                @enderror
                            </div><!-- End .col-sm-6 -->

                        </div><!-- End .row -->
                        <div class="row">
                            <div class="col-sm-6">
                                <label for="cemail" class="sr-only">Email</label>
                                <input type="email" wire:model="email"
                                    class="form-control  @error('email') is-invalid @enderror" id="cemail"
                                    placeholder="ອີເມວ *">
                                @error('email')
                                    <span style="color: red" class="error">{{ $message }}</span>
                                @enderror
                            </div><!-- End .col-sm-6 -->
                            <div class="col-sm-6">
                                <label for="csubject" class="sr-only">Subject</label>
                                <input type="text" wire:model="header"
                                    class="form-control  @error('header') is-invalid @enderror" id="csubject"
                                    placeholder="ຫົວຂໍ້ *">
                                @error('header')
                                    <span style="color: red" class="error">{{ $message }}</span>
                                @enderror
                            </div><!-- End .col-sm-6 -->
                        </div><!-- End .row -->
                        <label for="cmessage" class="sr-only">Message</label>
                        <textarea class="form-control @error('note') is-invalid @enderror" wire:model="note" cols="30" rows="4" id="cmessage" placeholder="ຄຳຄິດເຫັນ *"></textarea>
                        @error('note')
                            <span style="color: red" class="error">{{ $message }}</span>
                        @enderror
                        <button type="button" wire:click="contact"
                            class="btn btn-outline-primary-2 btn-minwidth-sm">
                            <span>ສົ່ງຂໍ້ມູນ</span>
                            <i class="icon-long-arrow-right"></i>
                        </button>
                    </form><!-- End .contact-form -->
                </div><!-- End .col-lg-6 -->
            </div><!-- End .row -->
            <hr class="mt-4 mb-5">
        </div><!-- End .container -->
        <div>
           @foreach ($location as $item)
           @if(!empty($item->latitude) && !empty($item->longitude))
           <iframe  src="https://maps.google.com/maps?q={{$item->latitude}},{{$item->longitude}}&hl=es;z=13&output=embed" style="height:400px; width: 100%;"></iframe>
           @else
           <td class="text-center bg-danger"><i class="fa fa-map-marker-alt"></i>ບໍ່ມີ location</td>
           @endif
           @endforeach
        </div><!-- End #map -->
    </div><!-- End .page-content -->
</main><!-- End .main -->

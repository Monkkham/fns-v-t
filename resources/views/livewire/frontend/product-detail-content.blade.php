<main wire:ignore class="main">
    <nav aria-label="breadcrumb" class="breadcrumb-nav border-0 mb-0">
        <div class="container d-flex align-items-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="index.html">ຫນ້າຫຼັກ</a></li>
                <li class="breadcrumb-item"><a href="#">ສິນຄ້າ</a></li>
                <li class="breadcrumb-item active" aria-current="page">ລາຍລະອຽດສິນຄ້າ</li>
            </ol>
        </div>
        <!-- End .container -->
    </nav>
    <!-- End .breadcrumb-nav -->

    <div class="page-content">
        <div class="container">
            @foreach ($products as $item)
                <div class="product-details-top">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="product-gallery product-gallery-vertical">
                                <div class="row">
                                    <figure class="product-main-image">
                                        <img id="product-zoom" src="{{ asset('public/'.$item->image) }}"
                                            data-zoom-image="{{ asset('public/'.$item->image) }}" alt="{{ $item->name }}">

                                        <a href="#" id="btn-product-gallery" class="btn-product-gallery">
                                            <i class="icon-arrows"></i>
                                        </a>
                                    </figure>
                                    <!-- End .product-main-image -->

                                    <div id="product-zoom-gallery" class="product-image-gallery">
                                        <a class="product-gallery-item active" href="{{ asset('public/'.$item->image) }}"
                                            data-image="assets/images/products/single/1.jpg"
                                            data-zoom-image="assets/images/products/single/1-big.jpg">
                                            <img src="{{ asset('public/'.$item->image) }}" alt="product side">
                                        </a>

                                        <a class="product-gallery-item" href="#"
                                            data-image="{{ asset('public/'.$item->image) }}"
                                            data-zoom-image="assets/images/products/single/2-big.jpg">
                                            <img src="{{ asset('public/'.$item->image) }}" alt="product cross">
                                        </a>

                                        <a class="product-gallery-item" href="#"
                                            data-image="{{ asset('public/'.$item->image) }}"
                                            data-zoom-image="assets/images/products/single/3-big.jpg">
                                            <img src="{{ asset('public/'.$item->image) }}" alt="product with model">
                                        </a>

                                        <a class="product-gallery-item" href="#"
                                            data-image="{{ asset('public/'.$item->image) }}"
                                            data-zoom-image="assets/images/products/single/4-big.jpg">
                                            <img src="{{ asset('public/'.$item->image) }}" alt="product back">
                                        </a>
                                    </div>
                                    <!-- End .product-image-gallery -->
                                </div>
                                <!-- End .row -->
                            </div>
                            <!-- End .product-gallery -->
                        </div>
                        <!-- End .col-md-6 -->

                        <div class="col-md-6">
                            <div class="product-details">
                                <h1 class="product-title">{{ $item->name }}</h1>
                                <!-- End .product-title -->

                                <div class="ratings-container">
                                    <div class="ratings">
                                        <div class="ratings-val" style="width: 80%;"></div>
                                        <!-- End .ratings-val -->
                                    </div>
                                    <!-- End .ratings -->
                                    <a class="ratings-text" href="#product-review-link" id="review-link">( 2 Reviews
                                        )</a>
                                </div>
                                <!-- End .rating-container -->

                                <div class="product-price">
                                    ລາຄາ: {{ number_format($item->promotion_price) }} ₭
                                </div>
                                <!-- End .product-price -->

                               <s>
                                <div class="product-content">
                                    ປົກກະຕິ: {{ number_format($item->sell_price) }} ₭
                                </div>
                               </s>
                                <p>
                                    @if ($item->qty > 0)
                                        <p class="text-success">In Stock</p>
                                    @else
                                        <p class="text-danger"><i class="fas fa-box-open"></i> ສິນຄ້າຫມົດ!</p>
                                    @endif
                                </p>
                                <!-- End .product-content -->
                                <div class="details-filter-row details-row-size">
                                    <p for="size">ປະເພດ: {{ $item->product_type->name }}</p>
                                    <!-- End .select-custom -->
                                </div>
                                <!-- End .details-filter-row -->

                                <div class="details-filter-row details-row-size">
                                    <label for="qty">ຈຳນວນ:</label>
                                    {{-- <div class="cart-product-quantity text-center">
                                        <a href="javascript:void(0)"><i class="fa fa-plus text-success" wire:click="increaseQty('{{$item->rowId}}')"></i></a>
                                        <input type="tel" class="form-control text-center" value="{{ $item->qty }}" min="1" max="100" step="1" data-decimals="0">
                                        <a href="javascript:void(0)"><i class="fa fa-minus text-danger" wire:click="decreaseQty('{{$item->rowId}}')"></i></a>
                                    </div><!-- End .cart-product-quantity --> --}}
                                    <div class="product-details-quantity">
                                        <input disabled type="number" id="qty" class="form-control" value="1"
                                            min="1" max="10" step="1" data-decimals="0" required>
                                    </div>
                                    <!-- End .product-details-quantity -->
                                </div>
                                <!-- End .details-filter-row -->

                                <div class="product-details-action">
                                    @if ($item->qty > 0)
                                        <button type="button"
                                            wire:click="addtoCart({{ $item->id }},'{{ $item->name }}',{{ $item->sell_price }})"
                                            class="btn btn-success rounded mr-3"><i class="fas fa-cart-plus"></i>
                                            ເກັບໃສ່ກະຕ່າ</button>
                                    @else
                                        <button disabled type="button"
                                            wire:click="addtoCart({{ $item->id }},'{{ $item->name }}',{{ $item->sell_price }})"
                                            class="btn btn-success rounded mr-3"><i class="fas fa-cart-plus"></i>
                                            ເກັບໃສ່ກະຕ່າ</button>
                                    @endif
                                    <button type="button"
                                        wire:click.prevent="addToWishlist({{ $item->id }},'{{ $item->name }}',{{ $item->sell_price }})"
                                        class="btn btn-info rounded"><i class="fas fa-heart"></i>
                                        ເພີ່ມສິ່ງທີ່ມັກ</button>
                                    <!-- End .details-action-wrapper -->
                                </div>


                                <div class="product-details-footer">
                                    <div class="social-icons social-icons-sm">
                                        <span class="social-label">Share:</span>
                                        <a href="#" class="social-icon" title="Facebook" target="_blank"><i
                                                class="icon-facebook-f"></i></a>
                                        <a href="#" class="social-icon" title="Twitter" target="_blank"><i
                                                class="icon-twitter"></i></a>
                                        <a href="#" class="social-icon" title="Instagram" target="_blank"><i
                                                class="icon-instagram"></i></a>
                                        <a href="#" class="social-icon" title="Pinterest" target="_blank"><i
                                                class="icon-pinterest"></i></a>
                                    </div>
                                </div>
                                <!-- End .product-details-footer -->
                            </div>
                            <!-- End .product-details -->
                        </div>
                        <!-- End .col-md-6 -->
                    </div>
                    <!-- End .row -->
                </div>

                <!-- End .product-details-top -->

                <div class="product-details-tab">
                    <ul class="nav nav-pills justify-content-center" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="product-desc-link" data-toggle="tab"
                                href="#product-desc-tab" role="tab" aria-controls="product-desc-tab"
                                aria-selected="true">------------------------ ຂໍ້ມູນລາຍລະອຽດສິນຄ້າ
                                ------------------------</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane fade show active" id="product-desc-tab" role="tabpanel"
                            aria-labelledby="product-desc-link">
                            <div class="product-desc-content">
                                <h3>{{ $item->name }}</h3>
                                <p>{!! $item->note !!}</p>
                            </div>
                            <!-- End .product-desc-content -->
                        </div>
                        <!-- .End .tab-pane -->

                    </div>
                    <!-- End .tab-content -->
                </div>
                <!-- End .product-details-tab -->
            @endforeach
            <h2 class="title text-center mb-4"><i class="fas fa-cart-arrow-down"></i> ສິນຄ້າທີ່ໃກ້ຄຽງກັນ</h2>
            <!-- End .title text-center -->

            <div class="owl-carousel owl-simple carousel-equal-height carousel-with-shadow" data-toggle="owl"
                data-owl-options='{
                    "nav": false, 
                    "dots": true,
                    "margin": 20,
                    "loop": false,
                    "responsive": {
                        "0": {
                            "items":1
                        },
                        "480": {
                            "items":2
                        },
                        "768": {
                            "items":3
                        },
                        "992": {
                            "items":4
                        },
                        "1200": {
                            "items":4,
                            "nav": true,
                            "dots": false
                        }
                    }
                }'>
                @foreach ($product_type_food as $item)
                    <div class="product">
                        <figure class="product-media">
                            @if ($item->status_sell == 1)
                                <span class="product-label label-new">
                                    ໃຫມ່
                                </span>
                            @elseif($item->status_sell == 2)
                                <span class="product-label label-sale">
                                    ຂາຍດີ
                                </span>
                            @else
                            @endif
                            <a href="javascript:void(0)" wire:click="ViewProductDetail({{ $item->id }})">
                                <img src="{{ asset('public/'.$item->image) }}" style="width: 100%; height: 160px;">
                            </a>

                            <div class="product-action-vertical">
                                <button type="button"
                                    wire:click.prevent="addToWishlist({{ $item->id }},'{{ $item->name }}',{{ $item->sell_price }})"
                                    class="btn-product-icon btn-wishlist btn-expandable"><span>ເພີ່ມສິ່ງທີ່ມັກ</span></button>
                                {{-- <a href="#" class="btn-product-icon btn-compare" title="Compare"><span>Compare</span></a>
                                <a href="popup/quickView.html" class="btn-product-icon btn-quickview" title="Quick view"><span>Quick view</span></a> --}}
                            </div><!-- End .product-action-vertical -->
                        </figure><!-- End .product-media -->

                        <div class="product-body">
                            <div class="product-cat">
                                @if (!empty($item->product_type))
                                    <a href="#">ປະເພດ:
                                        {{ $item->product_type->name }}
                                    @else
                                @endif
                                </a>
                            </div><!-- End .product-cat -->
                            <h3 class="product-title"><a href="product.html">{{ $item->name }}</a></h3>
                            <p>
                                @if ($item->qty > 0)
                                    <p class="text-success">In Stock</p>
                                @else
                                    <p class="text-danger">Out of stock</p>
                                @endif
                            </p>
                            <!-- End .product-title -->
                            <div class="product-price">
                                <span class="new-price">ລາຄາ: {{ number_format($item->promotion_price) }}
                                    ₭</span>
                                <s>
                                    <span class="old-price">ປົກກະຕິ: {{ number_format($item->sell_price) }}
                                        ₭</span>
                                </s>
                            </div><!-- End .product-price -->
                            <div class="ratings-container pb-1">
                            </div><!-- End .rating-container -->
                            @if ($item->qty > 0)
                                <div class="product pb-1">
                                    <button type="button"
                                        wire:click="addtoCart({{ $item->id }},'{{ $item->name }}',{{ $item->sell_price }})"
                                        class="btn-product btn-cart col-lg-12"
                                        title="ເກັບໃສ່ກະຕ່າ"><span>ເກັບໃສ່ກະຕ່າ</span></button>
                                </div><!-- End .product-action -->
                            @else
                                <p class="text-danger text-center"><i class="fas fa-box-open"></i> ສິນຄ້າຫມົດ!</p>
                                <div class="product pb-1">
                                    <button disabled type="button"
                                        wire:click="addtoCart({{ $item->id }},'{{ $item->name }}',{{ $item->sell_price }})"
                                        class="btn-product btn-cart col-lg-12"
                                        title="ເກັບໃສ່ກະຕ່າ"><span>ເກັບໃສ່ກະຕ່າ</span></button>
                                </div><!-- End .product-action -->
                            @endif
                        </div><!-- End .product-body -->
                    </div><!-- End .product -->
                @endforeach
            </div>
            <!-- End .owl-carousel -->
        </div>
        <!-- End .container -->
    </div>
    <!-- End .page-content -->
</main>
<!-- End .main -->

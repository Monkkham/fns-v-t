<main class="main">
    <div class="page-header text-center" style="background-image: url('assets/images/page-header-bg.jpg')">
        <div class="container">
            <h1 class="page-title"><i class="fas fa-credit-card"></i> ຊຳລະເງິນ</h1>
        </div><!-- End .container -->
    </div><!-- End .page-header -->
    <nav aria-label="breadcrumb" class="breadcrumb-nav">
        <div class="container">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="index.html">ຫນ້າຫຼັກ</a></li>
                <li class="breadcrumb-item"><a href="#">ກະຕ່າ</a></li>
                <li class="breadcrumb-item active" aria-current="page">ຊຳລະເງິນ</li>
            </ol>
        </div><!-- End .container -->
    </nav><!-- End .breadcrumb-nav -->

    <div class="page-content">
        <div class="checkout">
            <div class="container">
                {{-- <div class="checkout-discount">
                    <form action="#">
                        <input type="text" class="form-control" required id="checkout-discount-input">
                        <label for="checkout-discount-input" class="text-truncate">Have a coupon? <span>Click
                                here to enter your code</span></label>
                    </form>
                </div><!-- End .checkout-discount --> --}}
                <form action="#">
                    <div class="row">
                        <div class="col-lg-8">
                            <h2 class="checkout-title"><i class="fas fa-user"></i> ຂໍ້ມູນລູກຄ້າ</h2>
                            <!-- End .checkout-title -->
                            <div class="row">
                                <div class="col-sm-6">
                                    <label>ຊື່ *</label>
                                    <input disabled wire:model="name" type="text"
                                        class="form-control @error('name') is-invalid @enderror">
                                    @error('name')
                                    <span style="color: red" class="error">{{ $message }}</span>
                                    @enderror
                                </div><!-- End .col-sm-6 -->

                                <div class="col-sm-6">
                                    <label>ນາມສະກຸນ *</label>
                                    <input disabled wire:model="lastname" type="text"
                                        class="form-control @error('lastname') is-invalid @enderror">
                                    @error('lastname')
                                    <span style="color: red" class="error">{{ $message }}</span>
                                    @enderror
                                </div><!-- End .col-sm-6 -->
                                <div class="col-sm-6">
                                    <label>ເບີໂທ *</label>
                                    <input disabled wire:model="phone" type="text"
                                        class="form-control @error('phone') is-invalid @enderror">
                                    @error('phone')
                                    <span style="color: red" class="error">{{ $message }}</span>
                                    @enderror
                                </div><!-- End .col-sm-6 -->

                                <div class="col-sm-6">
                                    <label>ອີເມວ *</label>
                                    <input disabled wire:model="email" type="text"
                                        class="form-control @error('email') is-invalid @enderror">
                                    @error('email')
                                    <span style="color: red" class="error">{{ $message }}</span>
                                    @enderror
                                </div><!-- End .col-sm-6 -->
                            </div><!-- End .row -->
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group" wire:ignore>
                                        <label>ເລືອກແຂວງ</label>
                                        <select disabled wire:model="province_id" id="select1" class="form-control
													 @error('province_id') is-invalid @enderror">
                                            <option value="" selected>{{ __('lang.select') }}</option>
                                            @foreach ($province as $item)
                                            <option value="{{ $item->id }}">{{ $item->name_la }}
                                            </option>
                                            @endforeach
                                        </select>
                                        @error('province_id')
                                        <span style="color: red" class="error">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>ເລືອກເມືອງ</label>
                                        <select disabled wire:model="district_id" id="select2" class="form-control
													 @error('district_id') is-invalid @enderror">
                                            <option value="" selected>{{ __('lang.select') }}</option>
                                            @foreach ($district as $item)
                                            <option value="{{ $item->id }}">{{ $item->name_la }}
                                            </option>
                                            @endforeach
                                        </select>
                                        @error('district_id')
                                        <span style="color: red" class="error">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>ເລືອກບ້ານ</label>
                                        <select disabled wire:model="village_id" id="select3" class="form-control
													 @error('village_id') is-invalid @enderror">
                                            <option value="" selected>{{ __('lang.select') }}</option>
                                            @foreach ($village as $item)
                                            <option value="{{ $item->id }}">{{ $item->name_la }}
                                            </option>
                                            @endforeach
                                        </select>
                                        @error('village_id')
                                        <span style="color: red" class="error">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <label>ຖະຫນົນເລກທີ່ (ທີ່ຢູ່) *</label>
                            <input  type="text" class="form-control" wire:model="address" placeholder="">

                            <label>ຄຳຄິດເຫັນຂອງລູກຄ້າກ່ຽວກັບສິນຄ້າ</label>
                            <textarea class="form-control" wire:model="note" cols="30" rows="4"
                                placeholder="ຄຳຄິດເຫັນຂອງລູກຄ້າກ່ຽວກັບສິນຄ້າ"></textarea>

                        </div><!-- End .col-lg-9 -->

                        <aside class="col-lg-4">
                            <div class="summary">
                                <h3 class="summary-title"><i class="fas fa-shopping-cart"></i>
                                    ລາຍການສັ່ງຊື້ຂອງທ່ານ</h3><!-- End .summary-title -->

                                <table class="table table-summary">
                                    <thead>
                                        <tr>
                                            <th>ສິນຄ້າ</th>
                                            <th>ຈຳນວນ</th>
                                            <th>ເປັນເງິນ</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        @foreach (Cart::instance('cart')->content() as $item)
                                        <tr>
                                            <td><b>{{ $item->name }}</b></td>
                                            <td>x {{ $item->qty }}</td>
                                            <td>{{ number_format($item->subtotal) }} .00 ₭</td>
                                        </tr>
                                        @endforeach
                                        <tr class="text-info">
                                            <td colspan="2">ລວມເປັນເງິນ:</td>
                                            <td>{{ number_format($moneyForChange) }} ₭</td>
                                        </tr>
                                        <!-- End .summary-subtotal -->
                                        <tr>
                                            <td><i class="fas fa-truck"></i> ຂົນສົ່ງ:</td>
                                            <td>ຟີຣ</td>
                                            <td></td>
                                        </tr>
                                        {{-- @if (!Session::has('coupon')) --}}
                                        <tr>
                                            <td><i class="fas fa-search-dollar"></i> ອາກອນ:</td>
                                            {{-- @if (!empty(session()->get('checkout')['tax'])) --}}
                                            <td>{{ number_format(session()->get('checkout')['tax']) }}
                                                ₭
                                            </td>
                                            {{-- @endif --}}
                                            <td></td>
                                        </tr>
                                        {{-- @endif --}}
                                        <tr>
                                            <td><i class="fas fa-percent"></i> ສ່ວນຫລຸດ:</td>
                                            @if (Session::has('coupon'))
                                            @if (!empty(session()->get('checkout')['discount']))
                                            <td> -
                                                {{ number_format(session()->get('checkout')['discount']) }}
                                                ₭</td>
                                            @endif
                                            @else
                                            <td> - {{ Cart::instance('cart')->discount() }} ₭</td>
                                            @endif
                                            <td></td>
                                        </tr>
                                        <tr class="text-bold text-primary">
                                            <td colspan="2">
                                                <h6>ລວມເງິນຕ້ອງຊຳລະ:</h6>
                                            </td>
                                            @if (Session::has('coupon'))
                                            @if (!empty(session()->get('checkout')['total']))
                                            <td>
                                                <h6> {{ number_format(session()->get('checkout')['total']) }}
                                                    ₭</h6>
                                            </td>
                                            @endif
                                            @else
                                            <td>
                                                <h6> {{ Cart::instance('cart')->total() }} ₭</h6>
                                            </td>
                                            @endif
                                        </tr><!-- End .summary-total -->
                                    </tbody>
                                </table><!-- End .table table-summary -->
                                <div wire:ignore class="accordion-summary" id="accordion-payment">
                                    <p><i class="fas fa-hand-holding-usd"></i> ເລືອກຊ່ອງທາງການຊຳລະເງິນ</p>
                                    <div class="card pb-1 pt-1">
                                        <div class="card-header" id="heading-3">
                                            <h2 class="card-title text-success">
                                                <input type="radio" id="radioPrimary1" wire:model="mode" value="cod"
                                                    class="collapsed" data-toggle="collapse" href="#collapse-3"
                                                    aria-expanded="false" aria-controls="collapse-3">
                                                ຊຳລະເງິນສົດ (COD)
                                            </h2>
                                        </div><!-- End .card-header -->
                                        <div id="collapse-3" class="collapse" aria-labelledby="heading-3"
                                            data-parent="#accordion-payment">
                                            <div class="card-body">
                                                ລູກຄ້າຈ່າຍເງິນສົດໃຫ້ກັບພະນັກງານສົ່ງສິນຄ້າເມື່ອຮອດມືລູກຄ້າເເລ້ວ
                                            </div><!-- End .card-body -->
                                        </div><!-- End .collapse -->
                                    </div><!-- End .card -->
                                    {{-- @if ($mode == 'onepay') --}}
                                    <div class="card">
                                        <div class="card-header" id="heading-5">
                                            <h2 class="card-title text-danger">
                                                <input type="radio" id="radioPrimary2" wire:model="mode" value="onepay"
                                                    class="collapsed" data-toggle="collapse" href="#collapse-5"
                                                    aria-expanded="false" aria-controls="collapse-5">
                                                ຊຳລະເງິນໂອນ (OnePay)
                                            </h2>
                                        </div><!-- End .card-header -->
                                        <div id="collapse-5" class="collapse" aria-labelledby="heading-5"
                                            data-parent="#accordion-payment">
                                            <div class="input-group">
                                                <div class="custom-file">
                                                    <input type="file" wire:model="onepay_image"
                                                        class="custom-file-input" id="avatar" name="avatar"
                                                        accept="image/*" onchange="previewAvatar()">
                                                    <label class="custom-file-label"
                                                        for="exampleInputFile">ອັບໂຫລດຮູບໃບບິນ OnePay</label>
                                                </div>
                                            </div>
                                            <div class="card-body">
                                                <table>
                                                    <tr>
                                                        <th>ຊື່ທະນາຄານ:</th>
                                                        <th>(BCEL)</th>
                                                    </tr>
                                                    <tr>
                                                        <th>ຊື່ບັນຊີ:</th>
                                                        <th>Nom Latvunkham</th>
                                                    </tr>
                                                    <tr>
                                                        <th>ເລກບັນຊີ:</th>
                                                        <th><input style="width: 180px;" readonly id="textArea"
                                                                type="text" value="096120001688017005">
                                                            <button type="button" id="copyBtn" title="copied"><i
                                                                    class="fa fa-copy"></i></button>
                                                        </th>
                                                    </tr>
                                                </table>
                                            </div><!-- End .card-body -->
                                            <div class="mt-3">
                                                <img src="{{ asset('public/logo/onepay.jpg') }}" id="avatar-preview"
                                                    alt="Avatar Preview" class="img-thumbnail">
                                            </div>

                                        </div><!-- End .collapse -->
                                    </div><!-- End .card -->
                                    {{-- @endif --}}
                                </div><!-- End .accordion -->
                                @error('mode')
                                <span style="color: red" class="error">{{ $message }}</span>
                                @enderror
                                <button type="button" wire:click="PlaceOrder"
                                    class="btn btn-primary btn-order btn-block">
                                    <span class="btn-text"><i class="fas fa-credit-card"></i>
                                        ຍືນຢັນຊຳລະ</span>
                                    <span class="btn-hover-text"><i class="fas fa-credit-card"></i>
                                        ຊຳລະຕອນນີ້ເລີຍ</span>
                                </button>
                            </div><!-- End .summary -->
                        </aside><!-- End .col-lg-3 -->
                    </div><!-- End .row -->
                </form>
            </div><!-- End .container -->
        </div><!-- End .checkout -->
    </div><!-- End .page-content -->
</main><!-- End .main -->

@push('scripts')
<script>
    const copyBtn = document.getElementById('copyBtn');
                const textArea = document.getElementById('textArea');

                copyBtn.addEventListener('click', () => {
                    textArea.select();
                    document.execCommand('copy');
                });

                function previewAvatar() {
                    const preview = document.getElementById("avatar-preview");
                    const file = document.getElementById("avatar").files[0];
                    const reader = new FileReader();

                    reader.addEventListener("load", function() {
                        preview.src = reader.result;
                        preview.style.display = "block";
                    }, false);

                    if (file) {
                        reader.readAsDataURL(file);
                    }
                }



                window.addEventListener('show-modal-add', event => {
                    $('#modal-add').modal('show');
                })
                window.addEventListener('hide-modal-add', event => {
                    $('#modal-add').modal('hide');
                })

                // ================= avatar image edit==========================
                function readURL(input) {
                    if (input.files && input.files[0]) {
                        var reader = new FileReader();
                        reader.onload = function(e) {
                            $('#imagePreview').css('background-image', 'url(' + e.target.result + ')');
                            $('#imagePreview').hide();
                            $('#imagePreview').fadeIn(650);
                        }
                        reader.readAsDataURL(input.files[0]);
                    }
                }
                $("#imageUpload").change(function() {
                    readURL(this);
                });
</script>
@endpush

<main class="main">
    @if (Cart::instance('cart')->count() > 0)
        <div class="page-header text-center" style="background-image: url('assets/images/page-header-bg.jpg')">
            <div class="container">
                <h1 class="page-title"><i class="fas fa-cart-plus"></i> ລາຍການສິນຄ້າທີ່ທ່ານເລືອກ</h1>
            </div><!-- End .container -->
        </div><!-- End .page-header -->
    @endif
    <nav aria-label="breadcrumb" class="breadcrumb-nav">
        <div class="container">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="index.html">ຫນ້າຫຼັກ</a></li>
                <li class="breadcrumb-item active" aria-current="page">ກະຕ່າສິນຄ້າ</li>
            </ol>
        </div><!-- End .container -->
    </nav><!-- End .breadcrumb-nav -->

    <div class="page-content">
        @if (Cart::instance('cart')->count() > 0)
            <div class="cart">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-8">
                            <table class="table table-cart table-mobile">
                                <thead>
                                    <tr>
                                        <th>ສິນຄ້າ</th>
                                        <th>ລາຄາ</th>
                                        <th>ຈຳນວນ</th>
                                        <th>ເປັນເງິນ</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <div class="cart-bottom">
                                    <div class="cart-discount">
                                        <form>
                                            <div class="input-group">
                                                <input type="text" wire:model="couponCode" class="form-control"
                                                    required placeholder="ລະຫັດສ່ວນຫລຸດ">
                                                <div class="input-group-append">
                                                    <button wire:click="ApplyCouponCode" class="btn btn-primary"
                                                        type="button"><i class="icon-long-arrow-right"></i></button>
                                                </div><!-- .End .input-group-append -->
                                            </div><!-- End .input-group -->
                                        </form>
                                    </div><!-- End .cart-discount -->

                                    @if (Session::has('coupon'))
                                        <a href="javascript:void(0)" wire:click='removeCoupon'
                                            class="btn btn-warning"><i
                                                class="fas fa-trash"></i><span>ຍົກເລີກສ່ວນຫຼຸດ</span></a>
                                    @endif
                                </div><!-- End .cart-bottom -->
                                <tbody>
                                    @foreach (Cart::instance('cart')->content() as $item)
                                        <tr>
                                            <td class="product-col">
                                                <div class="product">
                                                    <figure class="product-media">
                                                        <a href="javascript:void(0)">
                                                            <img src="{{ asset('public/'.$item->model->image) }}"
                                                                title="{{ $item->model->name }}" alt="Product image"
                                                                style="width: 60px; height: 60px;">
                                                        </a>
                                                    </figure>

                                                    <h3 class="product-title">
                                                        <a href="javascript:void(0)">{{ $item->model->name }}</a>
                                                    </h3><!-- End .product-title -->
                                                </div><!-- End .product -->
                                            </td>
                                            <td class="price-col">{{ number_format($item->model->sell_price) }}.00 ₭
                                            </td>
                                            <td class="quantity-col">
                                                <div class="cart-product-quantity text-center">
                                                    <a href="javascript:void(0)"><i class="fa fa-plus text-success"
                                                            wire:click="increaseQty('{{ $item->rowId }}')"></i></a>
                                                    <input type="tel" class="form-control text-center"
                                                        value="{{ $item->qty }}" min="1" max="100"
                                                        step="1" data-decimals="0">
                                                    <a href="javascript:void(0)"><i class="fa fa-minus text-danger"
                                                            wire:click="decreaseQty('{{ $item->rowId }}')"></i></a>
                                                </div><!-- End .cart-product-quantity -->
                                            </td>
                                            <td class="total-col">{{ number_format($item->subtotal) }} ₭</td>
                                            <td class="remove-col"><button class="btn-remove text-danger"
                                                    wire:click="destroy('{{ $item->rowId }}')"><i
                                                        class="icon-close"></i></button></td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table><!-- End .table table-wishlist -->

                            <div class="cart-bottom">

                                <a href="javascript:void(0)" wire:click='deleteallcart' class="btn btn-danger"><i
                                        class="fas fa-trash"></i><span>ລຶບສິນຄ້າທັງຫມົດ</span></a>
                            </div><!-- End .cart-bottom -->
                        </div><!-- End .col-lg-9 -->
                        <aside class="col-lg-4">
                            <div class="summary summary-cart">
                                <h3 class="summary-title"><i class="fas fa-list"></i> ຄິດໄລ່ສິນຄ້າໃນກະຕ່າ</h3>
                                <!-- End .summary-title -->

                                <table class="table table-summary">
                                    <tbody>
                                        <tr class="summary-subtotal">
                                            <td><i class="fas fa-cart-plus"></i> ຈຳນວນສິນຄ້າ:</td>
                                            <td>{{ $this->sumqty }} ລາຍການ</td>
                                        </tr><!-- End .summary-subtotal -->
                                        <tr class="summary-subtotal">
                                            <td><i class="fas fa-dollar-sign"></i> ລວມເປັນເງິນ:</td>
                                            <td>{{ Cart::instance('cart')->subtotal() }} ₭</td>
                                        </tr><!-- End .summary-subtotal -->
                                        @if (Session::has('coupon'))
                                            <tr class="summary-subtotal">
                                                <td><i class="fas fa-percent"></i> ສ່ວນຫລຸດ:</td>
                                                <td>- {{ number_format($discount) }} ₭</td>
                                            </tr><!-- End .summary-subtotal -->
                                        @endif
                                        {{-- <tr class="summary-shipping">
                                        <td><i class="fas fa-truck-moving"></i> ບໍລິການຂົນສົ່ງ:</td>
                                        <td>&nbsp;</td>
                                    </tr>

                                    <tr class="summary-shipping-row">
                                        <td>
                                            <div class="custom-control custom-radio">
                                                <input type="radio" id="free-shipping" name="shipping" class="custom-control-input">
                                                <label class="custom-control-label" for="free-shipping">Free Shipping</label>
                                            </div><!-- End .custom-control -->
                                        </td>
                                        <td>$0.00</td>
                                    </tr><!-- End .summary-shipping-row -->

                                    <tr class="summary-shipping-row">
                                        <td>
                                            <div class="custom-control custom-radio">
                                                <input type="radio" id="standart-shipping" name="shipping" class="custom-control-input">
                                                <label class="custom-control-label" for="standart-shipping">Standart:</label>
                                            </div><!-- End .custom-control -->
                                        </td>
                                        <td>$10.00</td>
                                    </tr><!-- End .summary-shipping-row --> --}}

                                        {{-- <tr class="summary-shipping-row">
                                        <td>
                                            <div class="custom-control custom-radio">
                                                <input type="radio" id="express-shipping" name="shipping" class="custom-control-input">
                                                <label class="custom-control-label" for="express-shipping">Express:</label>
                                            </div><!-- End .custom-control -->
                                        </td>
                                        <td>$20.00</td>
                                    </tr><!-- End .summary-shipping-row --> --}}

                                        <tr class="summary-shipping-estimate">
                                            <td><i class="fas fa-map-marked"></i> ສະຖານທີ່ສົ່ງສິນຄ້າປາຍທາງ<br>
                                                @auth
                                                    @if (!empty(Auth::guard('web')->user()->id))
                                                        <a
                                                            href="{{ route('frontend.profile', auth()->user()->id) }}">ປ່ຽນທີ່ຢູ່ຂອງຂ້ອຍ</a>
                                                    @endif
                                                @endauth
                                            </td>
                                            <td>&nbsp;</td>
                                        </tr><!-- End .summary-shipping-estimate -->

                                        {{-- <tr class="summary-total">
                                        <td><i class="fas fa-money-bill"></i> ລວມເງິນທັງຫມົດ:</td>
                                        <td>{{ Cart::instance('cart')->total() }} ₭</td>
                                    </tr><!-- End .summary-total --> --}}
                                    </tbody>
                                </table><!-- End .table table-summary -->

                                <a href="javascript:void(0)" wire:click.prevent="checkout"
                                    class="btn btn-success btn-order btn-block"><i
                                        class="fas fa-hand-holding-usd"></i>ໄປທີ່ການຊຳລະ</a>
                            </div><!-- End .summary -->

                            <a href="{{ route('home') }}" class="btn btn-primary btn-block mb-3"><i
                                    class="fas fa-cart-plus"></i><span>ສືບຕໍ່ເລຶອກຊື້ສິນຄ້າ</span></a>
                        </aside><!-- End .col-lg-3 -->
                    </div><!-- End .row -->
                </div><!-- End .container -->
            </div><!-- End .cart -->
        @else
            <style>
                @import url(http://fonts.googleapis.com/css?family=Calibri:400,300,700);

                body {
                    background-color: #eee;
                    font-family: 'Calibri', sans-serif !important;
                }

                .mt-100 {
                    margin-top: 10px;

                }


                .card {
                    margin-bottom: 30px;
                    border: 0;
                    -webkit-transition: all .3s ease;
                    transition: all .3s ease;
                    letter-spacing: .5px;
                    border-radius: 8px;
                    -webkit-box-shadow: 1px 5px 24px 0 rgba(68, 102, 242, .05);
                    box-shadow: 1px 5px 24px 0 rgba(68, 102, 242, .05);
                }

                .card .card-header {
                    background-color: #fff;
                    border-bottom: none;
                    padding: 24px;
                    border-bottom: 1px solid #f6f7fb;
                    border-top-left-radius: 8px;
                    border-top-right-radius: 8px;
                }

                .card-header:first-child {
                    border-radius: calc(.25rem - 1px) calc(.25rem - 1px) 0 0;
                }



                .card .card-body {
                    padding: 30px;
                    background-color: transparent;
                }

                .btn-primary,
                .btn-primary.disabled,
                .btn-primary:disabled {
                    background-color: #4466f2 !important;
                    border-color: #4466f2 !important;
                }
            </style>
            <div class="container-fluid text-center">
                <div class="row">

                    <div class="col-md-12">

                        <div class="card">
                            <div class="card-body cart">
                                <div class="col-sm-12 empty-cart-cls text-center">
                                    <img src="https://cdn-icons-png.flaticon.com/512/2037/2037021.png"
                                        style="width: auto; height:100px; margin-left: 46%">
                                    <h3><strong><i class="fas fa-search"></i> ກະຕ່າວ່າງ!</strong></h3>
                                    <h4>ບໍ່ມີສິນຄ້າໃນກະຕ່າໄປເລືອກຊື້ສິນຄ້າເລີຍ :)</h4>
                                    <a href="{{ route('home') }}" class="btn btn-primary cart-btn-transform m-3"
                                        data-abc="true"><i class="fas fa-arrow-alt-circle-left"></i> ໄປທີ່ຮ້ານຄ້າ</a>

                                </div>
                            </div>
                        </div>


                    </div>

                </div>

            </div>
        @endif
    </div><!-- End .page-content -->
</main><!-- End .main -->

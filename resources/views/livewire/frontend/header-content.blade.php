<div class="header-center">
    <div
        class="header-search header-search-extended header-search-visible header-search-no-radius d-none d-lg-block">
        <a href="#" class="search-toggle" role="button"><i class="icon-search"></i></a>
        <form action="{{ route('frontend.search') }}">
            <div class="header-search-wrapper search-wrapper-wide">
                {{-- <div class="select-custom">
                    <select id="cat" name="product_type">
                        <option value="">ຄົ້ນຫາຫມວດຫມູ່ສິນຄ້າ</option>
                        @foreach ($producttypes as $item)
                        <option value="{{ $item->id }}">{{ $item->name }}</option>
                        @endforeach
                    </select>
                </div><!-- End .select-custom --> --}}
                <label for="q" class="sr-only">ຄົ້ນຫາ</label>
                <input type="text" name="search" class="form-control" value="{{ $search }}"
                    placeholder="ຄົ້ນຫາສິນຄ້າ ..." required>
                <button class="btn btn-primary" type="submit"><i class="icon-search"></i></button>
            </div><!-- End .header-search-wrapper -->
        </form>
    </div><!-- End .header-search -->
</div>
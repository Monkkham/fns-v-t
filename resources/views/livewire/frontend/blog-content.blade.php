<main class="main">
    <div class="page-header text-center" style="background-image: linear-gradient(#246dea, #11dfcb">
        <div class="container">
            <h3 class="page-title text-white"><i class="fas fa-blog"></i> ຂ່າວອັບເດດຮ້ານຄ້າ</h3>
        </div><!-- End .container -->
    </div><!-- End .page-header -->
    <nav aria-label="breadcrumb" class="breadcrumb-nav mb-2">
        <div class="container">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="index.html">ຫນ້າຫຼັກ</a></li>
                <li class="breadcrumb-item active" aria-current="page">ຂ່າວອັບເດດ</li>
            </ol>
        </div><!-- End .container -->
    </nav><!-- End .breadcrumb-nav -->

    <div class="page-content">
        <div class="container">
            {{-- <nav class="blog-nav">
                <ul class="menu-cat entry-filter justify-content-center">
                    <li class="active"><a href="#" data-filter="*">All Blog Posts<span>8</span></a></li>
                    <li><a href="#" data-filter=".lifestyle">Lifestyle<span>3</span></a></li>
                    <li><a href="#" data-filter=".shopping">Shopping<span>1</span></a></li>
                    <li><a href="#" data-filter=".fashion">Fashion<span>2</span></a></li>
                    <li><a href="#" data-filter=".travel">Travel<span>3</span></a></li>
                    <li><a href="#" data-filter=".hobbies">Hobbies<span>2</span></a></li>
                </ul><!-- End .blog-menu -->
            </nav><!-- End .blog-nav --> --}}

            <div class="entry-container max-col-4" data-layout="fitRows">
                @foreach ($post_public as $item)
                <div class="entry-item lifestyle shopping col-sm-6 col-md-4 col-lg-3">
                    <article class="entry entry-grid text-center">
                        <figure class="entry-media">
                            <a href="#">
                                <img src="{{ asset('public/'.$item->image) }}" style="height: 200px;" alt="image desc">
                            </a>
                        </figure><!-- End .entry-media -->
                        <div class="entry-body">
                            <div class="entry-meta">
                                <a href="#">ວັນທີ່: {{ date('d/m/Y', strtotime($item->created_at)) }}</a>
                                {{-- <span class="meta-separator">|</span> --}}
                            </div><!-- End .entry-meta -->
                            <div class="entry-content">
                                <p>{!! $item->note !!}</p>
                            </div><!-- End .entry-content -->
                        </div><!-- End .entry-body -->
                    </article><!-- End .entry -->
                </div><!-- End .entry-item -->
                @endforeach
            </div><!-- End .entry-container -->

            <nav aria-label="Page navigation">
                <ul class="pagination justify-content-center">
                        {{ $post_public->links() }}
                </ul>
            </nav>
        </div><!-- End .container -->
    </div><!-- End .page-content -->
</main><!-- End .main -->
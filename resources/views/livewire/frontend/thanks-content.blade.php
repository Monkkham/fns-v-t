  <head>
        <link href="https://cdn.jsdelivr.net/npm/[email protected]/dist/css/bootstrap.min.css" rel="stylesheet">
    </head>

    <body>
        <div class="d-flex justify-content-center align-items-center pt-4">
            <div class="col-md-8">
                <div class="border border-5 border-success"></div>
                <div class="card  bg-white shadow p-5">
                    <div class="mb-4 text-center">
                        <svg xmlns="http://www.w3.org/2000/svg" class="text-success" width="80" height="80"
                            fill="currentColor" class="bi bi-check-circle" viewBox="0 0 16 16">
                            <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z" />
                            <path
                                d="M10.97 4.97a.235.235 0 0 0-.02.022L7.477 9.417 5.384 7.323a.75.75 0 0 0-1.06 1.06L6.97 11.03a.75.75 0 0 0 1.079-.02l3.992-4.99a.75.75 0 0 0-1.071-1.05z" />
                        </svg>
                    </div>
                    <div class="text-center">
                        <h2>ການສັ່ງຊື້ສຳເລັດເເລ້ວ !<br> (ຂໍຂອບໃຈລູກຄ້າ)</h2>
                        <p><i class="fas fa-people-carry"></i> ກະລຸນາລໍຖ້າພະນັກງານກຳລັງຈັດສົ່ງສິນຄ້າເຖິງມືທ່ານໄວໄວນີ້ </p>
                        <a href="{{ route('home') }}" class="btn btn-outline-primary-2"><i class="fas fa-arrow-left"></i> ກັບຄືນສູ່ຫນ້າຫຼັກ</a>
                    </div>
                </div>
            </div>
        </div>
    </body>

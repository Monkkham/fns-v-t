        <main class="main">
        	<div class="page-header text-center" style="background-image: url('assets/images/page-header-bg.jpg')">
        		<div class="container">
        			<h1 class="page-title"><i class="fas fa-heart"></i> ລາຍການສິນຄ້າທີ່ທ່ານມັກ</h1>
        		</div><!-- End .container -->
        	</div><!-- End .page-header -->
            <nav aria-label="breadcrumb" class="breadcrumb-nav">
                <div class="container">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.html">ຫນ້າຫຼັກ</a></li>
                        <li class="breadcrumb-item active" aria-current="page">ສິ່ງທີ່ມັກ</li>
                    </ol>
                </div><!-- End .container -->
            </nav><!-- End .breadcrumb-nav -->

            <div class="page-content">
            	<div class="container">
					<table class="table table-wishlist table-mobile">
						<thead>
							<tr>
								<th>ສິນຄ້າ</th>
								<th>ລາຄາ</th>
								<th>ສະຖານະ</th>
								<th class="text-center" colspan="2">ຈັດການ</th>
							</tr>
						</thead>

						<tbody>
							@if(Cart::instance('wishlist')->count() > 0)
							@foreach(Cart::instance('wishlist')->content() as $item)
							<tr>
								<td class="product-col">
									<div class="product">
										<figure class="product-media">
											<a href="#">
												<img src="{{ asset('public/'.$item->model->image) }}" alt="Product image">
											</a>
										</figure>

										<h3 class="product-title">
											<a href="#">{{ $item->model->name }}</a>
										</h3><!-- End .product-title -->
									</div><!-- End .product -->
								</td>
								<td class="price-col">{{ number_format($item->model->sell_price) }} ກີບ</td>
								@if($item->model->qty)
								<td class="stock-col"><span class="in-stock">In stock</span></td>
								@else
								<td class="stock-col"><span class="out-of-stock"><i class="fas fa-box-open"></i> ສິນຄ້າຫມົດ</span></td>
								@endif
								<td class="action-col">
									@if($item->model->qty > 0)
									<button class="btn btn-block btn-primary" wire:click.prevent="moveProductFormwishListToCart('{{$item->rowId}}')"><i class="icon-cart-plus"></i>ເພີ່ມໃສ່ກະຕ່າ</button>
									@else
									<button disabled class="btn btn-block btn-primary" wire:click.prevent="moveProductFormwishListToCart('{{$item->rowId}}')"><i class="icon-cart-plus"></i>ເພີ່ມໃສ່ກະຕ່າ</button>
									@endif
								</td>
								<td class="remove-col"><button class="btn-remove text-danger" wire:click.prevent="removeWishList({{$item->model->id}})"><i class="icon-close"></i></button></td>
							</tr>
							@endforeach
							@else
							<tr>
								<td colspan="4">
									<a href="{{ route('home') }}" class="btn btn-danger btn-block mb-3"><i class="fas fa-search"></i><span>ບໍ່ມີລາຍການສິນຄ້າທີ່ມັກ! ໄປເລືອກສິນຄ້າເລີຍ</span></a>
								</td>
							</tr>
							@endif
						</tbody>
					</table><!-- End .table table-wishlist -->
	            	<div class="wishlist-share">
	            		<div class="social-icons social-icons-sm mb-2">
	            			<label class="social-label">Share on:</label>
	    					<a href="#" class="social-icon" title="Facebook" target="_blank"><i class="icon-facebook-f"></i></a>
	    					<a href="#" class="social-icon" title="Twitter" target="_blank"><i class="icon-twitter"></i></a>
	    					<a href="#" class="social-icon" title="Instagram" target="_blank"><i class="icon-instagram"></i></a>
	    					<a href="#" class="social-icon" title="Youtube" target="_blank"><i class="icon-youtube"></i></a>
	    					<a href="#" class="social-icon" title="Pinterest" target="_blank"><i class="icon-pinterest"></i></a>
	    				</div><!-- End .soial-icons -->
	            	</div><!-- End .wishlist-share -->
            	</div><!-- End .container -->
            </div><!-- End .page-content -->
        </main><!-- End .main -->
<main class="main">
    <div class="page-header text-center" style="background-image: linear-gradient(#246dea, #11dfcb">
        <div class="container">
            <h3 class="page-title text-white"><i class="fas fa-store"></i> ຮ້ານຄ້າ</h3>
        </div><!-- End .container -->
    </div><!-- End .page-header -->
    <nav aria-label="breadcrumb" class="breadcrumb-nav mb-2">
        <div class="container">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="index.html">ຫນ້າຫຼັກ</a></li>
                <li class="breadcrumb-item"><a href="#">ຮ້ານຄ້າ</a></li>
                <li class="breadcrumb-item active" aria-current="page">ສິນຄ້າ</li>
            </ol>
        </div><!-- End .container -->
    </nav><!-- End .breadcrumb-nav -->

    <div class="page-content">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="toolbox">
                        <div class="toolbox-left">
                            <div class="toolbox-info">
                                <i class="fa fa-cart-plus"></i>   ສິນຄ້າທັງຫມົດໃນຮ້ານຄ້າ
                            </div><!-- End .toolbox-info -->
                        </div><!-- End .toolbox-left -->

                        <div class="toolbox-right">
                                <div class="toolbox-sort">
                                    <label for="sortby"> ເລືອກຫມວດຫມູ່ສິນຄ້າ:</label>
                                    <div class="select-custom">
                                        <select wire:model="product_type_id" class="form-control">
                                            <option value="" selected>---ເລືອກ---</option>
                                            @foreach ($product_type as $item)
                                                <option value="{{ $item->id }}">{{ $item->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div><!-- End .toolbox-sort -->
                        </div><!-- End .toolbox-right -->
                    </div><!-- End .toolbox -->

                    @foreach ($product_all as $item)
                    <div class="products mb-3">
                        <div class="product product-list">
                            <div class="row">
                                <div class="col-4 col-lg-2">
                                    <figure class="product-media">
                                        @if ($item->status_sell == 1)
                                        <span class="product-label label-new">
                                            ໃຫມ່
                                        </span>
                                        @elseif($item->status_sell == 2)
                                        <span class="product-label label-sale">
                                            ຂາຍດີ
                                        </span>
                                        @else
                                        @endif
                                        <a href="javascript:void(0)" wire:click="ViewProductDetail({{ $item->id }})">
                                            <img src="{{ asset('public/'.$item->image) }}" alt="Product image" class="product-image">
                                        </a>
                                    </figure><!-- End .product-media -->
                                </div><!-- End .col-sm-6 col-lg-3 -->

                                <div class="col-6 col-lg-3 order-lg-last">
                                    <div class="product-list-action">
                                        <div class="product-price">
                                       ລາຄາ  {{ number_format($item->sell_price) }} .00 ₭
                                        </div><!-- End .product-price -->
                                       <s>
                                        <div class="product-old">
                                          ປົກກະຕິ  {{ number_format($item->promotion_price) }} .00 ₭
                                           </div><!-- End .product-price -->
                                       </s>
                                        <div class="ratings-container">
                                            <div class="ratings">
                                                <div class="ratings-val" style="width: 20%;"></div><!-- End .ratings-val -->
                                            </div><!-- End .ratings -->
                                            <span class="ratings-text">( 2 Reviews )</span>
                                        </div><!-- End .rating-container -->

                                        {{-- <div class="product-action">
                                            <a href="popup/quickView.html" class="btn-product btn-quickview" title="Quick view"><span>quick view</span></a>
                                            <a href="#" class="btn-product btn-compare" title="Compare"><span>compare</span></a>
                                        </div><!-- End .product-action --> --}}
                                        @if ($this->cartData->where('id', $item->id)->count('id') > 0)
                                        <h4>added</h4>
                                        @else
                                       @if($item->qty > 0)
                                       <button type="button" wire:click="addtoCart({{$item->id}},'{{$item->name}}',{{$item->sell_price}})"  class="btn btn-success rounded"><i class="fas fa-cart-plus"></i> ເກັບໃສ່ກະຕ່າ</button>
                                       @else
                                       <button disabled type="button" wire:click="addtoCart({{$item->id}},'{{$item->name}}',{{$item->sell_price}})"  class="btn btn-success rounded"><i class="fas fa-cart-plus"></i> ເກັບໃສ່ກະຕ່າ</button>
                                       @endif
                                        @endif
                                       
                                        
                                    </div><!-- End .product-list-action -->
                                </div><!-- End .col-sm-6 col-lg-3 -->

                                <div class="col-lg-6">
                                    <div class="product-body product-action-inner">
                                        <a href="javascript:void(0)" wire:click.prevent="addToWishlist({{$item->id}},'{{$item->name}}',{{$item->sell_price}})" class="btn-product btn-wishlist" title="ເພີ່ມໃສ່ລາຍການທີ່ມັກ"></a>
                                        <div class="text-bold">
                                            <a href="javascript:void(0)">ລະຫັດ: {{ $item->code }}</a>
                                        </div><!-- End .product-cat -->
                                        @if(!empty($item->product_type))
                                        <div class="product-cat">
                                            <a href="#">ປະເພດ: {{ $item->product_type->name }}</a>
                                        </div><!-- End .product-cat -->
                                        @endif
                                        <h3 class="product-title"><a href="javascript:void(0)" wire:click="ViewProductDetail({{ $item->id }})">{{ $item->name }}</a></h3><!-- End .product-title -->

                                        <div class="product-content">
                                            <p>
                                                @if ($item->qty > 0)
                                                    <p class="text-success">In Stock</p>
                                                    @else
                                                    <p class="text-danger"><i class="fas fa-box-open"></i> ສິນຄ້າຫມົດ!</p>
                                                @endif
                                            </p>
                                        </div><!-- End .product-content -->
                                        
                                        {{-- <div class="product-nav product-nav-thumbs">
                                            <a href="#" class="active">
                                                <img src="{{ asset('public/'.$item->image) }}" alt="product desc">
                                            </a>
                                            <a href="#">
                                                <img src="assets/images/products/product-4-2-thumb.jpg" alt="product desc">
                                            </a>

                                            <a href="#">
                                                <img src="assets/images/products/product-4-3-thumb.jpg" alt="product desc">
                                            </a>
                                        </div><!-- End .product-nav --> --}}
                                    </div><!-- End .product-body -->
                                </div><!-- End .col-lg-6 -->
                            </div><!-- End .row -->
                        </div><!-- End .product -->
                    </div><!-- End .products -->
                    @endforeach
                    <nav aria-label="Page navigation">
                        <div class="float-right">
                            {{ $product_all->links() }}
                        </div>
                    </nav>
                </div><!-- End .col-lg-9 -->
              
            </div><!-- End .row -->
        </div><!-- End .container -->
    </div><!-- End .page-content -->
</main><!-- End .main -->
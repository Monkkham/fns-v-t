<main class="main">
    <nav aria-label="breadcrumb" class="breadcrumb-nav border-0 mb-0">
        <div class="container">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="index.html">ຫນ້າຫຼັກ</a></li>
                <li class="breadcrumb-item active" aria-current="page">ເຂົ້າສູ່ລະບົບ</li>
            </ol>
        </div><!-- End .container -->
    </nav><!-- End .breadcrumb-nav -->

    <div class="login-page bg-image pt-8 pb-8 pt-md-3 pb-md-3 pt-lg-3 pb-lg-3"
        style="background-image: linear-gradient(30deg,#6495ED,#40E0D0)">
        <div class="container">
            <div class="form-box">
                <div class="form-tab">
                    <ul class="nav nav-pills nav-fill" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link" id="signin-tab-2" data-toggle="tab" href="#signin-2" role="tab"
                                aria-controls="signin-2" aria-selected="false"><i class="fa fa-user-lock"></i>
                                ເຂົ້າສູ່ລະບົບ</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane fade show active" id="register-2" role="tabpanel"
                            aria-labelledby="register-tab-2">
                            <form>
                                <label for="register-password-2">ເບີໂທ</label>
                                <input wire:model="phone" placeholder="8 ຕົວເລກ"
                                    type="phone" class="form-control money @error('phone') is-invalid @enderror">
                                @error('phone')
                                    <span style="color: red" class="error">{{ $message }}</span>
                                @enderror
                                <label for="register-password-2">ລະຫັດຜ່ານ</label>
                                <input wire:model="password" placeholder="**********" type="password"
                                    class="form-control money @error('password') is-invalid @enderror">
                                @error('password')
                                    <span style="color: red" class="error">{{ $message }}</span>
                                @enderror
                                <div class="form-footer">
                                    <button wire:click="login" type="button" class="btn btn-primary">
                                        <span>ເຂົ້າສູ່ລະບົບ</span>
                                        <i class="icon-long-arrow-right"></i>
                                    </button>
                                    <div class="icheck-primary">
                                        <input type="checkbox" id="remember" wire:model="remember">
                                        <label for="remember">
                                          ຈຶ່ຈຳຂ້ອຍໄວ້ 
                                        </label>
                                      </div>
                                   
                                   
                                </div><!-- End .form-footer -->
                            </form>
                            <div class="form-choice">
                                <div class="">
                                    <label for="register-policy-2">ບໍ່ທັນມີບັນຊີເທື່ອ?<a
                                            class="text-primary" href="{{ route('frontend.register') }}">
                                            ລົງທະບຽນໃຫມ່</a>
                                        *</label>
                                </div><!-- End .custom-checkbox -->
                                {{-- <p class="text-center">ຫລື ເຂົ້າສູ່ລະບົບຜ່ານ</p>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <a href="#" class="btn btn-login btn-g">
                                            <i class="icon-google"></i>
                                            Login With Google
                                        </a>
                                    </div><!-- End .col-6 -->
                                    <div class="col-sm-6">
                                        <a href="#" class="btn btn-login  btn-f">
                                            <i class="icon-facebook-f"></i>
                                            Login With Facebook
                                        </a>
                                    </div><!-- End .col-6 -->
                                </div><!-- End .row --> --}}
                            </div><!-- End .form-choice -->
                        </div><!-- .End .tab-pane -->
                    </div><!-- End .tab-content -->
                </div><!-- End .form-tab -->
            </div><!-- End .form-box -->
        </div><!-- End .container -->
    </div><!-- End .login-page section-bg -->
</main><!-- End .main -->

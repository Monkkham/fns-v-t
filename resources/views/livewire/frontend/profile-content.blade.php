<main class="main">
    <nav aria-label="breadcrumb" class="breadcrumb-nav mb-3">
        <div class="container">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('home') }}">ຫນ້າຫຼັກ</a></li>
                <li class="breadcrumb-item active" aria-current="page">ໂປຣຟາຍ</li>
            </ol>
        </div><!-- End .container -->
    </nav><!-- End .breadcrumb-nav -->

    <div class="page-content">
        <div class="dashboard">
            <div class="container">
                <div class="row">
                    <aside class="col-md-2 col-lg-2">
                        <ul class="nav nav-dashboard flex-column mb-3 mb-md-0" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="tab-account-link" data-toggle="tab" href="#tab-account"
                                    role="tab" aria-controls="tab-account" aria-selected="false"><i
                                        class="fas fa-user-circle"></i> ບັນຊີຜູ້ໃຊ້</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="tab-address-link" data-toggle="tab" href="#tab-address"
                                    role="tab" aria-controls="tab-address" aria-selected="true"><i
                                        class="fas fa-history"></i> ປະຫວັດການສັ່ງຊື້</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link text-danger" href="{{ route('frontend.logout') }}"><i
                                        class="fas fa-sign-out-alt"></i> ອອກຈາກລະບົບ</a>
                            </li>
                        </ul>
                    </aside><!-- End .col-lg-3 -->
                    <div class="col-md-8 col-lg-9">
                        <div class="tab-content">
                            <div class="tab-pane fade" id="tab-address" role="tabpanel"
                                aria-labelledby="tab-address-link">
                                @if ($order_history->count() > 0)
                                <div class="page-content">
                                    <div class="container">
                                        <table class="table table-striped table-hover">
                                            <thead>
                                                <tr>
                                                    <th colspan="4">
                                                        <h5><i class="fas fa-cart-arrow-down"></i>
                                                            ປະຫວັດການຊື້ສິນຄ້າຂອງຂ້ອຍ</h5>
                                                    </th>
                                                </tr>
                                                <tr class="bg-light">
                                                    <th class="p-3">ລຳດັບ</th>
                                                    <th>ວັນທີ່</th>
                                                    <th>ລະຫັດສັ່ງຊື້</th>
                                                    <th>ລວມເປັນເງິນ</th>
                                                    <th>ສະຖານະ</th>
                                                </tr>
                                            </thead>
                                            @php
                                                $num = 1;
                                            @endphp
                                            <tbody>
                                                @foreach ($order_history as $item)
                                                    <tr>
                                                        <td class="p-3">{{ $num++ }}</td>
                                                        <td>{{ date('d/m/Y', strtotime($item->created_at)) }}</td>
                                                        <td>{{ $item->code }}</td>
                                                        <td>{{ number_format($item->total) }} ₭</td>
                                                        <td>
                                                            @if ($item->status == 1)
                                                                <p class="badge badge-success p-2"><i
                                                                        class="fa fa-plus-circle"></i>
                                                                    ໃຫມ່
                                                                    <span
                                                                        class="spinner-grow spinner-grow-sm text-white text-center"
                                                                        role="status" aria-hidden="true"></span>
                                                                </p>
                                                            @elseif($item->status == 2)
                                                                <p class="badge badge-warning p-2"><i
                                                                        class="fa fa-truck"></i>
                                                                    ກຳລັງສົ່ງ</p>
                                                            @elseif($item->status == 3)
                                                                <p class="badge badge-info p-2"><i
                                                                        class="fa fa-check-circle"></i>
                                                                    ສົ່ງສຳເລັດ</p>
                                                            @elseif($item->status == 4)
                                                                <p class="badge badge-danger p-2"><i
                                                                        class="fa fa-times-circle"></i>
                                                                    ຍົກເລີກ</p>
                                                            @endif
                                                        </td>
                                                        
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table><!-- End .table table-wishlist -->
                                    </div><!-- End .container -->
                                </div><!-- End .page-content -->
                                @else
                                <style>
                                    @import url(http://fonts.googleapis.com/css?family=Calibri:400,300,700);
                    
                                    body {
                                        background-color: #eee;
                                        font-family: 'Calibri', sans-serif !important;
                                    }
                    
                                    .mt-100 {
                                        margin-top: 10px;
                    
                                    }
                    
                    
                                    .card {
                                        margin-bottom: 30px;
                                        border: 0;
                                        -webkit-transition: all .3s ease;
                                        transition: all .3s ease;
                                        letter-spacing: .5px;
                                        border-radius: 8px;
                                        -webkit-box-shadow: 1px 5px 24px 0 rgba(68, 102, 242, .05);
                                        box-shadow: 1px 5px 24px 0 rgba(68, 102, 242, .05);
                                    }
                    
                                    .card .card-header {
                                        background-color: #fff;
                                        border-bottom: none;
                                        padding: 24px;
                                        border-bottom: 1px solid #f6f7fb;
                                        border-top-left-radius: 8px;
                                        border-top-right-radius: 8px;
                                    }
                    
                                    .card-header:first-child {
                                        border-radius: calc(.25rem - 1px) calc(.25rem - 1px) 0 0;
                                    }
                    
                    
                    
                                    .card .card-body {
                                        padding: 30px;
                                        background-color: transparent;
                                    }
                    
                                    .btn-primary,
                                    .btn-primary.disabled,
                                    .btn-primary:disabled {
                                        background-color: #4466f2 !important;
                                        border-color: #4466f2 !important;
                                    }
                                </style>
                                <div class="container-fluid text-center">
                                    <div class="row">
                    
                                        <div class="col-md-12">
                    
                                            <div class="card">
                                                <div class="card-body cart">
                                                    <div class="col-sm-12 empty-cart-cls text-center">
                                                       
                                                        
                                                        <h4>ບໍ່ມີປະຫວັດການຊື້ໄປທີ່ຮ້ານຄ້າເພື່ອສັ່ງຊື້ :)</h4>
                                                        <a href="{{ route('home') }}" class="btn btn-primary cart-btn-transform m-3"
                                                            data-abc="true"><i class="fas fa-arrow-alt-circle-left"></i> ໄປທີ່ຮ້ານຄ້າ</a>
                    
                                                    </div>
                                                </div>
                                            </div>
                    
                    
                                        </div>
                    
                                    </div>
                    
                                </div>
                            @endif
                            </div><!-- .End .tab-pane -->

                            <div class="tab-pane fade show active" id="tab-account" role="tabpanel"
                                aria-labelledby="tab-account-link">
                                <form>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <label>{{ __('lang.select') }}{{ __('lang.gender') }}</label>
                                            <div class="form-group clearfix">
                                                <div class="icheck-success d-inline">
                                                    <input type="radio" id="radioPrimary1" value="1"
                                                        wire:model="gender" checked>
                                                    <label for="radioPrimary1">
                                                        <p class="text-success">{{ __('lang.famale') }}</p>
                                                    </label>
                                                </div>
                                                <div class="icheck-success d-inline">
                                                    <input type="radio" id="radioPrimary2" value="2"
                                                        wire:model="gender">
                                                    <label for="radioPrimary2">
                                                        <p class="text-danger">{{ __('lang.male') }}</p>
                                                    </label>
                                                </div>
                                            </div>
                                            @error('gender')
                                                <span style="color: red" class="error">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <label>ລະຫັດ</label>
                                            <input disabled wire:model="code" type="text" class="form-control">
                                        </div><!-- End .col-sm-6 -->
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-6">
                                            <label>ຊື່ *</label>
                                            <input wire:model="name" type="text"
                                                class="form-control @error('name') is-invalid @enderror">
                                            @error('name')
                                                <span style="color: red" class="error">{{ $message }}</span>
                                            @enderror
                                        </div><!-- End .col-sm-6 -->
                                        <div class="col-sm-6">
                                            <label>ນາມສະກຸນ *</label>
                                            <input wire:model="lastname" type="text"
                                                class="form-control @error('lastname') is-invalid @enderror">
                                            @error('lastname')
                                                <span style="color: red" class="error">{{ $message }}</span>
                                            @enderror
                                        </div><!-- End .col-sm-6 -->
                                    </div><!-- End .row -->
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <label>ເບີໂທ *</label>
                                            <input wire:model="phone" type="tel"
                                                class="form-control @error('phone') is-invalid @enderror">
                                            @error('phone')
                                                <span style="color: red" class="error">{{ $message }}</span>
                                            @enderror

                                        </div>
                                        <div class="col-sm-6">
                                            <label>ອີເມວ *</label>
                                            <input wire:model="email" type="email"
                                                class="form-control @error('email') is-invalid @enderror">
                                            @error('email')
                                                <span style="color: red" class="error">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group" wire:ignore>
                                                <label>ເລືອກແຂວງ</label>
                                                <select wire:model="province_id" id="select1"
                                                    class="form-control
                                                 @error('province_id') is-invalid @enderror">
                                                    <option value="" selected>{{ __('lang.select') }}</option>
                                                    @foreach ($province as $item)
                                                        <option value="{{ $item->id }}">{{ $item->name_la }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                                @error('province_id')
                                                    <span style="color: red" class="error">{{ $message }}</span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>ເລືອກເມືອງ</label>
                                                <select wire:model="district_id" id="select2"
                                                    class="form-control
                                                 @error('district_id') is-invalid @enderror">
                                                    <option value="" selected>{{ __('lang.select') }}</option>
                                                    @foreach ($district as $item)
                                                        <option value="{{ $item->id }}">{{ $item->name_la }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                                @error('district_id')
                                                    <span style="color: red" class="error">{{ $message }}</span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>ເລືອກບ້ານ</label>
                                                <select wire:model="village_id" id="select3"
                                                    class="form-control
                                                 @error('village_id') is-invalid @enderror">
                                                    <option value="" selected>{{ __('lang.select') }}</option>
                                                    @foreach ($village as $item)
                                                        <option value="{{ $item->id }}">{{ $item->name_la }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                                @error('village_id')
                                                    <span style="color: red" class="error">{{ $message }}</span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <label>ລະຫັດຜ່ານເກົ່າ *</label>
                                    <input wire:model="old_password" type="password"
                                        class="form-control @error('old_password') is-invalid @enderror">
                                    @error('old_password')
                                        <span style="color: red" class="error">{{ $message }}</span>
                                    @enderror

                                    <div class="row">
                                        <div class="col-sm-6">
                                            <label>ລະຫັດຜ່ານໃຫມ່</label>
                                            <input wire:model="password" type="password"
                                                class="form-control @error('password') is-invalid @enderror">
                                            @error('password')
                                                <span style="color: red" class="error">{{ $message }}</span>
                                            @enderror
                                        </div>
                                        <div class="col-sm-6">
                                            <label>ຍືນຍັນລະຫັດຜ່ານໃຫມ່ອີກຄັ້ງ</label>
                                            <input wire:model="confirmpassword" type="password"
                                                class="form-control mb-2 @error('confirmpassword') is-invalid @enderror">
                                            @error('confirmpassword')
                                                <span style="color: red" class="error">{{ $message }}</span>
                                            @enderror

                                        </div>
                                    </div>
                                    <button wire:click="updateProfile" type="button" class="btn btn-primary">
                                        <span><i class="fas fa-edit"></i> ບັນທຶກແກ້ໄຂ</span>
                                        <i class="icon-long-arrow-right"></i>
                                    </button>
                                </form>
                            </div><!-- .End .tab-pane -->
                        </div><!-- .End .tab-pane -->
                    </div><!-- End .col-lg-9 -->
                </div><!-- End .row -->
            </div><!-- End .container -->
        </div><!-- End .dashboard -->
    </div><!-- End .page-content -->
</main><!-- End .main -->

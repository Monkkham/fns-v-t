<main class="main">
    <nav aria-label="breadcrumb" class="breadcrumb-nav border-0 mb-0">
        <div class="container">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('home') }}">ຫນ້າຫຼັກ</a></li>
                <li class="breadcrumb-item active" aria-current="page">ກ່ຽວກັບພວກເຮົາ</li>
            </ol>
            <hr>
        </div><!-- End .container -->
       
    </nav><!-- End .breadcrumb-nav -->
    {{-- <div class="container">
        <div class="page-header page-header-big text-center" style="background-image: url('assets/images/about-header-bg.jpg')">
            <h1 class="page-title text-white">About us<span class="text-white">Who we are</span></h1>
        </div><!-- End .page-header -->
    </div><!-- End .container --> --}}
    @foreach ($about_us as $item)
        <div class="page-content pb-0">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 mb-3 mb-lg-0">
                        <h2 class="title">{{ $item->name }}</h2><!-- End .title -->
                        <p>{!! $item->address !!}</p>
                    </div><!-- End .col-lg-6 -->

                    <div class="col-lg-6">
                        <h2 class="title"></h2><!-- End .title -->
                        <p>{!! $item->note !!}</p>
                    </div><!-- End .col-lg-6 -->
                </div><!-- End .row -->

                <div class="mb-5"></div><!-- End .mb-4 -->
            </div><!-- End .container -->
    @endforeach
    {{-- ================================================= --}}
    <div class="container">
        <hr class="mt-4 mb-6">
        <h2 class="title text-center mb-4">ໂຄງຮ່າງການຈັດຕັ້ງ ຮ້ານຂາຍເຄື່ອງໄຊໂຍ</h2>
        <!-- End .title text-center mb-2 -->
        <h2 class="title text-center mb-4">***=======================***</h2><!-- End .title text-center mb-2 -->
        <hr class="mt-4 mb-6">
        <div class="row">
            @foreach ($employee as $item)
                <div class="col-md-2">
                    <div class="member member-anim">
                        <figure class="member-media">
                            <img src="{{ asset('public/'.$item->image) }}" style="height: 200px; width:180px;"
                                alt="member photo">
                            <figcaption class="member-overlay" style="height: 200px; width:180px;">
                                <div class="member-overlay-content">
                                    <h3 class="member-title">{{ $item->name }}
                                        {{ $item->lastname }}<span>{{ $item->phone }}</span></h3>
                                    <!-- End .member-title -->
                                    <p></p>
                                    <div class="social-icons social-icons-simple">
                                        <a href="#" class="social-icon" title="Facebook" target="_blank"><i
                                                class="icon-facebook-f"></i></a>
                                        <a href="#" class="social-icon" title="Twitter" target="_blank"><i
                                                class="icon-twitter"></i></a>
                                        <a href="#" class="social-icon" title="Instagram" target="_blank"><i
                                                class="icon-instagram"></i></a>
                                    </div><!-- End .soial-icons -->
                                </div><!-- End .member-overlay-content -->
                            </figcaption><!-- End .member-overlay -->
                        </figure><!-- End .member-media -->
                        <div class="member-content">
                            <h3 class="member-title">{{ $item->name }}
                                {{ $item->lastname }}<span>{{ $item->phone }}</span></h3><!-- End .member-title -->
                        </div><!-- End .member-content -->
                    </div><!-- End .member -->
                </div><!-- End .col-md-4 -->
            @endforeach
        </div><!-- End .row -->
    </div><!-- End .container -->
    {{-- ================================================= --}}
    @foreach ($about_us as $item)
        <div class="bg-light-2 pt-6 pb-5 mb-6 mb-lg-8">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 mb-3 mb-lg-0">
                        <h2 class="title"></h2><!-- End .title -->
                        <p class="mb-2">{!! $item->role !!}</p>
                    </div><!-- End .col-lg-5 -->
                </div><!-- End .row -->
            </div><!-- End .container -->
        </div><!-- End .bg-light-2 pt-6 pb-6 -->
    @endforeach
    </div><!-- End .page-content -->
</main><!-- End .main -->

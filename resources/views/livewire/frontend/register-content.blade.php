<main class="main">
    <nav aria-label="breadcrumb" class="breadcrumb-nav border-0 mb-0">
        <div class="container">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="index.html">ຫນ້າຫຼັກ</a></li>
                <li class="breadcrumb-item active" aria-current="page">ລົງທະບຽນ</li>
            </ol>
        </div><!-- End .container -->
    </nav><!-- End .breadcrumb-nav -->

    <div class="login-page bg-image pt-8 pb-8 pt-md-3 pb-md-3 pt-lg-3 pb-lg-3"
        style="background-image: linear-gradient(30deg,#6495ED,#40E0D0)">
        <div class="container">
            <div class="form-box">
                <div class="form-tab">
                    <ul class="nav nav-pills nav-fill" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link" id="signin-tab-2" data-toggle="tab" href="#signin-2" role="tab"
                                aria-controls="signin-2" aria-selected="false"><i class="fa fa-user-plus"></i>
                               ລົງທະບຽນໃຫມ່</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane fade show active" id="register-2" role="tabpanel"
                            aria-labelledby="register-tab-2">
                            <form action="#">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <label>ເລືອກເພດ</label>
                                        <div class="form-group clearfix">
                                            <div class="icheck-success d-inline">
                                                <input type="radio" id="radioPrimary1" value="1" wire:model="gender"
                                                    checked>
                                                <label for="radioPrimary1"><p class="text-success">ຍິງ</p>
                                                </label>
                                            </div>
                                            <div class="icheck-success d-inline">
                                                <input type="radio" id="radioPrimary2" value="2" wire:model="gender">
                                                <label for="radioPrimary2"><p class="text-danger">ຊາຍ</p>
                                                </label>
                                            </div>
                                        </div>
                                        @error('gender')
                                            <span style="color: red" class="error">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <label for="register-password-2">ຊື່ຜູ້ໃຊ້</label>
                                        <input wire:model="name" placeholder="ປ້ອນຊື່" type="text"
                                            class="form-control money @error('name') is-invalid @enderror">
                                        @error('name')
                                            <span style="color: red" class="error">{{ $message }}</span>
                                        @enderror
                                    </div>
                                    <div class="col-sm-6">
                                        <label for="register-password-2">ນາມສະກຸນ</label>
                                        <input wire:model="lastname" placeholder="ປ້ອນນາມສະກຸນ" type="text"
                                            class="form-control money @error('lastname') is-invalid @enderror">
                                        @error('lastname')
                                            <span style="color: red" class="error">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                        
                                <label for="register-password-2">ເບີໂທ</label>
                                <input wire:model="phone" placeholder="020" type="tel"
                                    class="form-control money @error('phone') is-invalid @enderror">
                                @error('phone')
                                    <span style="color: red" class="error">{{ $message }}</span>
                                @enderror
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>ເລືອກແຂວງ</label>
                                            <select wire:model="province_id" class="form-control">
                                                <option value="" selected>{{ __('lang.select') }}</option>
                                                @foreach ($province as $item)
                                                    <option value="{{ $item->id }}">{{ $item->name_la }}</option>
                                                @endforeach
                                            </select>
                                            @error('province_id')
                                                <span style="color: red" class="error">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>ເລືອກເມືອງ</label>
                                            <select wire:model="district_id" class="form-control">
                                                <option value="" selected>{{ __('lang.select') }}</option>
                                                @foreach ($districts as $item)
                                                    <option value="{{ $item->id }}">{{ $item->name_la }}</option>
                                                @endforeach
                                            </select>
                                            @error('district_id')
                                                <span style="color: red" class="error">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>ເລືອກບ້ານ</label>
                                            <select wire:model="village_id" class="form-control">
                                                <option value="" selected>{{ __('lang.select') }}</option>
                                                @foreach ($villages as $item)
                                                    <option value="{{ $item->id }}">{{ $item->name_la }}</option>
                                                @endforeach
                                            </select>
                                            @error('village_id')
                                                <span style="color: red" class="error">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                   </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <label for="register-password-2">ລະຫັດຜ່ານ</label>
                                        <input wire:model="password" placeholder="********" type="password"
                                            class="form-control money @error('password') is-invalid @enderror">
                                        @error('password')
                                            <span style="color: red" class="error">{{ $message }}</span>
                                        @enderror
                                    </div>
                                    <div class="col-sm-6">
                                        <label for="register-password-2">ຍືນຍັນລະຫັດຜ່ານ</label>
                                        <input wire:model="confirm_password" placeholder="********" type="password"
                                            class="form-control money @error('confirm_password') is-invalid @enderror">
                                        @error('confirm_password')
                                            <span style="color: red" class="error">{{ $message }}</span>
                                        @enderror
                                    </div>
                                    @if(Session::has('no_match'))
                                    <span style="color: red" class="error">{{ session::get('no_match') }}</span>
                                @endif
                                </div>
                                <div class="form-footer">
                                    <button wire:click="register" type="button" class="btn btn-primary">
                                        <span>ລົງທະບຽນ</span>
                                        <i class="icon-long-arrow-right"></i>
                                    </button>
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" wire:model="agree" class="custom-control-input" id="register-policy-2" required>
                                        <label class="custom-control-label" for="register-policy-2">ຂ້ອຍຍອມຮັບນະໂຍບາຍຂອງ<a href="{{ route('frontend.about') }}"> ຮ້ານຄ້າ</a> *</label>
                                    </div><!-- End .custom-checkbox -->
                                    @if(Session::has('agree'))
                                        <span style="color: red" class="error">{{ session::get('agree') }}</span>
                                    @endif
                                </div><!-- End .form-footer -->
                            </form>
                            {{-- <div class="form-choice">
                                <p class="text-center">ຫລື ລົງທະບຽນຜ່ານ</p>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <a href="#" class="btn btn-login btn-g">
                                            <i class="icon-google"></i>
                                            Login With Google
                                        </a>
                                    </div><!-- End .col-6 -->
                                    <div class="col-sm-6">
                                        <a href="#" class="btn btn-login  btn-f">
                                            <i class="icon-facebook-f"></i>
                                            Login With Facebook
                                        </a>
                                    </div><!-- End .col-6 -->
                                </div><!-- End .row -->
                            </div><!-- End .form-choice --> --}}
                        </div><!-- .End .tab-pane -->
                    </div><!-- End .tab-content -->
                </div><!-- End .form-tab -->
            </div><!-- End .form-box -->
        </div><!-- End .container -->
    </div><!-- End .login-page section-bg -->
</main><!-- End .main -->

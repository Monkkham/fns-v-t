<main wire:ignore.self class="main">
    <div wire:ignore class="intro-slider-container">
        <div class="intro-slider owl-carousel owl-simple owl-nav-inside" data-toggle="owl"
            data-owl-options='{
                "nav": false,
                "responsive": {
                    "992": {
                        "nav": true
                    }
                }
            }'>
            @foreach ($slider as $item)
                <div class="intro-slide" style="background-image: url({{ asset('public/'.$item->image) }});">
                    <div class="container intro-content">
                        <div class="row">
                            <div class="col-auto offset-lg-3 intro-col">
                                <h3 class="intro-subtitle">{{ $item->note }}</h3><!-- End .h3 intro-subtitle -->
                                <h1 class="intro-title text-white">{{ $item->name }} <br>
                                </h1><!-- End .intro-title -->
                                <a href="{{ route('frontend.shop') }}" class="btn btn-primary">
                                    <span>ເລືອກຊື້ເລີຍ</span>
                                    <i class="icon-long-arrow-right"></i>
                                </a>
                            </div><!-- End .col-auto offset-lg-3 -->
                        </div><!-- End .row -->
                    </div><!-- End .container intro-content -->
                </div><!-- End .intro-slide -->
            @endforeach
        </div><!-- End .owl-carousel owl-simple -->

        <span class="slider-loader"></span><!-- End .slider-loader -->
    </div><!-- End .intro-slider-container -->

    <div class="mb-4"></div><!-- End .mb-2 -->



    <div class="mb-2"></div><!-- End .mb-2 -->

    {{-- <div class="container">
        <div class="row">
            <div class="col-sm-6 col-lg-3">
                <div class="banner banner-overlay">
                    <a href="#">
                        <img src="assets/images/demos/demo-13/banners/banner-1.jpg" alt="Banner">
                    </a>

                    <div class="banner-content">
                        <h4 class="banner-subtitle text-white"><a href="#">Weekend Sale</a></h4>
                        <!-- End .banner-subtitle -->
                        <h3 class="banner-title text-white"><a href="#">Lighting <br>& Accessories <br><span>25%
                                    off</span></a></h3><!-- End .banner-title -->
                        <a href="#" class="banner-link">Shop Now <i class="icon-long-arrow-right"></i></a>
                    </div><!-- End .banner-content -->
                </div><!-- End .banner -->
            </div><!-- End .col-lg-3 -->

            <div class="col-sm-6 col-lg-3 order-lg-last">
                <div class="banner banner-overlay">
                    <a href="#">
                        <img src="assets/images/demos/demo-13/banners/banner-3.jpg" alt="Banner">
                    </a>

                    <div class="banner-content">
                        <h4 class="banner-subtitle text-white"><a href="#">Smart Offer</a></h4>
                        <!-- End .banner-subtitle -->
                        <h3 class="banner-title text-white"><a href="#">Anniversary <br>Special <br><span>15%
                                    off</span></a></h3><!-- End .banner-title -->
                        <a href="#" class="banner-link">Shop Now <i class="icon-long-arrow-right"></i></a>
                    </div><!-- End .banner-content -->
                </div><!-- End .banner -->
            </div><!-- End .col-lg-3 -->

            <div class="col-lg-6">
                <div class="banner banner-overlay">
                    <a href="#">
                        <img src="assets/images/demos/demo-13/banners/banner-2.jpg" alt="Banner">
                    </a>

                    <div class="banner-content">
                        <h4 class="banner-subtitle text-white d-none d-sm-block"><a href="#">Amazing Value</a>
                        </h4><!-- End .banner-subtitle -->
                        <h3 class="banner-title text-white"><a href="#">Clothes Trending <br>Spring Collection
                                2019 <br><span>from $12,99</span></a></h3><!-- End .banner-title -->
                        <a href="#" class="banner-link">Discover Now <i class="icon-long-arrow-right"></i></a>
                    </div><!-- End .banner-content -->
                </div><!-- End .banner -->
            </div><!-- End .col-lg-6 -->
        </div><!-- End .row -->
    </div><!-- End .container --> --}}

    <div class="mb-3"></div><!-- End .mb-3 -->

    <div class="bg-light pt-3 pb-5">
        <div class="container">
            <div class="heading heading-center mb-6">
                <h2 class="title"><i class="fas fa-cart-plus"></i> ສິນຄ້າທັງຫມົດ</h2>
                <!-- End .title -->
            </div>
            <!-- End .heading -->
            <div class="tab-content">
                <div class="tab-pane p-0 fade show active" id="top-all-tab" role="tabpanel"
                    aria-labelledby="top-all-link">
                    <div wire:ignore.self class="products">
                        <div class="row justify-content-center">

                            @foreach ($product_all as $item)
                                <div class="col-6 col-md-4 col-lg-3">
                                    <div class="product">
                                        <figure class="product-media">
                                            @if ($item->status_sell == 1)
                                            <span class="product-label label-new">
                                                ໃຫມ່
                                            </span>
                                            @elseif($item->status_sell == 2)
                                            <span class="product-label label-sale">
                                                ຂາຍດີ
                                            </span>
                                            @else
                                            @endif
                                            <a href="javascript:void(0)"
                                                wire:click="ViewProductDetail({{ $item->id }})">
                                                <img src="{{ asset('public/'.$item->image) }}" style="width: 100%; height: 180px;">
                                            </a>

                                            <div class="product-action-vertical">
                                                <button type="button"
                                                    wire:click="addToWishlist({{ $item->id }},'{{ $item->name }}',{{ $item->sell_price }})"
                                                    class="btn-product-icon btn-wishlist btn-expandable"><span>ເພີ່ມສິ່ງທີ່ມັກ</span></button>
                                            </div><!-- End .product-action-vertical -->
                                        </figure><!-- End .product-media -->

                                        <div class="product-body">
                                            <div class="product-cat">
                                                @if (!empty($item->product_type))
                                                    <a href="#">ປະເພດ:
                                                        {{ $item->product_type->name }}
                                                    @else
                                                @endif
                                                </a>
                                            </div><!-- End .product-cat -->
                                            <h3 class="product-title"><a href="product.html">{{ $item->name }}</a>
                                                <p>
                                                    @if ($item->qty > 0)
                                                        <p class="text-success">In Stock</p>
                                                    @endif
                                                </p>
                                            </h3><!-- End .product-title -->
                                            <div class="product-price">
                                                <span class="new-price">ລາຄາ:
                                                    {{ number_format($item->promotion_price) }} ₭</span>
                                            </div><!-- End .product-price -->
                                            <div class="product">
                                               <s>
                                                <span class="old-price">ປົກກະຕິ: {{ number_format($item->sell_price) }}
                                                    ₭</span>
                                               </s>
                                            </div>
                                            <div class="ratings-container">
                                            </div><!-- End .rating-container -->
                                            {{-- @if ($this->isInCart($item->id))
                        <h4>Addedd</h4>
                        @else --}}
                                            <div class="product">
                                                @if ($item->qty > 0)
                                                {{-- @if ($cartData->where('id', $item->id)->count() > 0)
                                                            <h3>Addedd</h3>
                                                            @else --}}
                                                            <button type="button"
                                                            wire:click="addtoCart({{ $item->id }},'{{ $item->name }}',{{ $item->sell_price }})"
                                                            class="btn btn-success col-lg-12" title="ເກັບໃສ່ກະຕ່າ"><span><i
                                                                    class="fas fa-cart-plus"></i>
                                                                ເກັບໃສ່ກະຕ່າ</span></button>
                                                            {{-- @endif --}}
                                                @else
                                                    <p class="text-danger text-center"><i class="fas fa-box-open"></i>
                                                        ສິນຄ້າຫມົດ!</p>
                                                    <button disabled type="button"
                                                        wire:click="addtoCart({{ $item->id }},'{{ $item->name }}',{{ $item->sell_price }})"
                                                        class="btn btn-success col-lg-12" title="ເກັບໃສ່ກະຕ່າ"><span><i
                                                                class="fas fa-cart-plus"></i>
                                                            ເກັບໃສ່ກະຕ່າ</span></button>
                                                @endif
                                            </div><!-- End .product-action -->
                                            {{-- @endif --}}

                                        </div><!-- End .product-body -->
                                    </div><!-- End .product -->
                                </div>
                            @endforeach

                            <!-- End .col-sm-6 col-md-4 col-lg-3 -->
                        </div>
                        <nav aria-label="Page navigation">
                            <ul class="pagination justify-content-center">
                                {{ $product_all->links() }}
                            </ul>
                        </nav>
                        <!-- End .row -->
                    </div>
                    <!-- End .products -->
                </div>
                <!-- .End .tab-pane -->
            </div>
            <!-- End .tab-content -->
        </div><!-- End .container -->
    </div><!-- End .bg-light pt-5 pb-5 -->

    <div class="mb-3"></div><!-- End .mb-3 -->

    <div wire:ignore class="container electronics">
        <div class="heading heading-flex heading-border mb-3">
            <div class="heading-left">
                <h2 class="title">ປະເພດເຄື່ອງໃຊ້</h2><!-- End .title -->
            </div><!-- End .heading-left -->

            <div class="heading-right">
                <ul class="nav nav-pills nav-border-anim justify-content-center" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="elec-new-link" data-toggle="tab" href="#elec-new-tab"
                            role="tab" aria-controls="elec-new-tab" aria-selected="true">ໃຫມ່</a>
                    </li>
                </ul>
            </div><!-- End .heading-right -->
        </div><!-- End .heading -->

        <div class="tab-content tab-content-carousel">
            <div class="tab-pane p-0 fade show active" id="elec-new-tab" role="tabpanel"
                aria-labelledby="elec-new-link">
                <div class="owl-carousel owl-simple carousel-equal-height carousel-with-shadow" data-toggle="owl"
                    data-owl-options='{
                        "nav": false, 
                        "dots": true,
                        "margin": 20,
                        "loop": false,
                        "responsive": {
                            "0": {
                                "items":2
                            },
                            "480": {
                                "items":2
                            },
                            "768": {
                                "items":3
                            },
                            "992": {
                                "items":4
                            },
                            "1280": {
                                "items":5,
                                "nav": true
                            }
                        }
                    }'>
                    @foreach ($product_type_use as $item)
                        <div class="product">
                            <figure class="product-media">

                                @if ($item->status_sell == 1)
                                    <span class="product-label label-new">
                                        ໃຫມ່
                                    </span>
                                @elseif($item->status_sell == 2)
                                    <span class="product-label label-sale">
                                        ຂາຍດີ
                                    </span>
                                @else
                                @endif

                                <a href="javascript:void(0)"
                                wire:click="ViewProductDetail({{ $item->id }})">
                                    <img src="{{ asset('public/'.$item->image) }}" style="width: 100%; height: 160px;">
                                </a>

                                <div class="product-action-vertical">
                                    <button type="button"
                                        wire:click.prevent="addToWishlist({{ $item->id }},'{{ $item->name }}',{{ $item->sell_price }})"
                                        class="btn-product-icon btn-wishlist btn-expandable"><span>ເພີ່ມສິ່ງທີ່ມັກ</span></button>
                                    {{-- <a href="#" class="btn-product-icon btn-compare" title="Compare"><span>Compare</span></a>
                                <a href="popup/quickView.html" class="btn-product-icon btn-quickview" title="Quick view"><span>Quick view</span></a> --}}
                                </div><!-- End .product-action-vertical -->
                            </figure><!-- End .product-media -->

                            <div class="product-body">
                                <div class="product-cat">
                                    @if (!empty($item->product_type))
                                        <a href="#">ປະເພດ:
                                            {{ $item->product_type->name }}
                                        @else
                                    @endif
                                    </a>
                                </div><!-- End .product-cat -->
                                <h3 class="product-title"><a href="product.html">{{ $item->name }}</a></h3>
                                <p>
                                    @if ($item->qty > 0)
                                        <p class="text-success">In Stock</p>
                                    @else
                                        <p class="text-danger">Out of stock</p>
                                    @endif
                                </p>
                                <!-- End .product-title -->
                                <div class="product-price">
                                    <span class="new-price">ລາຄາ: {{ number_format($item->promotion_price) }}
                                        ₭</span>
                                   <s>
                                    <span class="old-price">ປົກກະຕິ: {{ number_format($item->sell_price) }}
                                        ₭</span>
                                   </s>
                                </div><!-- End .product-price -->
                                <div class="ratings-container pb-1">
                                </div><!-- End .rating-container -->
                                @if ($item->qty > 0)
                                    <div class="product pb-1">
                                        <button type="button"
                                            wire:click="addtoCart({{ $item->id }},'{{ $item->name }}',{{ $item->sell_price }})"
                                            class="btn-product btn-cart col-lg-12"
                                            title="ເກັບໃສ່ກະຕ່າ"><span>ເກັບໃສ່ກະຕ່າ</span></button>
                                    </div><!-- End .product-action -->
                                @else
                                    <p class="text-danger text-center"><i class="fas fa-box-open"></i> ສິນຄ້າຫມົດ!</p>
                                    <div class="product pb-1">
                                        <button disabled type="button"
                                            wire:click="addtoCart({{ $item->id }},'{{ $item->name }}',{{ $item->sell_price }})"
                                            class="btn-product btn-cart col-lg-12"
                                            title="ເກັບໃສ່ກະຕ່າ"><span>ເກັບໃສ່ກະຕ່າ</span></button>
                                    </div><!-- End .product-action -->
                                @endif
                            </div><!-- End .product-body -->
                        </div><!-- End .product -->
                    @endforeach
                </div><!-- End .owl-carousel -->
            </div><!-- .End .tab-pane -->
        </div><!-- End .tab-content -->
    </div><!-- End .container -->

    <div class="mb-3"></div><!-- End .mb-3 -->


    <div class="mb-1"></div><!-- End .mb-1 -->

    <div wire:ignore class="container furniture">
        <div class="heading heading-flex heading-border mb-3">
            <div class="heading-left">
                <h2 class="title">ປະເພດອາຫານ</h2><!-- End .title -->
            </div><!-- End .heading-left -->

            <div class="heading-right">
                <ul class="nav nav-pills nav-border-anim justify-content-center" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="furn-new-link" data-toggle="tab" href="#furn-new-tab"
                            role="tab" aria-controls="furn-new-tab" aria-selected="true">ໃຫມ່</a>
                    </li>
                </ul>
            </div><!-- End .heading-right -->
        </div><!-- End .heading -->

        <div class="tab-content tab-content-carousel">
            <div class="tab-pane p-0 fade show active" id="furn-new-tab" role="tabpanel"
                aria-labelledby="furn-new-link">
                <div class="owl-carousel owl-simple carousel-equal-height carousel-with-shadow" data-toggle="owl"
                    data-owl-options='{
                        "nav": false, 
                        "dots": true,
                        "margin": 20,
                        "loop": false,
                        "responsive": {
                            "0": {
                                "items":2
                            },
                            "480": {
                                "items":2
                            },
                            "768": {
                                "items":3
                            },
                            "992": {
                                "items":4
                            },
                            "1280": {
                                "items":5,
                                "nav": true
                            }
                        }
                    }'>
                    @foreach ($product_type_food as $item)
                        <div class="product">
                            <figure class="product-media">
                                @if ($item->status_sell == 1)
                                <span class="product-label label-new">
                                    ໃຫມ່
                                </span>
                                @elseif($item->status_sell == 2)
                                <span class="product-label label-sale">
                                    ຂາຍດີ
                                </span>
                                @else
                                @endif
                                <a href="javascript:void(0)"
                                wire:click="ViewProductDetail({{ $item->id }})">
                                    <img src="{{ asset('public/'.$item->image) }}" style="width: 100%; height: 160px;">
                                </a>

                                <div class="product-action-vertical">
                                    <button type="button"
                                        wire:click.prevent="addToWishlist({{ $item->id }},'{{ $item->name }}',{{ $item->sell_price }})"
                                        class="btn-product-icon btn-wishlist btn-expandable"><span>ເພີ່ມສິ່ງທີ່ມັກ</span></button>
                                    {{-- <a href="#" class="btn-product-icon btn-compare" title="Compare"><span>Compare</span></a>
                                <a href="popup/quickView.html" class="btn-product-icon btn-quickview" title="Quick view"><span>Quick view</span></a> --}}
                                </div><!-- End .product-action-vertical -->
                            </figure><!-- End .product-media -->

                            <div class="product-body">
                                <div class="product-cat">
                                    @if (!empty($item->product_type))
                                        <a href="#">ປະເພດ:
                                            {{ $item->product_type->name }}
                                        @else
                                    @endif
                                    </a>
                                </div><!-- End .product-cat -->
                                <h3 class="product-title"><a href="product.html">{{ $item->name }}</a></h3>
                                <p>
                                    @if ($item->qty > 0)
                                        <p class="text-success">In Stock</p>
                                    @else
                                        <p class="text-danger">Out of stock</p>
                                    @endif
                                </p>
                                <!-- End .product-title -->
                                <div class="product-price">
                                    <span class="new-price">ລາຄາ: {{ number_format($item->promotion_price) }}
                                        ₭</span>
                                   <s>
                                    <span class="old-price">ປົກກະຕິ: {{ number_format($item->sell_price) }}
                                        ₭</span>
                                   </s>
                                </div><!-- End .product-price -->
                                <div class="ratings-container pb-1">
                                </div><!-- End .rating-container -->
                                @if ($item->qty > 0)
                                    <div class="product pb-1">
                                        <button type="button"
                                            wire:click="addtoCart({{ $item->id }},'{{ $item->name }}',{{ $item->sell_price }})"
                                            class="btn-product btn-cart col-lg-12"
                                            title="ເກັບໃສ່ກະຕ່າ"><span>ເກັບໃສ່ກະຕ່າ</span></button>
                                    </div><!-- End .product-action -->
                                @else
                                    <p class="text-danger text-center"><i class="fas fa-box-open"></i> ສິນຄ້າຫມົດ!</p>
                                    <div class="product pb-1">
                                        <button disabled type="button"
                                            wire:click="addtoCart({{ $item->id }},'{{ $item->name }}',{{ $item->sell_price }})"
                                            class="btn-product btn-cart col-lg-12"
                                            title="ເກັບໃສ່ກະຕ່າ"><span>ເກັບໃສ່ກະຕ່າ</span></button>
                                    </div><!-- End .product-action -->
                                @endif
                            </div><!-- End .product-body -->
                        </div><!-- End .product -->
                    @endforeach
                </div><!-- End .owl-carousel -->
            </div><!-- .End .tab-pane -->
        </div><!-- End .tab-content -->
    </div><!-- End .container -->

    <div class="mb-3"></div><!-- End .mb-3 -->


    <div class="mb-3"></div><!-- End .mb-3 -->

    {{-- <div wire:ignore class="container">
        <h2 class="title title-border mb-5">ແບຣນສິນຄ້າ</h2><!-- End .title -->
        <div class="owl-carousel mb-5 owl-simple" data-toggle="owl"
            data-owl-options='{
                "nav": false, 
                "dots": true,
                "margin": 30,
                "loop": false,
                "responsive": {
                    "0": {
                        "items":2
                    },
                    "420": {
                        "items":3
                    },
                    "600": {
                        "items":4
                    },
                    "900": {
                        "items":5
                    },
                    "1024": {
                        "items":6
                    },
                    "1280": {
                        "items":6,
                        "nav": true,
                        "dots": false
                    }
                }
            }'>
            <a href="#" class="brand">
                <img src="assets/images/brands/1.png" alt="Brand Name">
            </a>

            <a href="#" class="brand">
                <img src="assets/images/brands/2.png" alt="Brand Name">
            </a>

            <a href="#" class="brand">
                <img src="assets/images/brands/3.png" alt="Brand Name">
            </a>

            <a href="#" class="brand">
                <img src="assets/images/brands/4.png" alt="Brand Name">
            </a>

            <a href="#" class="brand">
                <img src="assets/images/brands/5.png" alt="Brand Name">
            </a>

            <a href="#" class="brand">
                <img src="assets/images/brands/6.png" alt="Brand Name">
            </a>

            <a href="#" class="brand">
                <img src="assets/images/brands/7.png" alt="Brand Name">
            </a>
        </div><!-- End .owl-carousel -->
    </div><!-- End .container --> --}}

    <div class="cta cta-horizontal cta-horizontal-box bg-primary">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-2xl-5col">
                    <h3 class="cta-title text-white">ຕິດຕໍ່ຮ້ານຄ້າ</h3><!-- End .cta-title -->
                    <p class="cta-desc text-white">ສົ່ງອີເມວເພື່ອຮ້ອງຂໍລະຫັດສ່ວນຫຼຸດສິນຄ້າ</p>
                    <!-- End .cta-desc -->
                </div><!-- End .col-lg-5 -->

                <div class="col-3xl-5col">
                    <form action="#">
                        <div class="input-group">
                            <input type="email" class="form-control form-control-white"
                                placeholder="Enter your Email Address" aria-label="Email Adress" required>
                            <div class="input-group-append">
                                <button class="btn btn-outline-white-2" type="submit"><span>Subscribe</span><i
                                        class="icon-long-arrow-right"></i></button>
                            </div><!-- .End .input-group-append -->
                        </div><!-- .End .input-group -->
                    </form>
                </div><!-- End .col-lg-7 -->
            </div><!-- End .row -->
        </div><!-- End .container -->
    </div><!-- End .cta -->

    <div wire:ignore class="blog-posts bg-light pt-4 pb-7">
        <div class="container">
            <h2 class="title"><i class="fas fa-blog"></i> ອັບເດດຂ່າວກ່ຽວກັບຮ້ານຄ້າ</h2>
            <!-- End .title-lg text-center -->
            <div class="owl-carousel owl-simple" data-toggle="owl"
                data-owl-options='{
                    "nav": false, 
                    "dots": true,
                    "items": 3,
                    "margin": 20,
                    "loop": false,
                    "responsive": {
                        "0": {
                            "items":1
                        },
                        "600": {
                            "items":2
                        },
                        "992": {
                            "items":3
                        },
                        "1280": {
                            "items":4,
                            "nav": true, 
                            "dots": false
                        }
                    }
                }'>
                @foreach ($post_public as $item)
                    <article class="entry">
                        <figure class="entry-media">
                            <a href="#">
                                <img src="{{ asset('public/'.$item->image) }}" style="width: 100%; height: 200px;" alt="image desc">
                            </a>
                        </figure><!-- End .entry-media -->

                        <div class="entry-body">
                            <div class="entry-meta">
                                <a href="#">ວັນທີ່: {{ $item->created_at }}</a>
                            </div><!-- End .entry-meta -->

                            <div class="entry-content">
                                <p>{!! $item->note !!}</p>
                            </div><!-- End .entry-content -->
                        </div><!-- End .entry-body -->
                    </article><!-- End .entry -->
                @endforeach

            </div><!-- End .owl-carousel -->
        </div><!-- End .container -->
    </div><!-- End .blog-posts -->
</main><!-- End .main -->

<?php

namespace App\Http\Livewire\Backend\Store;

use Livewire\Component;
use App\Models\PostPublic;
use Livewire\WithPagination;
use Livewire\WithFileUploads;
use Carbon\Carbon;

class PostContent extends Component
{
    use WithFileUploads;
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    public $note,$image,$newimage,$search,$ID,$note_update;
    public function render()
    {
        $post = PostPublic::orderBy('id','desc')
        ->where('note','like','%' . $this->search. '%')
        ->paginate(5);
        return view('livewire.backend.store.post-content',compact('post'))->layout('layouts.backend.base');
    }
    public function resetform()
    {
        $this->image = '';
        $this->note = '';
    }
    public function create()
    {
        $this->resetform();
        $this->dispatchBrowserEvent('show-modal-add');
    }

    public function store()
    {
        $this->validate([
            'note'=>'required',
            // 'image'=>'required',
        ],[
            'note.required'=>'ປ້ອນຂໍ້ມູນກ່ອນ!',
            // 'image.required'=>'ເລືອກຮູບພາບກ່ອນ!',
        ]);
        $data = new PostPublic();
            //upload image
            if (!empty($this->image)) {
                $this->validate([
                    'image' => 'required|mimes:jpg,png,jpeg',
                ]);
                $imageName = Carbon::now()->timestamp . '.' . $this->image->extension();
                $this->image->storeAs('frontend/post', $imageName);
                $data->image = 'frontend/post'.'/'.$imageName;
            }else{
                $data->image = '';
            }
        $data->note = $this->note;
        $data->save();
        $this->dispatchBrowserEvent('hide-modal-add');
        // $this->emit('alert', ['type' => 'success', 'message' => 'ບັນທຶກຂໍ້ມູນສຳເລັດ!']);
        $this->dispatchBrowserEvent('swal', [
            'title' => 'ບັນທຶກຂໍ້ມູນສຳເລັດ!',
            'icon'=>'success',
            'iconColor'=>'green',
        ]);
        $this->resetform();
}
    public function edit($ids)
    {
        $this->dispatchBrowserEvent('show-modal-edit');

        $Data = PostPublic::find($ids);
        $this->ID = $Data->id;
        $this->newimage = $Data->image;
        $this->note = $Data->note;
    }
    public function update()
    {
        // $this->validate([
        //     'note'=>'required',
        //     'image'=>'required',
        // ],[
        //     'note.required'=>'ປ້ອນຂໍ້ມູນກ່ອນ!',
        //     'image.required'=>'ເລືອກຮູບພາບກ່ອນ!',
        // ]);
        $ids = $this->ID;
        $data = PostPublic::find($ids);
        $data->note = $this->note;
        if ($this->image) {
            $this->validate([
                'image' => 'required|mimes:png,jpg,jpeg',
            ]);
            if ($this->image) {
                $this->validate([
                    'image' => 'required|mimes:png,jpg,jpeg',
                ]);
                if ($this->image != $data->image) {
                    if (!empty($data->image)) {
                        $images = explode(",", $data->images);
                        foreach ($images as $image) {
                            unlink('' . '' . $data->image);
                        }
                        $data->delete();
                    }
                }
                $imageName = Carbon::now()->timestamp . '.' . $this->image->extension();
                $this->image->storeAs('frontend/post', $imageName);
                $data->image = 'frontend/post'.'/'.$imageName;
            }
        }
        $data->save();
        $this->dispatchBrowserEvent('hide-modal-edit');
        // $this->emit('alert', ['type' => 'success', 'message' => 'ແກ້ໄຂຂໍ້ມູນສຳເລັດ!']);
        $this->dispatchBrowserEvent('swal', [
            'title' => 'ແກ້ໄຂຂໍ້ມູນສຳເລັດ!',
            'icon'=>'success',
            'iconColor'=>'green',
        ]);
        $this->resetform();
}
public function showDestroy($ids)
{
    $this->dispatchBrowserEvent('show-modal-delete');
    $Data = PostPublic::find($ids);
    $this->ID = $Data->id;
    $this->note = $Data->note;
}
public function destroy()
{
    $ids = $this->ID;
    $data = PostPublic::find($ids);
    $data->delete();
    $this->dispatchBrowserEvent('hide-modal-delete');
    // $this->emit('alert', ['type' => 'success', 'message' => 'ລຶບຂໍ້ມູນສຳເລັດ!']);
    $this->dispatchBrowserEvent('swal', [
        'title' => 'ລຶບຂໍ້ມູນສຳເລັດ!',
        'icon'=>'success',
        'iconColor'=>'green',
    ]);
}
}

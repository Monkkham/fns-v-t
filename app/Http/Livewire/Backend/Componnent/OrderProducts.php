<?php

namespace App\Http\Livewire\Backend\Componnent;

use App\Models\Orders;
use App\Models\Order_detail;
use App\Models\Products;
use App\Models\Product_type;
use App\Models\Supplier;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Support\Facades\DB;
use Livewire\Component;
use Livewire\WithPagination;

class OrderProducts extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    public $no1 = 1;
    public $product_type_id;
    public $search;
    public $cartData;
    public $cartCount;
    public $cartTotal;
    public $supplier_id, $suppiler_data;
    // public $suplyerSelect;
    // public function mount()
    // {
    //     $this->suplyerSelect = Supplier::all();
    // }
    public function render()
    {
        $suppliers = Supplier::all();
        if (!empty($this->supplier_id)) {
            $this->suppiler_data = Supplier::orderBy('id', 'desc')
                ->where('id', $this->supplier_id)->first();
        }
        $product_type = Product_type::orderBy('id', 'desc')->get();
        if (!empty($this->product_type_id)) {
            if (!empty($this->search)) {
                $products = Products::orderBy('id', 'desc')
                    ->where('product_type_id', $this->product_type_id)
                    ->where(function ($query) {
                        $query->where('code', 'like', '%' . $this->search . '%')
                            ->orWhere('name', 'like', '%' . $this->search . '%');
                    })->paginate(12);
            } else {
                $products = Products::orderBy('id', 'desc')
                    ->where('product_type_id', $this->product_type_id)
                    ->paginate(12);
            }
        } else {
            if (!empty($this->search)) {
                $products = Products::orderBy('id', 'desc')
                    ->where(function ($query) {
                        $query->where('code', 'like', '%' . $this->search . '%')
                            ->orWhere('name', 'like', '%' . $this->search . '%');
                    })->paginate(12);
            } else {
                $products = Products::orderBy('id', 'desc')
                    ->paginate(15);
            }
        }
        $this->cartData = Cart::content();
        $this->cartCount = Cart::count();
        $this->cartTotal = Cart::subTotal();
        return view('livewire.backend.componnent.order-products', compact('products', 'suppliers', 'product_type'))->layout('layouts.backend.base');
    }

    // add to cart
    public function addCart($p_id)
    {
        $products = Products::findOrFail($p_id);
        $itemsId = $products->id;
        $itemsNames = $products->name;
        $itemsQtys = 1;
        $itemsPrices = (float) $products->sell_price;
        Cart::add($itemsId, $itemsNames, $itemsQtys, $itemsPrices);
        $this->emit('alert', ['type' => 'success', 'message' => 'ເພີ່ມໃສ່ກະຕ່າເເລ້ວ!']);
    }
    // update cart
    public $rowId, $qty;
    public function updateCart($qty, $rowId, $status)
    {
        if ($status == 1) {
            $this->rowId = $rowId;
            $this->qty = $qty + 1;
            Cart::update($this->rowId, $this->qty);
            $this->emit('alert', ['type' => 'success', 'message' => 'ເພີ່ມຂື້ນ+!']);
            return;
        } else {
            $this->rowId = $rowId;
            $this->qty = $qty - 1;
            Cart::update($this->rowId, $this->qty);
            $this->emit('alert', ['type' => 'error', 'message' => 'ລືບອອກ-!']);
            return;
        }
    }
    // remove cart
    public function removeCart()
    {
        Cart::destroy();
        $this->emit('alert', ['type' => 'success', 'message' => 'ລຶບລ້າງກະຕ່າ!']);
        return;
    }

    public $nas = 0;
    public function _updateQty($Id, $qty)
    {
        if ($this->nas > $qty) {
            $Qty = $this->nas;
            Cart::update($Id, $Qty);
        } else {
            $this->nas = $qty;
            Cart::update($Id, $qty);
        }
    }
    // show cart with modal
    public function _checkout()
    {
        if ($this->cartCount == 0) {
            $this->emit('alert', ['type' => 'error', 'message' => 'ກະລຸນາເພີ່ມສິນຄ້າໃສ່ກະຕ່າກ່ອນ!']);
            return;
        } else {
            $this->showaddform();
        }
    }
    // sale save
    public $suplyer;
    public function _sale()
    {
        $this->validate([
            'suplyer' => 'required',
        ]);
        DB::beginTransaction();
        // try {
        $sale = new Orders();
        $sale->code = 'NN' . rand(100000, 999999);
        $sale->total_money = str_replace(",", "", $this->cartTotal);
        $sale->status = 1;
        $sale->payment = 1;
        $sale->mode = 'onepay';
        $sale->supplier_id = $this->suplyer;
        $sale->employee_id = auth()->user()->id;
        $sale->note = 'order';
        $sale->save();
        $cartData = Cart::content();
        foreach ($cartData as $item) {
            if (isset($item->id)) {
                $saleItem = new Order_detail();
                $saleItem->order_id = $sale->id;
                $saleItem->product_id = $item->id;
                $saleItem->amount = $item->qty;
                $saleItem->subtotal = $item->qty * $item->price;
                $saleItem->save();
                DB::commit();
            }
        }
        DB::commit();
        $this->closeaddform();
        Cart::destroy();
        $this->resetForm();
        $this->dispatchBrowserEvent('swal:confirm', [
            'type' => 'success',
            'message' => 'ສ້າງບິນສັ່ງຊື້ສິນຄ້າສຳເລັດ!',
            'text' => 'ຂໍຂອບໃຈ',
        ]);
        // $id = $sale->id;
        // return redirect()->route('backend.printorder', ['slug_id' => $id]);

        // $this->emit('alert', ['type' => 'success', 'message' => 'orderded!']);
        // $id = $sale->id;
        // return redirect()->route('sale-pos-print', ['id' => $id]);
        // } catch (\Exception $e) {
        //     DB::rollback();
        //     $this->emit('alert', ['type' => 'error', 'message' => 'something wrong!']);
        //     $this->resetForm();
        // }
    }
    // reset supplier
    public function resetForm()
    {
        $this->suplyer = '';
    }
    // show form modal

    protected function showaddform()
    {
        $this->dispatchBrowserEvent('showforma');
    }
    protected function closeaddform()
    {
        $this->dispatchBrowserEvent('closeforma');
    }
}

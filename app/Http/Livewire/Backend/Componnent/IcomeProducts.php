<?php

namespace App\Http\Livewire\Backend\Componnent;

use App\Models\Orders;
use Livewire\Component;
use App\Models\Products;
use App\Models\Order_detail;
use Livewire\WithPagination;
use Gloudemans\Shoppingcart\Facades\Cart;
use App\Models\Orders as ModelsOrders;

class IcomeProducts extends Component
{
    public $no1 = 1, $no2 = 1;
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    public $search;
    public $icomeOrderItems;
    public $ID,$supplier,$supplier_data,$employee_data;
    public $orderdetail_data = [];
    public $orderdetail_sum_amount,$orderdetail_sum_subtotal,$payment,$status;
    public function render()
    {
        $imports_orders = Orders::orderBy('id', 'desc')->where('code', 'like', '%' . $this->search . '%')->paginate(5);
        return view('livewire.backend.componnent.icome-products', compact('imports_orders'))->layout('layouts.backend.base');
    }

    // ================== ສະເເດງລາຍລະອຽດໂຄງການ ======================= //
    public function show_bill($ids)
    {
        $this->dispatchBrowserEvent('show-modal-bill');
        $data = Orders::find($ids);
        $this->ID = $data->id;
        $this->employee_data = $data->employee;
        $this->supplier_data = $data->supplier;
        $this->payment = $data->payment;
        $this->status = $data->status;
        $this->created_at = $data->created_at;
        $this->orderdetail_data = Order_detail::where('order_id', $this->ID)->get();
        $this->orderdetail_sum_amount = Order_detail::where('order_id', $this->ID)->sum('amount');
        $this->orderdetail_sum_subtotal = Order_detail::where('order_id', $this->ID)->sum('subtotal');
    }
    // show order
    public $code, $subtotal, $count_product, $product_id, $order_id, $created_at, $total, $amount, $orderId;
    public $importOrderItems;
    public function showorder($ids)
    {
        $this->showaddform();
        $importOrder = Orders::find($ids);
        $this->code = $importOrder->code;
        $this->created_at = $importOrder->created_at;
        $this->total_money = $importOrder->total_money;
        $this->count_product = Order_detail::where('order_id', $ids)->count();
        $this->subtotal = Order_detail::where('order_id', $ids)->sum('subtotal');
        $this->importOrderItems = Order_detail::where('order_id', $ids)->get();
        $this->orderId = $ids;
    }
    public function DeleteOrderItem($ids)
    {
        $data = Order_detail::find($ids);
        $data->delete();
        $this->emit('alert', ['type' => 'success', 'message' => 'ລຶບລາຍການສິນຄ້າ!']);
        return;
    }

    public $nas = 0;
    public function _updateQty($Id, $qty)
    {
        if ($this->nas > $qty) {
            $Qty = $this->nas;
            Cart::update($Id, $Qty);
        } else {
            $this->nas = $qty;
            Cart::update($Id, $qty);
        }
    }

    // update product amount
    public function updateStock($order_id, $product_id, $status)
    {
        if ($status == 1) {
            $order_detail = Order_detail::where('product_id', $product_id)->where('order_id', $order_id)->first();
            $order_detail->amount = $order_detail->amount + 1;
            $order_detail->save();
            $this->emit('alert', ['type' => 'success', 'message' => 'ເພີ່ມຂື້ນ+!']);
            return;
        } else {
            $order_detail = Order_detail::where('product_id', $product_id)->where('order_id', $order_id)->first();
            $order_detail->amount = $order_detail->amount - 1;
            $order_detail->save();
            $this->emit('alert', ['type' => 'error', 'message' => 'ລຶບອອກ-!']);
            return;
        }
    }

    // update order
    public function icomeOk($orderId, $status)
    {
        $importOrder = Orders::findOrFail($orderId);
        if ($status == 3) {
            if ($importOrder->status != 2) {
                $importOrder->status = 2;
                $importOrder->payment = 2;
                $importOrder->save();
                $importOrderItems = Order_detail::where('order_id', $orderId)->get();
                foreach ($importOrderItems as $item) {
                    $product = Products::findOrFail($item->product_id);
                    $product->qty = $product->qty + $item->amount;
                    $product->save();
                }
                // $ids = $orderId;
                // return redirect()->route('sale-pos-print', ['id' => $ids]);
                $this->dispatchBrowserEvent('swal:confirm', [
                    'type' => 'success',
                    'message' => 'ນຳເຂົ້າສິນຄ້າສຳເລັດ!',
                    'text' => 'ຂໍຂອບໃຈ',
                ]);
            } else {
                $this->emit('alert', ['type' => 'error', 'message' => 'ມີບາງຢ່າງຜິດພາດ!']);
            }
        } else {
            $ids = $this->orderId;
            $data = Orders::find($ids);
            $data->status = 3;
            $data->save();
            $this->dispatchBrowserEvent('hide-modal-delete');
            $this->dispatchBrowserEvent('swal', [
                'title' => 'ລາຍການສັ່ງຊື້ຖືກຍົກເລີກ!',
                'icon' => 'success',
                'iconColor' => 'green',
            ]);
        }
        $this->closeaddform();
    }
    // show form modal
    protected function showaddform()
    {
        $this->dispatchBrowserEvent('showforma');
    }
    protected function closeaddform()
    {
        $this->dispatchBrowserEvent('closeforma');
    }
}

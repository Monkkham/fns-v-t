<?php

namespace App\Http\Livewire\Backend\Componnent;

use App\Models\Products as ModelsProducts;
use App\Models\Sales;
use App\Models\Sale_detail;
use Livewire\Component;
use Livewire\WithPagination;

class SalePendings extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    public $no1 = 1, $no2 = 1;
    public $search, $onepay_image, $ID;
    public $code, $created_at, $payment, $product_id, $sales_id, $subtotal, $status, $tax, $discount;
    public $sales_detail, $customer_data, $employee_data, $sale_detail_data = [];
    public $sum_qty, $note,$origin_start,$origin_end;
    public function render()
    {
        if ($this->search == !null) {
            $salePendingsList = Sales::where('type_sales', 2)->where('code', 'like', '%' . $this->search . '%')->paginate(6);
        } else {
            $salePendingsList = Sales::where('type_sales', 2)->paginate(6);
        }
        // $product = Sale_detail::where('sales_id', $this->sales_id)->get();
        // $product = Sale_detail::select('sale_detail.*','sales.total')
        // ->join('sales','sale_detail.sales_id','=','sales.id')
        // ->where('sale_detail.sales_id', $this->sales_id)->orderBy('sale_detail.id', 'desc')->get();
        // dd($product);
        // $product = Sale_detail::where('id',$this->product_id)->get();
        // dd($product);
        return view('livewire.backend.componnent.sale-pendings', compact('salePendingsList'))->layout('layouts.backend.base');
    }
    public function ShowSending($ids)
    {
        $sales = Sales::find($ids);
        $this->ID = $sales->id;
        $this->status = $sales->status;
        $this->origin_start = $sales->origin_start;
        $this->origin_end = $sales->origin_end;
        $this->dispatchBrowserEvent('show-modal-send-product');
    }
    public function ConfirmSending()
    {
        $this->validate([
            'origin_start'=>'required',
            'origin_end'=>'required',
        ],[
            'origin_start.required'=>'ປ້ອນຂໍ້ມູນກ່ອນ!',
            'origin_end.required'=>'ປ້ອນຂໍ້ມູນກ່ອນ!',
        ]);
        $ids = $this->ID;
        $data = Sales::find($ids);
        $data->note = $this->note;
        $data->origin_start = $this->origin_start;
        $data->origin_end = $this->origin_end;
        $data->employee_id = auth()->user()->id;
        $data->status = 2;
        $data->save();
        $this->dispatchBrowserEvent('hide-modal-send-product');
        // $this->emit('alert', ['type' => 'success', 'message' => 'ລຶບຂໍ້ມູນສຳເລັດ!']);
        $this->dispatchBrowserEvent('swal', [
            'title' => 'ຍືນຍັນກຳລັງສົ່ງສິນຄ້າ!',
            'icon' => 'success',
            'iconColor' => 'green',
        ]);
    }
    public function showorder($ids)
    {
        $this->dispatchBrowserEvent('show-modal-bill');
        $data = Sales::find($ids);
        $this->ID = $data->id;
        $this->code = $data->code;
        $this->origin_start = $data->origin_start;
        $this->origin_end = $data->origin_end;
        $this->discount = $data->discount;
        $this->tax = $data->tax;
        $this->employee_data = $data->employee;
        $this->customer_data = $data->customer;
        $this->created_at = $data->created_at;
        $this->subtotal = $data->subtotal;
        $this->total = $data->total;
        $this->status = $data->status;
        $this->sum_qty = Sale_detail::where('sales_id', $this->ID)->sum('quantity');
        $this->sale_detail_data = Sale_detail::where('sales_id', $this->ID)->get();
        // $this->salId = $id;
        // go to print
    }
    public function show_onepay($ids)
    {
        $this->dispatchBrowserEvent('show-onepay');
        $data = Sales::find($ids);
        $this->ID = $data->id;
        $this->code = $data->code;
        $this->total = $data->total;
        $this->payment = $data->payment;
        $this->created_at = $data->created_at;
        $this->onepay_image = $data->onepay_image;
    }
    public function confirm_onepay()
    {
        $ids = $this->ID;
        $data = Sales::find($ids);
        $data->payment = 0;
        $data->save();
        $this->dispatchBrowserEvent('hide-onepay');
        // $this->emit('alert', ['type' => 'success', 'message' => 'ລຶບຂໍ້ມູນສຳເລັດ!']);
        $this->dispatchBrowserEvent('swal', [
            'title' => 'ຍືນຍັນ OnePay ສຳເລັດ!',
            'icon' => 'success',
            'iconColor' => 'green',
        ]);
    }
    public function showSuccess($ids)
    {
        $this->dispatchBrowserEvent('show-modal-success');
        $Data = Sales::find($ids);
        $this->ID = $Data->id;
        $this->name = $Data->name;
    }
    public function ConfirmSuccess($ids)
    {
        // $ids = $this->ID;
        $data = Sales::find($ids);
        $data->status = 3;
        $data->save();
        $this->dispatchBrowserEvent('hide-modal-success');
        // $this->emit('alert', ['type' => 'success', 'message' => 'ລຶບຂໍ້ມູນສຳເລັດ!']);
        $this->dispatchBrowserEvent('swal', [
            'title' => 'ສົ່ງເຖິງລູກຄ້າສຳເລັດ!',
            'icon' => 'success',
            'iconColor' => 'green',
        ]);
    }
    public function showCancle($ids)
    {
        $this->dispatchBrowserEvent('show-modal-cancle');
        $Data = Sales::find($ids);
        $this->ID = $Data->id;
        $this->code = $Data->code;
        $this->note = $Data->note;
    }
    public function UpdateCancle()
    {
        $ids = $this->ID;
        $data = Sales::find($ids);
        $data->note = $this->note;
        $data->status = 4;
        $data->save();
        $this->dispatchBrowserEvent('hide-modal-cancle');
        // $this->emit('alert', ['type' => 'success', 'message' => 'ລຶບຂໍ້ມູນສຳເລັດ!']);
        $this->dispatchBrowserEvent('swal', [
            'title' => 'ຍົກເລີກການສັ່ງຊື້ເເລ້ວ!',
            'icon' => 'success',
            'iconColor' => 'green',
        ]);
    }
    public function show_small_bill($ids)
    {
        $this->dispatchBrowserEvent('show-modal-small-bill');
        $data = Sales::find($ids);
        $this->ID = $data->id;
        $this->code = $data->code;
        $this->subtotal = $data->subtotal;
        $this->customer_data = $data->customer;
        $this->total = $data->total;
        $this->tax = $data->tax;
        $this->discount = $data->discount;
        $this->created_at = $data->created_at;
        $this->sales_detail = Sale_detail::select('*')->where('sales_id', $this->ID)->get();
        // dd($this->sales_detail);
    }
    // show order
    public $bill, $date, $account, $total, $qty, $salId;
    public $icomeOrderItems;

    // update sale
    public function icomeOk($salId, $status)
    {
        $icomeOrder = Sales::findOrFail($salId);
        if ($status == 3) {
            if ($icomeOrder->status != 3) {
                $icomeOrder->status = 3;
                $icomeOrder->save();
                $icomeOrderItems = Sale_detail::where('sales_id', $salId)->get();
                foreach ($icomeOrderItems as $item) {
                    $product = ModelsProducts::findOrFail($item->product_id);
                    $product->qty = $product->qty + $item->quantity;
                    $product->save();
                }
                // $id = $orderId;
                // return redirect()->route('sale-pos-print', ['id' => $id]);
            } else {
                $this->emit('alert', ['type' => 'error', 'message' => 'something wrong!']);
            }
        } else {
            $icomeOrder->delete();
            Sale_detail::where('sales_id', $salId)->delete();
            $this->emit('alert', ['type' => 'success', 'message' => 'ລາຍການສັ່ງຊື້ຖືກລົບ!']);
        }
        $this->closeaddform();
    }

    // show form modal
    protected function showaddform()
    {
        $this->dispatchBrowserEvent('showforma');
    }
    protected function closeaddform()
    {
        $this->dispatchBrowserEvent('closeforma');
    }
}

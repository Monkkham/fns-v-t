<?php

namespace App\Http\Livewire\Backend\Componnent;

use App\Models\Order_detail as ModelsOrderItem;
use App\Models\Orders as ModelsOrders;
use Livewire\Component;

class ReportIcomeProducts extends Component
{
    public $no1 = 1;
    public $status;
    public function render()
    {
        if ($this->status == 'day') {
            $reportIcomeProducts = ModelsOrders::where('status', 3)->whereDate('created_at', date('Y-m-d'))->get();
            $reportIcomeProductsItems = ModelsOrderItem::whereDate('created_at', date('Y-m-d'))->get();
        } elseif ($this->status == 'month') {
            $reportIcomeProducts = ModelsOrders::where('status', 3)->whereMonth('created_at', date('m'))->get();
            $reportIcomeProductsItems = ModelsOrderItem::whereMonth('created_at', date('m'))->get();
        } elseif ($this->status == '6month') {
            $reportIcomeProducts = ModelsOrders::where('status', 3)->whereMonth('created_at', '>=', date('m') - 6)->get();
            $reportIcomeProductsItems = ModelsOrderItem::whereMonth('created_at', '>=', date('m') - 6)->get();
        } elseif ($this->status == 'year') {
            $reportIcomeProducts = ModelsOrders::where('status', 3)->whereYear('created_at', date('Y'))->get();
            $reportIcomeProductsItems = ModelsOrderItem::whereYear('created_at', date('Y'))->get();
        } else {
            $reportIcomeProducts = ModelsOrders::where('status', 3)->get();
            $reportIcomeProductsItems = ModelsOrderItem::all();
        }
        return view('livewire.backend.componnent.report-icome-products', compact('reportIcomeProducts', 'reportIcomeProductsItems'))->layout('layouts.backend.base');
    }
}
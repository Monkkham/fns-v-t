<?php

namespace App\Http\Livewire\Backend\Componnent;

use App\Models\Sale_detail as ModalSaleItem;
use App\Models\Sales as ModelsSales;
use Livewire\Component;

class ReportSaleProducts extends Component
{
    public $no1 = 1;
    public $status;
    public function render()
    {
        if ($this->status == 'day') {
            $reportSaleProducts = ModelsSales::where('status', 3)->whereDate('created_at', date('Y-m-d'))->get();
            $reportSaleProductsItems = ModalSaleItem::whereDate('created_at', date('Y-m-d'))->get();
        } elseif ($this->status == 'month') {
            $reportSaleProducts = ModelsSales::where('status', 3)->whereMonth('created_at', date('m'))->get();
            $reportSaleProductsItems = ModalSaleItem::whereMonth('created_at', date('m'))->get();
        } elseif ($this->status == '6month') {
            $reportSaleProducts = ModelsSales::where('status', 3)->whereMonth('created_at', '>=', date('m') - 6)->get();
            $reportSaleProductsItems = ModalSaleItem::whereMonth('created_at', '>=', date('m') - 6)->get();
        } elseif ($this->status == 'year') {
            $reportSaleProducts = ModelsSales::where('status', 3)->whereYear('created_at', date('Y'))->get();
            $reportSaleProductsItems = ModalSaleItem::whereYear('created_at', date('Y'))->get();
        } else {
            $reportSaleProducts = ModelsSales::where('status', 3)->get();
            $reportSaleProductsItems = ModalSaleItem::all();
        }
        return view('livewire.backend.componnent.report-sale-products', compact('reportSaleProducts', 'reportSaleProductsItems'))->layout('layouts.backend.base');
    }
}

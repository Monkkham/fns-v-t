<?php

namespace App\Http\Livewire\Backend\Reports;

use App\Models\Sales;
use App\Models\Sale_detail;
use Livewire\Component;

class SalesReportContent extends Component
{
    public $start, $end;
    public $starts, $ends;
    public $code, $customer_id, $name, $total, $payment, $status, $created_at, $onepay_image, $search, $datetime, $qtyp;
    public $ID, $food_data = [], $table_data = [];
    public $count_food_data, $sum_subtotal_food_data, $count_table_data, $customer_data;
    public $subtotal;
    public $sales_detail, $employee_data, $sale_detail_data = [];
    public $sum_qty, $tax, $discount, $type_sales,$num_code;
    public function render()
    {
        $this->num_code = rand(10000000, 99999999);
        $start = $this->starts;
        $end = date('Y-m-d', strtotime($this->ends . ' + ' . 1 . 'days'));
        if (!empty($this->type_sales)) {
            $sales = Sales::select('*')
                ->where('status', 3)
                ->where('type_sales', $this->type_sales)
                ->whereBetween('created_at', [$this->starts, $end])->get();
            $sum_total = Sales::select('total')
                ->where('status', 3)
                ->where('type_sales', $this->type_sales)
                ->whereBetween('created_at', [$this->starts, $end])
                ->orderBy('id', 'desc')->sum('total');
        } else {
            $sales = Sales::select('*')
                ->where('status', 3)
                ->whereBetween('created_at', [$this->starts, $end])->get();
            $sum_total = Sales::select('total')
                ->where('status', 3)
                ->whereBetween('created_at', [$this->starts, $end])
                ->orderBy('id', 'desc')->sum('total');
        }
        return view('livewire.backend.reports.sales-report-content', compact('sales', 'sum_total'))->layout('layouts.backend.base');
    }
    public function ShowDetail($ids)
    {
        $this->dispatchBrowserEvent('show-modal-detail');
        $data = Sales::find($ids);
        $this->ID = $data->id;
        $this->code = $data->code;
        $this->discount = $data->discount;
        $this->tax = $data->tax;
        $this->employee_data = $data->employee;
        $this->customer_data = $data->customer;
        $this->created_at = $data->created_at;
        $this->subtotal = $data->subtotal;
        $this->total = $data->total;
        $this->status = $data->status;
        $this->sum_qty = Sale_detail::where('sales_id', $this->ID)->sum('quantity');
        $this->sale_detail_data = Sale_detail::where('sales_id', $this->ID)->get();
    }
    public function sub()
    {
        if (!empty($this->start || $this->end)) {
            $this->starts = $this->start;
            $this->ends = $this->end;
        } else {
            $this->emit('alert', ['type' => 'error', 'message' => 'ກະລຸນາເລືອກວັນທີ່ຫາວັນທີ່ກ່ອນ!']);
        }
    }
}

<?php

namespace App\Http\Livewire\Backend\Reports;

use App\Models\Orders;
use Livewire\Component;
use App\Models\Order_detail;

class OrderReportContent extends Component
{
    public $start, $end;
    public $starts, $ends,$num_code;
    public $created_at,$orderdetail_data = [],$orderdetail_sum_amount,$orderdetail_sum_subtotal;
    public $employee_data,$supplier_data,$payment,$status;
    public function render()
    {
        $this->num_code = rand(10000000, 99999999);
        $start = $this->starts;
        $end = date('Y-m-d', strtotime($this->ends . ' + ' . 1 . 'days'));
        $orders = Orders::select('*')->where('status',2)
            ->whereBetween('created_at', [$this->starts, $end])->get();
            $sum_total = Orders::select('total_money')->where('status',2)
            ->whereBetween('created_at', [$this->starts, $end])
            ->orderBy('id', 'desc')->sum('total_money');
        return view('livewire.backend.reports.order-report-content',compact('orders','sum_total'))->layout('layouts.backend.base');
    }
    public function ShowDetail($ids)
    {
        $this->dispatchBrowserEvent('show-modal-detail');
        $data = Orders::find($ids);
        $this->ID = $data->id;
        $this->employee_data = $data->employee;
        $this->supplier_data = $data->supplier;
        $this->payment = $data->payment;
        $this->status = $data->status;
        $this->created_at = $data->created_at;
        $this->orderdetail_data = Order_detail::where('order_id',$this->ID)->get();
        $this->orderdetail_sum_amount = Order_detail::where('order_id',$this->ID)->sum('amount');
        $this->orderdetail_sum_subtotal = Order_detail::where('order_id',$this->ID)->sum('subtotal');
    }
    public function sub()
    {
       if(!empty($this->start || $this->end))
       {
        $this->starts = $this->start;
        $this->ends = $this->end;
       }else{
        $this->emit('alert', ['type' => 'error', 'message' => 'ກະລຸນາເລືອກວັນທີ່ຫາວັນທີ່ກ່ອນ!']);
       }
    }
}

<?php

namespace App\Http\Livewire\Backend\Reports;

use App\Models\Orders;
use Livewire\Component;
use App\Models\NoteBook;

class ExpendReportContent extends Component
{
    public $start, $end;
    public $starts, $ends, $num_code;
    public function render()
    {
        $this->num_code = rand(10000000, 99999999);
        $start = $this->starts;
        $end = date('Y-m-d', strtotime($this->ends . ' + ' . 1 . 'days'));
        $expends = NoteBook::select('*')
            ->where('type', 2)
            ->whereBetween('created_at', [$this->starts, $end])->get();
        $sum_total = NoteBook::select('money')
            ->where('type', 2)
            ->whereBetween('created_at', [$this->starts, $end])
            ->orderBy('id', 'desc')->sum('money');
            $sum_order = Orders::select('total_money')
            ->where('status', 3)
            ->whereBetween('created_at', [$this->starts, $end])
            ->orderBy('id', 'desc')->sum('total_money');
        return view('livewire.backend.reports.expend-report-content', compact('expends', 'sum_total','sum_order'))->layout('layouts.backend.base');
    }
    public function sub()
    {
        if (!empty($this->start || $this->end)) {
            $this->starts = $this->start;
            $this->ends = $this->end;
        } else {
            $this->emit('alert', ['type' => 'error', 'message' => 'ກະລຸນາເລືອກວັນທີ່ຫາວັນທີ່ກ່ອນ!']);
        }
    }
}

<?php

namespace App\Http\Livewire\Backend\Reports;

use Livewire\Component;
use App\Models\Products;

class ProductReportContent extends Component
{
    public $start, $end;
    public $starts, $ends;
    public $stock;
    public $num_code;
    public function render()
    {
        $this->num_code = rand(10000000, 99999999);
        $start = $this->starts;
        $end = date('Y-m-d', strtotime($this->ends . ' + ' . 1 . 'days'));
        // if(!empty($this->stock)){
        //     $products = Products::select('*')
        //     ->where('qty', $this->stock)
        //     ->whereBetween('created_at', [$this->starts, $end])->get();
        // }else{
            $products = Products::select('*')
            ->whereBetween('created_at', [$this->starts, $end])->get();
        // }
        return view('livewire.backend.reports.product-report-content',compact('products'))->layout('layouts.backend.base');
    }
    public function sub()
    {
       if(!empty($this->start || $this->end))
       {
        $this->starts = $this->start;
        $this->ends = $this->end;
       }else{
        $this->emit('alert', ['type' => 'error', 'message' => 'ກະລຸນາເລືອກວັນທີ່ຫາວັນທີ່ກ່ອນ!']);
       }
    }
}

<?php

namespace App\Http\Livewire\Backend;

use App\Models\Orders;
use App\Models\Order_detail;
use Livewire\Component;

class OrderPrintContent extends Component
{
    public $search;
    public $icomeOrderItems;
    public $ID,$supplier,$supplier_data,$employee_data;
    public $orderdetail_data = [];
    public $orderdetail_sum_quantity,$orderdetail_sum_subtotal,$payment,$status,$data;
    public function mount($slug_id)
    {
        $this->data = Orders::find($slug_id);
        $this->ID = $this->data->id;
        $this->employee_data = $this->data->employee;
        $this->supplier_data = $this->data->supplier;
        $this->payment = $this->data->payment;
        $this->status = $this->data->status;
        $this->created_at = $this->data->created_at;
        $this->orderdetail_data = Order_detail::where('order_id',$this->ID)->get();
        $this->orderdetail_sum_quantity = Order_detail::where('order_id',$this->ID)->sum('amount');
        $this->orderdetail_sum_subtotal = Order_detail::where('order_id',$this->ID)->sum('subtotal');
    }
    public function render()
    {
        return view('livewire.backend.order-print-content')->layout('layouts.backend.base');
    }
}

<?php

namespace App\Http\Livewire\Backend\DataStore;

use Livewire\Component;
use Livewire\WithPagination;
use App\Models\Product_type;

class ProductTypeContent extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    public $ID, $name,$code, $search;
    public function render()
    {
        $product_type = Product_type::orderBy('id','desc')
        ->where('name','like','%' . $this->search. '%')
        ->paginate(5);
        return view('livewire.backend.data-store.product-type-content',compact('product_type'))->layout('layouts.backend.base');
    }
    public function resetform()
    {
        $this->name = '';
        $this->ID = '';
    }
    protected $rules = [
        'name'=>'required|unique:product_type'
    ];
    protected $messages = [
        'name.required'=>'ກະລຸນາປ້ອນຂໍ້ມູນກ່ອນ!',
        'name.unique'=>'ຂໍ້ມູນນີ້ມີໃນລະບົບເເລ້ວ!',
    ];
    // public function updated($propertyName)
    // {
    //     $this->validateOnly($propertyName);
    // }
    public function store()
    {
        $updateId = $this->ID;
        if($updateId > 0)
        {
            $this->validate([
                'name'=>'required'
            ],[
                'name.required'=>'ກະລຸນາປ້ອນຂໍ້ມູນກ່ອນ!',
            ]);
            $data = Product_type::find($updateId);
            $data->update([
                'name' => $this->name
                ]);
                $this->dispatchBrowserEvent('swal', [
                 'title' => 'ແກ້ໄຂຂໍ້ມູນສຳເລັດ !',
                 'icon'=>'success',
                 'iconColor'=>'green',
             ]);
             $this->resetform();
         }
         else //ເພີ່ມໃໝ່
         {
            $this->validate([
                'name'=>'required|unique:product_type'
            ],[
                'name.required'=>'ກະລຸນາປ້ອນຂໍ້ມູນກ່ອນ!',
                'name.unique'=>'ຂໍ້ມູນນີ້ມີໃນລະບົບເເລ້ວ!',
            ]);
             $employee_max = Product_type::count('id');
             $count = $employee_max + 1;
             $data = new Product_type();
                 $data->code = 'PT-00'.$count;
                 $data->name = $this->name;
                 $data->save();
             $this->dispatchBrowserEvent('swal', [
                 'title' => 'ເພີ່ມຂໍ້ມູນສຳເລັດ !',
                 'icon'=>'success',
                 'iconColor'=>'green',
             ]);
             $this->resetform();
         }
         // $this->emit('alert', ['type' => 'success', 'message' => 'ເພີ່ມຂໍ້ມູນສຳເລັດ!']);
        }
        public function edit($ids)
        {
            $data = Product_type::find($ids);
            $this->name = $data->name;
            $this->ID = $data->id;
        }
        public function showDestroy($ids)
        {
            $this->dispatchBrowserEvent('show-modal-delete');
            $data = Product_type::find($ids);
            $this->ID = $data->id;
            $this->name = $data->name;
        }

        public function destroy($ids)
        {
            $ids = $this->ID;
            $data = Product_type::find($ids);
            $data->delete();
            $this->dispatchBrowserEvent('hide-modal-delete');
            // $this->emit('alert', ['type' => 'success', 'message' => 'ລຶບຂໍ້ມູນສຳເລັດ!']);
            $this->dispatchBrowserEvent('swal', [
                    'title' => 'ລຶບຂໍ້ມູນສຳເລັດ !',
                    'icon'=>'success',
                    'iconColor'=>'green',
                    ]);
                    $this->resetform();
        }
        // public function destroy($ids)
        // {
        //     $ids = $this->ID;
        //     $product_type = Product_type::find($ids);
        //         $product_type->del = 0;
        //         $product_type->save();
        //         $this->dispatchBrowserEvent('hide-modal-delete');
        //         $this->emit('alert', ['type' => 'success', 'message' => 'ລຶບຂໍ້ມູນສຳເລັດ!']);
        //         $this->dispatchBrowserEvent('swal', [
        //             'title' => 'ລຶບຂໍ້ມູນສຳເລັດ !',
        //             'icon'=>'success',
        //             'iconColor'=>'green',
        //         ]);
        //         $this->resetdata();
        // }
}

<?php

namespace App\Http\Livewire\Backend\DataStore;

use Carbon\Carbon;
use App\Models\Roles;
use Livewire\Component;
use App\Models\Customer;
use App\Models\Villages;
use App\Models\Districts;
use App\Models\Provinces;
use Livewire\WithPagination;
use App\Models\Customer_type;
use Livewire\WithFileUploads;

class CustomerContent extends Component
{
    use WithFileUploads;
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    public $ID,$newimage,$search,$con_password, $village = [], $district = [];
    public 
    $customer_type_id,
    $roles_id,
    $village_id,
    $district_id,
    $province_id,
    $code,
    $image,
    $name,
    $lastname,
    $password,
    $phone,
    $email,
    $address,
    $gender,
    $created_at,
    $updated_at;
    public function render()
    {
        $province = Provinces::all();
        if(!empty($this->province_id)){
            $this->district = Districts::where('province_id',$this->province_id)->get();
        }
        if(!empty($this->district_id)){
            $this->village = Villages::where('district_id',$this->district_id)->get();
        }
        $customer_type = Customer_type::get();
        $roles = Roles::get();
        $customer = Customer::orderBy('id','desc')
        ->where('name','like','%' . $this->search. '%')
        ->orwhere('phone','like','%' . $this->search. '%')
        ->paginate(10);
        return view('livewire.backend.data-store.customer-content',compact('customer','customer_type','roles','province'))->layout('layouts.backend.base');
    }
    public function resetform()
    {
        $this->customer_type_id = '';
        $this->roles_id = '';
        $this->village_id = '';
        $this->district_id = '';
        $this->province_id = '';
        $this->image = '';
        $this->name = '';
        $this->lastname = '';
        $this->password = '';
        $this->phone = '';
        $this->email = '';
        $this->address = '';
        $this->gender = '';
    }
        public function create()
    {
        $this->resetform();
        $this->dispatchBrowserEvent('show-modal-add');
    }

    public function store()
    {
        $this->validate([
            'name'=>'required',
            // 'image'=>'required',
            'customer_type_id'=>'required',
            'roles_id'=>'required',
            'phone'=>'required|regex:/^[0-9]+$/i|max:8||unique:customer',
            'lastname'=>'required',
            'gender'=>'required',
            'password'=>'required',
        ],[
            'name.required'=>'ປ້ອນຂໍ້ມູນກ່ອນ!',
            // 'name.unique'=>'ຂໍ້ມູນນີ້ມີໃນລະບົບເເລ້ວ!',
            'customer_type_id.required'=>'ເລືອກຂໍ້ມູນກ່ອນ!',
            'roles_id.required'=>'ເລືອກຂໍ້ມູນກ່ອນ!',
            'phone.required'=>'ປ້ອນຂໍ້ມູນກ່ອນ!',
            'phone.regex'=>'ປ້ອນເປັນຕົວເລກທັງຫມົດ!',
            'phone.max'=>'ຕົວເລກບໍ່ເກີນ 8 ໂຕ!',
            'phone.unique'=>'ຂໍ້ມູນນີ້ມີໃນລະບົບເເລ້ວ!',
            'lastname.required'=>'ປ້ອນຂໍ້ມູນກ່ອນ!',
            // 'image.required'=>'ເລືອກຮູບພາບກ່ອນ!',
            'gender.required'=>'ເລືອກຂໍ້ມູນກ່ອນ!',
            'password.required'=>'ປ້ອນຂໍ້ມູນກ່ອນ!',
        ]);
        $data = new Customer();
        if($this->password == $this->con_password){
            //upload image
            if (!empty($this->image)) {
                $this->validate([
                    'image' => 'required|mimes:jpg,png,jpeg',
                ]);
                $imageName = Carbon::now()->timestamp . '.' . $this->image->extension();
                $this->image->storeAs('upload/customer', $imageName);
                $data->image = 'upload/customer'.'/'.$imageName;
            }else{
                $data->image = '';
            }
        $data->customer_type_id = $this->customer_type_id;
        $data->roles_id = $this->roles_id;
        $data->village_id = $this->village_id;
        $data->district_id = $this->district_id;
        $data->province_id = $this->province_id;
        $data->name = $this->name;
        $data->lastname = $this->lastname;
        $data->phone = $this->phone;
        $data->password = bcrypt($this->password);
        $data->email = $this->email;
        $data->address = $this->address;
        $data->gender = $this->gender;
        $data->save();
        $this->dispatchBrowserEvent('hide-modal-add');
        // $this->emit('alert', ['type' => 'success', 'message' => 'ບັນທຶກຂໍ້ມູນສຳເລັດ!']);
        $this->dispatchBrowserEvent('swal', [
            'title' => 'ບັນທຶກຂໍ້ມູນສຳເລັດ!',
            'icon'=>'success',
            'iconColor'=>'green',
        ]);
        $this->resetform();
    }else{
        session()->flash('no_match_password', 'ລະຫັດຜ່ານຍືນຍັນບໍ່ຕົງກັນ!');
     }
}
    public function edit($ids)
    {
        $this->dispatchBrowserEvent('show-modal-edit');
        $Data = Customer::find($ids);
        $this->ID = $Data->id;
        $this->newimage = $Data->image;
        $this->customer_type_id = $Data->customer_type_id;
        $this->roles_id = $Data->roles_id;
        $this->village_id = $Data->village_id;
        $this->district_id = $Data->district_id;
        $this->province_id = $Data->province_id;
        $this->name = $Data->name;
        $this->lastname = $Data->lastname;
        $this->phone = $Data->phone;
        $this->email = $Data->email;
        $this->address = $Data->address;
        $this->gender = $Data->gender;
    }
    public function update()
    {
        $this->validate([
            'name'=>'required',
            // 'image'=>'required',
            'phone'=>'required|regex:/^[0-9]+$/i|max:8|',
            'lastname'=>'required',
            // 'email'=>'required',
            'gender'=>'required',
            // 'customer_type_id'=>'required',
            // 'roles_id'=>'required',
            // 'password'=>'required',
        ],[
            'name.required'=>'ປ້ອນຂໍ້ມູນກ່ອນ!',
            'phone.required'=>'ປ້ອນຂໍ້ມູນກ່ອນ!',
            'phone.regex'=>'ປ້ອນເປັນຕົວເລກທັງຫມົດ!',
            'phone.max'=>'ຕົວເລກບໍ່ເກີນ 8 ໂຕ!',
            // 'phone.unique'=>'ຂໍ້ມູນນີ້ມີໃນລະບົບເເລ້ວ!',
            'lastname.required'=>'ເລືອກສະກຸນເງິນກ່ອນ!',
            // 'image.required'=>'ເລືອກຮູບພາບກ່ອນ!',
            'gender.required'=>'ເລືອກຂໍ້ມູນກ່ອນ!',
            // 'customer_type_id.required'=>'ເລືອກຂໍ້ມູນກ່ອນ!',
            // 'roles_id.required'=>'ເລືອກຂໍ້ມູນກ່ອນ!',
            // 'password.required'=>'ປ້ອນຂໍ້ມູນກ່ອນ!',
        ]);
        $ids = $this->ID;
        $data = Customer::find($ids);
        $data->customer_type_id = $this->customer_type_id;
        $data->roles_id = $this->roles_id;
        $data->village_id = $this->village_id;
        $data->district_id = $this->district_id;
        $data->province_id = $this->province_id;
        $data->name = $this->name;
        $data->lastname = $this->lastname;
        // if($this->password == $this->con_password){
        // if(!empty($this->password)){
        //     $data->password = bcrypt($this->password);
        // }
        $data->phone = $this->phone;
        $data->email = $this->email;
        $data->address = $this->address;
        $data->gender = $this->gender;
        if ($this->image) {
            $this->validate([
                'image' => 'required|mimes:png,jpg,jpeg',
            ]);
            if ($this->image) {
                $this->validate([
                    'image' => 'required|mimes:png,jpg,jpeg',
                ]);
                if ($this->image != $data->image) {
                    if (!empty($data->image)) {
                        $images = explode(",", $data->images);
                        foreach ($images as $image) {
                            unlink('' . '' . $data->image);
                        }
                        $data->delete();
                    }
                }
                $imageName = Carbon::now()->timestamp . '.' . $this->image->extension();
                $this->image->storeAs('upload/customer', $imageName);
                $data->image = 'upload/customer'.'/'.$imageName;
            }
        }
        $data->save();
        $this->dispatchBrowserEvent('hide-modal-edit');
        // $this->emit('alert', ['type' => 'success', 'message' => 'ແກ້ໄຂຂໍ້ມູນສຳເລັດ!']);
        $this->dispatchBrowserEvent('swal', [
            'title' => 'ແກ້ໄຂຂໍ້ມູນສຳເລັດ!',
            'icon'=>'success',
            'iconColor'=>'green',
        ]);
        $this->resetform();
    // }
    // else{
    //     session()->flash('no_match_password', 'ລະຫັດຜ່ານຍືນຍັນບໍ່ຕົງກັນ!');
    //  }
}
    public function showDestroy($ids)
    {
        $this->dispatchBrowserEvent('show-modal-delete');
        $Data = Customer::find($ids);
        $this->ID = $Data->id;
        $this->name = $Data->name;
    }
    public function destroy()
    {
        $ids = $this->ID;
        $data = Customer::find($ids);
        $data->delete();
        $this->dispatchBrowserEvent('hide-modal-delete');
        // $this->emit('alert', ['type' => 'success', 'message' => 'ລຶບຂໍ້ມູນສຳເລັດ!']);
        $this->dispatchBrowserEvent('swal', [
            'title' => 'ລຶບຂໍ້ມູນສຳເລັດ!',
            'icon'=>'success',
            'iconColor'=>'green',
        ]);
    }
}

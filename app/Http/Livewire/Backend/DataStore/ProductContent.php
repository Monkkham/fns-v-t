<?php

namespace App\Http\Livewire\Backend\DataStore;

use App\Models\Unit;
use Livewire\Component;
use App\Models\Products;
use App\Models\Product_type;
use Livewire\WithPagination;
use Livewire\WithFileUploads;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
class ProductContent extends Component
{
    use WithFileUploads;
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    public $search, $ID,$newimage;
    public 
    $employee_id,
    $product_type_id,
    $unit_id,
    $code, 
    $name, 
    $image, 
    $sell_price, 
    $promotion_price, 
    $qty, 
    $status, 
    $status_sell = 3, 
    $note, 
    $note1, 
    $note2, 
    $created_at, 
    $updated_at;
    public function render()
    {
        $product_type = Product_type::all();
        $units = Unit::all();
        $products = Products::orderBy('id','desc')
        ->where('code','like','%' . $this->search. '%')
        ->orwhere('name','like','%' . $this->search. '%')
        ->paginate(5);
        return view('livewire.backend.data-store.product-content',compact('products','product_type','units'))->layout('layouts.backend.base');
    }
    public function resetform()
    {
        $this->employee_id = '';
        $this->product_type_id = '';
        $this->unit_id = '';
        $this->code = '';
        $this->name = '';
        $this->image = '';
        $this->sell_price = '';
        $this->promotion_price = '';
        $this->qty = '';
        $this->status = '';
        $this->note = '';
    }
        public function create()
    {
        $this->resetform();
        $this->dispatchBrowserEvent('show-modal-add');
    }

    public function store()
    {
        $this->validate([
            'name'=>'required',
            // 'image'=>'required',
            'product_type_id'=>'required',
            'unit_id'=>'required',
            'qty'=>'required',
            'sell_price'=>'required',
            'promotion_price'=>'required',
        ],[
            'name.required'=>'ປ້ອນຂໍ້ມູນກ່ອນ!',
            'product_type_id.required'=>'ເລືອກຂໍ້ມູນກ່ອນ!',
            'unit_id.required'=>'ເລືອກຂໍ້ມູນກ່ອນ!',
            // 'image.required'=>'ເລືອກຮູບພາບກ່ອນ!',
            'qty.required'=>'ເລືອກຂໍ້ມູນກ່ອນ!',
            'sell_price.required'=>'ປ້ອນຂໍ້ມູນກ່ອນ!',
            'promotion_price.required'=>'ປ້ອນຂໍ້ມູນກ່ອນ!',
        ]);
        $product_max = Products::count('id');
        $count = $product_max + 1;
        $data = new Products();
        // if(!empty($product_max)){
            $data->code = 'PD-00'.$count;
        // }else{
        //     $data->code = 'EM0';
        // }
            //upload image
            if (!empty($this->image)) {
                $this->validate([
                    'image' => 'required|mimes:jpg,png,jpeg',
                ]);
                $imageName = Carbon::now()->timestamp . '.' . $this->image->extension();
                $this->image->storeAs('upload/product', $imageName);
                $data->image = 'upload/product'.'/'.$imageName;
            }else{
                $data->image = '';
            }
        // $data->employee_id = Auth::user()->id;
        $data->product_type_id = $this->product_type_id;
        $data->unit_id = $this->unit_id;
        $data->name = $this->name;
        $data->sell_price = $this->sell_price;
        $data->promotion_price = $this->promotion_price;
        $data->qty = $this->qty;
        $data->status = $this->status;
        $data->status_sell = $this->status_sell;
        $data->note = $this->note;
        $data->save();
        $this->dispatchBrowserEvent('hide-modal-add');
        // $this->emit('alert', ['type' => 'success', 'message' => 'ບັນທຶກຂໍ້ມູນສຳເລັດ!']);
        $this->dispatchBrowserEvent('swal', [
            'title' => 'ບັນທຶກຂໍ້ມູນສຳເລັດ!',
            'icon'=>'success',
            'iconColor'=>'green',
        ]);
        $this->resetform();
}
    public function edit($ids)
    {
        $this->dispatchBrowserEvent('show-modal-edit');

        $Data = Products::find($ids);
        $this->ID = $Data->id;
        $this->newimage = $Data->image;
        $this->product_type_id = $Data->product_type_id;
        $this->unit_id = $Data->unit_id;
        $this->name = $Data->name;
        $this->sell_price = $Data->sell_price;
        $this->promotion_price = $Data->promotion_price;
        $this->qty = $Data->qty;
        $this->status = $Data->status;
        $this->status_sell = $Data->status_sell;
        $this->note = $Data->note;
    }
    public function update()
    {
        $this->validate([
            'name'=>'required',
            // 'image'=>'required',
            'product_type_id'=>'required',
            'unit_id'=>'required',
            'qty'=>'required',
            'sell_price'=>'required',
            'promotion_price'=>'required',
        ],[
            'name.required'=>'ປ້ອນຂໍ້ມູນກ່ອນ!',
            'product_type_id.required'=>'ເລືອກຂໍ້ມູນກ່ອນ!',
            'unit_id.required'=>'ເລືອກຂໍ້ມູນກ່ອນ!',
            // 'image.required'=>'ເລືອກຮູບພາບກ່ອນ!',
            'qty.required'=>'ເລືອກຂໍ້ມູນກ່ອນ!',
            'sell_price.required'=>'ປ້ອນຂໍ້ມູນກ່ອນ!',
            'promotion_price.required'=>'ປ້ອນຂໍ້ມູນກ່ອນ!',
        ]);
        $ids = $this->ID;
        $data = Products::find($ids);
        $data->product_type_id = $this->product_type_id;
        $data->unit_id = $this->unit_id;
        $data->name = $this->name;
        $data->sell_price = $this->sell_price;
        $data->promotion_price = $this->promotion_price;
        $data->qty = $this->qty;
        $data->status = $this->status;
        $data->status_sell = $this->status_sell;
        $data->note = $this->note;
        if ($this->image) {
            $this->validate([
                'image' => 'required|mimes:png,jpg,jpeg',
            ]);
            if ($this->image) {
                $this->validate([
                    'image' => 'required|mimes:png,jpg,jpeg',
                ]);
                if ($this->image != $data->image) {
                    if (!empty($data->image)) {
                        $images = explode(",", $data->images);
                        foreach ($images as $image) {
                            unlink('' . '' . $data->image);
                        }
                        $data->delete();
                    }
                }
                $imageName = Carbon::now()->timestamp . '.' . $this->image->extension();
                $this->image->storeAs('upload/product', $imageName);
                $data->image = 'upload/product'.'/'.$imageName;
            }
        }
        $data->save();
        $this->dispatchBrowserEvent('hide-modal-edit');
        // $this->emit('alert', ['type' => 'success', 'message' => 'ແກ້ໄຂຂໍ້ມູນສຳເລັດ!']);
        $this->dispatchBrowserEvent('swal', [
            'title' => 'ແກ້ໄຂຂໍ້ມູນສຳເລັດ!',
            'icon'=>'success',
            'iconColor'=>'green',
        ]);
        $this->resetform();
    // }
    // else{
    //     session()->flash('no_match_password', 'ລະຫັດຜ່ານຍືນຍັນບໍ່ຕົງກັນ!');
    //  }
}
    public function showDestroy($ids)
    {
        $this->dispatchBrowserEvent('show-modal-delete');
        $Data = Products::find($ids);
        $this->ID = $Data->id;
        $this->name = $Data->name;
    }
    public function destroy()
    {
        $ids = $this->ID;
        $data = Products::find($ids);
        $data->delete();
        $this->dispatchBrowserEvent('hide-modal-delete');
        // $this->emit('alert', ['type' => 'success', 'message' => 'ລຶບຂໍ້ມູນສຳເລັດ!']);
        $this->dispatchBrowserEvent('swal', [
            'title' => 'ລຶບຂໍ້ມູນສຳເລັດ!',
            'icon'=>'success',
            'iconColor'=>'green',
        ]);
    }
}

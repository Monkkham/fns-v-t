<?php

namespace App\Http\Livewire\Backend\DataStore;

use App\Models\Tax;
use Livewire\Component;
use Livewire\WithPagination;

class TaxContent extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    public $ID, $percent, $name, $search;
    public function render()
    {
        $tax = Tax::orderBy('id','desc')
        ->where('name','like','%' . $this->search. '%')
        ->paginate(5);
        return view('livewire.backend.data-store.tax-content',compact('tax'))->layout('layouts.backend.base');
    }
    public function resetform()
    {
        $this->percent = '';
        $this->name = '';
        $this->ID = '';
    }
    protected $rules = [
        'name'=>'required|unique:tax',
        'percent'=>'required',
    ];
    protected $messages = [
        'name.required'=>'ກະລຸນາປ້ອນຂໍ້ມູນກ່ອນ!',
        'name.unique'=>'ຂໍ້ມູນນີ້ມີໃນລະບົບເເລ້ວ!',
        'percent.required'=>'ກະລຸນາປ້ອນຂໍ້ມູນກ່ອນ!'
    ];
    // public function updated($propertyName)
    // {
    //     $this->validateOnly($propertyName);
    // }
    public function store()
    {
        $updateId = $this->ID;
        if($updateId > 0)
        {
            $this->validate([
                // 'code'=>'required',
                'percent'=>'required',
            ],[
                // 'code.required'=>'ກະລຸນາປ້ອນຂໍ້ມູນກ່ອນ!',
                // 'code.unique'=>'ຂໍ້ມູນນີ້ມີໃນລະບົບເເລ້ວ!',
                'percent.required'=>'ກະລຸນາປ້ອນຂໍ້ມູນກ່ອນ!'
            ]);
            $data = Tax::find($updateId);
            $data->update([
                'percent' => $this->percent,
                'name' => $this->name,
                ]);
                $this->dispatchBrowserEvent('swal', [
                 'title' => 'ແກ້ໄຂຂໍ້ມູນສຳເລັດ !',
                 'icon'=>'success',
                 'iconColor'=>'green',
             ]);
             $this->resetform();
         }
         else //ເພີ່ມໃໝ່
         {
            $this->validate([
                'name'=>'required|unique:tax',
                'percent'=>'required',
            ],[
                'name.required'=>'ກະລຸນາປ້ອນຂໍ້ມູນກ່ອນ!',
                'name.unique'=>'ຂໍ້ມູນນີ້ມີໃນລະບົບເເລ້ວ!',
                'percent.required'=>'ກະລຸນາປ້ອນຂໍ້ມູນກ່ອນ!'
            ]);
             $data = new Tax();
                 $data->percent = $this->percent;
                 $data->name = $this->name;
                 $data->save();
             $this->dispatchBrowserEvent('swal', [
                 'title' => 'ເພີ່ມຂໍ້ມູນສຳເລັດ !',
                 'icon'=>'success',
                 'iconColor'=>'green',
             ]);
             $this->resetform();
         }
         // $this->emit('alert', ['type' => 'success', 'message' => 'ເພີ່ມຂໍ້ມູນສຳເລັດ!']);
        }
        public function edit($ids)
        {
            $data = Tax::find($ids);
            $this->percent = $data->percent;
            $this->name = $data->name;
            $this->ID = $data->id;
        }
        public function showDestroy($ids)
        {
            $this->dispatchBrowserEvent('show-modal-delete');
            $data = Tax::find($ids);
            $this->ID = $data->id;
            $this->name = $data->name;
        }

        public function destroy($ids)
        {
            $ids = $this->ID;
            $data = Tax::find($ids);
            $data->delete();
            $this->dispatchBrowserEvent('hide-modal-delete');
            // $this->emit('alert', ['type' => 'success', 'message' => 'ລຶບຂໍ້ມູນສຳເລັດ!']);
            $this->dispatchBrowserEvent('swal', [
                    'title' => 'ລຶບຂໍ້ມູນສຳເລັດ !',
                    'icon'=>'success',
                    'iconColor'=>'green',
                    ]);
                    $this->resetform();
        }
        // public function destroy($ids)
        // {
        //     $ids = $this->ID;
        //     $customer_type = Tax::find($ids);
        //         $customer_type->del = 0;
        //         $customer_type->save();
        //         $this->dispatchBrowserEvent('hide-modal-delete');
        //         $this->emit('alert', ['type' => 'success', 'message' => 'ລຶບຂໍ້ມູນສຳເລັດ!']);
        //         $this->dispatchBrowserEvent('swal', [
        //             'title' => 'ລຶບຂໍ້ມູນສຳເລັດ !',
        //             'icon'=>'success',
        //             'iconColor'=>'green',
        //         ]);
        //         $this->resetdata();
        // }
}

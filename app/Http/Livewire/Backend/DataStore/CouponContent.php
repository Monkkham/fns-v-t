<?php

namespace App\Http\Livewire\Backend\DataStore;

use App\Models\Coupon;
use Livewire\Component;
use Livewire\WithPagination;

class CouponContent extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    public $ID, $code, $value, $money, $type,$expire_date,$search;
    public function render()
    {
        $coupon = Coupon::orderBy('id','desc')
        ->where('code','like','%' . $this->search. '%')
        ->paginate(5);
        return view('livewire.backend.data-store.coupon-content',compact('coupon'))->layout('layouts.backend.base');
    }
    public function resetform()
    {
        $this->code = '';
        $this->type = '';
        $this->value = '';
        $this->money = '';
        $this->expire_date = '';
        $this->ID = '';
    }
    protected $rules = [
        'code'=>'required|unique:coupon',
        'value'=>'required',
        'type'=>'required',
        'expire_date'=>'required',
    ];
    protected $messages = [
        'code.required'=>'ກະລຸນາປ້ອນຂໍ້ມູນກ່ອນ!',
        'code.unique'=>'ຂໍ້ມູນນີ້ມີໃນລະບົບເເລ້ວ!',
        'value.required'=>'ກະລຸນາປ້ອນຂໍ້ມູນກ່ອນ!',
        'type.required'=>'ກະລຸນາປ້ອນຂໍ້ມູນກ່ອນ!',
        'expire_date.required'=>'ກະລຸນາປ້ອນຂໍ້ມູນກ່ອນ!',
    ];
    // public function updated($propertyName)
    // {
    //     $this->validateOnly($propertyName);
    // }
    public function store()
    {
        $updateId = $this->ID;
        if($updateId > 0)
        {
            $this->validate([
                // 'code'=>'required',
                'value'=>'required',
                'type'=>'required',
                'expire_date'=>'required',
            ],[
                // 'code.required'=>'ກະລຸນາປ້ອນຂໍ້ມູນກ່ອນ!',
                // 'code.unique'=>'ຂໍ້ມູນນີ້ມີໃນລະບົບເເລ້ວ!',
                'value.required'=>'ກະລຸນາປ້ອນຂໍ້ມູນກ່ອນ!',
                'type.required'=>'ກະລຸນາປ້ອນຂໍ້ມູນກ່ອນ!',
                'expire_date.required'=>'ກະລຸນາປ້ອນຂໍ້ມູນກ່ອນ!',
            ]);
            $data = Coupon::find($updateId);
            $data->update([
                'type' => $this->type,
                'value' => $this->value,
                'money' => $this->money,
                'expire_date' => $this->expire_date,
                ]);
                $this->dispatchBrowserEvent('swal', [
                 'title' => 'ແກ້ໄຂຂໍ້ມູນສຳເລັດ !',
                 'icon'=>'success',
                 'iconColor'=>'green',
             ]);
             $this->resetform();
         }
         else //ເພີ່ມໃໝ່
         {
            $this->validate([
                // 'code'=>'required|unique:coupon',
                'value'=>'required',
            ],[
                // 'code.required'=>'ກະລຸນາປ້ອນຂໍ້ມູນກ່ອນ!',
                // 'code.unique'=>'ຂໍ້ມູນນີ້ມີໃນລະບົບເເລ້ວ!',
                'value.required'=>'ກະລຸນາປ້ອນຂໍ້ມູນກ່ອນ!'
            ]);
             $employee_max = Coupon::count('id');
             $count = $employee_max + 1;
             $data = new Coupon();
                 $data->code = 'XYCP00'.$count;
                 $data->type = $this->type;
                 $data->value = $this->value;
                 $data->money = $this->money;
                 $data->expire_date = $this->expire_date;
                 $data->save();
             $this->dispatchBrowserEvent('swal', [
                 'title' => 'ເພີ່ມຂໍ້ມູນສຳເລັດ !',
                 'icon'=>'success',
                 'iconColor'=>'green',
             ]);
             $this->resetform();
         }
         // $this->emit('alert', ['type' => 'success', 'message' => 'ເພີ່ມຂໍ້ມູນສຳເລັດ!']);
        }
        public function edit($ids)
        {
            $data = Coupon::find($ids);
            $this->type = $data->type;
            $this->value = $data->value;
            $this->money = $data->money;
            $this->expire_date = $data->expire_date;
            $this->ID = $data->id;
        }
        public function showDestroy($ids)
        {
            $this->dispatchBrowserEvent('show-modal-delete');
            $data = Coupon::find($ids);
            $this->ID = $data->id;
            $this->code = $data->code;
        }

        public function destroy($ids)
        {
            $ids = $this->ID;
            $data = Coupon::find($ids);
            $data->delete();
            $this->dispatchBrowserEvent('hide-modal-delete');
            // $this->emit('alert', ['type' => 'success', 'message' => 'ລຶບຂໍ້ມູນສຳເລັດ!']);
            $this->dispatchBrowserEvent('swal', [
                    'title' => 'ລຶບຂໍ້ມູນສຳເລັດ !',
                    'icon'=>'success',
                    'iconColor'=>'green',
                    ]);
                    $this->resetform();
        }
        // public function destroy($ids)
        // {
        //     $ids = $this->ID;
        //     $customer_type = Coupon::find($ids);
        //         $customer_type->del = 0;
        //         $customer_type->save();
        //         $this->dispatchBrowserEvent('hide-modal-delete');
        //         $this->emit('alert', ['type' => 'success', 'message' => 'ລຶບຂໍ້ມູນສຳເລັດ!']);
        //         $this->dispatchBrowserEvent('swal', [
        //             'title' => 'ລຶບຂໍ້ມູນສຳເລັດ !',
        //             'icon'=>'success',
        //             'iconColor'=>'green',
        //         ]);
        //         $this->resetdata();
        // }
}

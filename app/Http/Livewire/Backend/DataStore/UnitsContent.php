<?php

namespace App\Http\Livewire\Backend\DataStore;

use App\Models\Unit;
use Livewire\Component;
use Livewire\WithPagination;

class UnitsContent extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    public $ID, $name, $code, $search;
    public function render()
    {
        $units = Unit::orderBy('id','desc')
        ->where('name','like','%' . $this->search. '%')
        ->paginate(5);
        return view('livewire.backend.data-store.units-content',compact('units'))->layout('layouts.backend.base');
    }
    public function resetform()
    {
        $this->name = '';
        $this->ID = '';
    }
    protected $rules = [
        'name'=>'required|unique:customer_type'
    ];
    protected $messages = [
        'name.required'=>'ກະລຸນາປ້ອນຂໍ້ມູນກ່ອນ!',
        'name.unique'=>'ຂໍ້ມູນນີ້ມີໃນລະບົບເເລ້ວ!',
    ];
    // public function updated($propertyName)
    // {
    //     $this->validateOnly($propertyName);
    // }
    public function store()
    {
        $updateId = $this->ID;
        if($updateId > 0)
        {
            $this->validate([
                'name'=>'required|unique:unit'
            ],[
                'name.required'=>'ກະລຸນາປ້ອນຂໍ້ມູນກ່ອນ!',
                'name.unique'=>'ຂໍ້ມູນນີ້ມີໃນລະບົບເເລ້ວ!',
            ]);
            $data = Unit::find($updateId);
            $data->update([
                'name' => $this->name
                ]);
                $this->dispatchBrowserEvent('swal', [
                 'title' => 'ແກ້ໄຂຂໍ້ມູນສຳເລັດ !',
                 'icon'=>'success',
                 'iconColor'=>'green',
             ]);
             $this->resetform();
         }
         else //ເພີ່ມໃໝ່
         {
            $this->validate([
                'name'=>'required|unique:unit'
            ],[
                'name.required'=>'ກະລຸນາປ້ອນຂໍ້ມູນກ່ອນ!',
                'name.unique'=>'ຂໍ້ມູນນີ້ມີໃນລະບົບເເລ້ວ!',
            ]);
             $employee_max = Unit::count('id');
             $count = $employee_max + 1;
             $data = new Unit();
                 $data->code = 'UN-00'.$count;
                 $data->name = $this->name;
                 $data->save();
             $this->dispatchBrowserEvent('swal', [
                 'title' => 'ເພີ່ມຂໍ້ມູນສຳເລັດ !',
                 'icon'=>'success',
                 'iconColor'=>'green',
             ]);
             $this->resetform();
         }
         // $this->emit('alert', ['type' => 'success', 'message' => 'ເພີ່ມຂໍ້ມູນສຳເລັດ!']);
        }
        public function edit($ids)
        {
            $data = Unit::find($ids);
            $this->name = $data->name;
            $this->ID = $data->id;
        }
        public function showDestroy($ids)
        {
            $this->dispatchBrowserEvent('show-modal-delete');
            $data = Unit::find($ids);
            $this->ID = $data->id;
            $this->name = $data->name;
        }

        public function destroy($ids)
        {
            $ids = $this->ID;
            $data = Unit::find($ids);
            $data->delete();
            $this->dispatchBrowserEvent('hide-modal-delete');
            // $this->emit('alert', ['type' => 'success', 'message' => 'ລຶບຂໍ້ມູນສຳເລັດ!']);
            $this->dispatchBrowserEvent('swal', [
                    'title' => 'ລຶບຂໍ້ມູນສຳເລັດ !',
                    'icon'=>'success',
                    'iconColor'=>'green',
                    ]);
                    $this->resetform();
        }
        // public function destroy($ids)
        // {
        //     $ids = $this->ID;
        //     $customer_type = Unit::find($ids);
        //         $customer_type->del = 0;
        //         $customer_type->save();
        //         $this->dispatchBrowserEvent('hide-modal-delete');
        //         $this->emit('alert', ['type' => 'success', 'message' => 'ລຶບຂໍ້ມູນສຳເລັດ!']);
        //         $this->dispatchBrowserEvent('swal', [
        //             'title' => 'ລຶບຂໍ້ມູນສຳເລັດ !',
        //             'icon'=>'success',
        //             'iconColor'=>'green',
        //         ]);
        //         $this->resetdata();
        // }
}

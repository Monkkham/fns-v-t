<?php

namespace App\Http\Livewire\Backend\DataStore;

use Livewire\Component;
use App\Models\Supplier;
use App\Models\Villages;
use App\Models\Districts;
use App\Models\Provinces;
use Livewire\WithPagination;
use Livewire\WithFileUploads;
use Carbon\Carbon;
class SupplierContent extends Component
{
    use WithFileUploads;
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    public $ID,$newimage,$search, $district = [],$village = [];
    public 
    $village_id,
    $district_id,
    $province_id,
    $image,
    $name,
    $code,
    $lastname,
    $phone,
    $email,
    $address,
    $gender,
    $created_at,
    $updated_at;
    public function render()
    {
        $province = Provinces::all();
        if(!empty($this->province_id)){
            $this->district = Districts::where('province_id',$this->province_id)->get();
        }
        if(!empty($this->district_id)){
            $this->village = Villages::where('district_id',$this->district_id)->get();
        }
        $supplier = Supplier::orderBy('id','desc')
        ->where('name','like','%' . $this->search. '%')
        ->orwhere('phone','like','%' . $this->search. '%')
        ->paginate(5);
        return view('livewire.backend.data-store.supplier-content',compact('supplier','province'))->layout('layouts.backend.base');
    }
    public function resetform()
    {
        $this->village_id = '';
        $this->district_id = '';
        $this->province_id = '';
        $this->image = '';
        $this->code = '';
        $this->name = '';
        $this->lastname = '';
        $this->phone = '';
        $this->email = '';
        $this->address = '';
        $this->gender = '';
    }
        public function create(){

        $this->resetform();
        $this->dispatchBrowserEvent('show-modal-add');
    }

    public function store()
    {
        $this->validate([
            'name'=>'required',
            // 'image'=>'required',
            'phone'=>'required|regex:/^[0-9]+$/i|max:8||unique:customer',
            'lastname'=>'required',
            // 'email'=>'required',
            'gender'=>'required',
        ],[
            'name.required'=>'ປ້ອນຂໍ້ມູນກ່ອນ!',
            // 'name.unique'=>'ຂໍ້ມູນນີ້ມີໃນລະບົບເເລ້ວ!',
            'phone.required'=>'ປ້ອນຂໍ້ມູນກ່ອນ!',
            'phone.regex'=>'ປ້ອນເປັນຕົວເລກທັງຫມົດ!',
            'phone.max'=>'ຕົວເລກບໍ່ເກີນ 8 ໂຕ!',
            'phone.unique'=>'ຂໍ້ມູນນີ້ມີໃນລະບົບເເລ້ວ!',
            'lastname.required'=>'ປ້ອນຂໍ້ມູນກ່ອນ!',
            // 'image.required'=>'ເລືອກຮູບພາບກ່ອນ!',
            'gender.required'=>'ເລືອກຂໍ້ມູນກ່ອນ!',
            // 'email.required'=>'ປ້ອນຂໍ້ມູນກ່ອນ!',
        ]);
        $supplier_max = Supplier::count('id');
        $count = $supplier_max + 1;
        $data = new Supplier();
        if(!empty($supplier_max)){
            $data->code = 'SP-00'.$count;
        }else{
            $data->code = 'SP00-';
        }
            //upload image
            if (!empty($this->image)) {
                $this->validate([
                    'image' => 'required|mimes:jpg,png,jpeg',
                ]);
                $imageName = Carbon::now()->timestamp . '.' . $this->image->extension();
                $this->image->storeAs('upload/supplier', $imageName);
                $data->image = 'upload/supplier'.'/'.$imageName;
            }else{
                $data->image = '';
            }
        $data->village_id = $this->village_id;
        $data->district_id = $this->district_id;
        $data->province_id = $this->province_id;
        $data->name = $this->name;
        $data->lastname = $this->lastname;
        $data->phone = $this->phone;
        $data->email = $this->email;
        $data->address = $this->address;
        $data->gender = $this->gender;
        $data->save();
        $this->dispatchBrowserEvent('hide-modal-add');
        // $this->emit('alert', ['type' => 'success', 'message' => 'ບັນທຶກຂໍ້ມູນສຳເລັດ!']);
        $this->dispatchBrowserEvent('swal', [
            'title' => 'ບັນທຶກຂໍ້ມູນສຳເລັດ!',
            'icon'=>'success',
            'iconColor'=>'green',
        ]);
        $this->resetform();
    } 
    public function edit($ids)
    {
        $this->dispatchBrowserEvent('show-modal-edit');

        $Data = Supplier::find($ids);
        $this->ID = $Data->id;
        $this->village_id = $Data->village_id;
        $this->district_id = $Data->district_id;
        $this->province_id = $Data->province_id;
        $this->newimage = $Data->image;
        $this->name = $Data->name;
        $this->lastname = $Data->lastname;
        $this->phone = $Data->phone;
        $this->email = $Data->email;
        $this->address = $Data->address;
        $this->gender = $Data->gender;
    }
    public function update()
    {
        $this->validate([
            'name'=>'required',
            // 'image'=>'required',
            'phone'=>'required|regex:/^[0-9]+$/i',
            'lastname'=>'required',
            'email'=>'required',
            'gender'=>'required',
        ],[
            'name.required'=>'ປ້ອນຂໍ້ມູນກ່ອນ!',
            'phone.required'=>'ປ້ອນຂໍ້ມູນກ່ອນ!',
            'phone.regex'=>'ປ້ອນເປັນຕົວເລກທັງຫມົດ!',
            'lastname.required'=>'ເລືອກສະກຸນເງິນກ່ອນ!',
            // 'image.required'=>'ເລືອກຮູບພາບກ່ອນ!',
            'gender.required'=>'ເລືອກຂໍ້ມູນກ່ອນ!',
            'email.required'=>'ປ້ອນຂໍ້ມູນກ່ອນ!',
        ]);
        $ids = $this->ID;
        $data = Supplier::find($ids);
        $data->village_id = $this->village_id;
        $data->district_id = $this->district_id;
        $data->province_id = $this->province_id;
        $data->name = $this->name;
        $data->lastname = $this->lastname;
        $data->phone = $this->phone;
        $data->email = $this->email;
        $data->address = $this->address;
        $data->gender = $this->gender;
        if ($this->image) {
            $this->validate([
                'image' => 'required|mimes:png,jpg,jpeg',
            ]);
            if ($this->image) {
                $this->validate([
                    'image' => 'required|mimes:png,jpg,jpeg',
                ]);
                if ($this->image != $data->image) {
                    if (!empty($data->image)) {
                        $images = explode(",", $data->images);
                        foreach ($images as $image) {
                            unlink('' . '' . $data->image);
                        }
                        $data->delete();
                    }
                }
                $imageName = Carbon::now()->timestamp . '.' . $this->image->extension();
                $this->image->storeAs('upload/supplier', $imageName);
                $data->image = 'upload/supplier'.'/'.$imageName;
            }
        }
        $data->save();
        $this->dispatchBrowserEvent('hide-modal-edit');
        // $this->emit('alert', ['type' => 'success', 'message' => 'ແກ້ໄຂຂໍ້ມູນສຳເລັດ!']);
        $this->dispatchBrowserEvent('swal', [
            'title' => 'ແກ້ໄຂຂໍ້ມູນສຳເລັດ!',
            'icon'=>'success',
            'iconColor'=>'green',
        ]);
        $this->resetform();
    }
    public function showDestroy($ids)
    {
        $this->dispatchBrowserEvent('show-modal-delete');
        $Data = Supplier::find($ids);
        $this->ID = $Data->id;
        $this->name = $Data->name;
    }
    public function destroy()
    {
        $ids = $this->ID;
        $data = Supplier::find($ids);
        $data->delete();
        $this->dispatchBrowserEvent('hide-modal-delete');
        // $this->emit('alert', ['type' => 'success', 'message' => 'ລຶບຂໍ້ມູນສຳເລັດ!']);
        $this->dispatchBrowserEvent('swal', [
            'title' => 'ລຶບຂໍ້ມູນສຳເລັດ!',
            'icon'=>'success',
            'iconColor'=>'green',
        ]);
    }
}

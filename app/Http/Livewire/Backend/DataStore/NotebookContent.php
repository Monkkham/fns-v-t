<?php

namespace App\Http\Livewire\Backend\DataStore;

use Livewire\Component;
use App\Models\NoteBook;
use Livewire\WithPagination;

class NotebookContent extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    public $ID, $name,$code,$money, $search,$type;
    public function render()
    {
        $notebooks = NoteBook::orderBy('id','desc')
        ->where('name','like','%' . $this->search. '%')
        ->paginate(5);
        return view('livewire.backend.data-store.notebook-content',compact('notebooks'))->layout('layouts.backend.base');
    }
    public function resetform()
    {
        $this->type = '';
        $this->name = '';
        $this->money = '';
        $this->ID = '';
    }
    protected $rules = [
        'type' => 'required',
        'name' => 'required',
        'money' => 'required',
    ];
    protected $messages = [
        'type.required' => 'ເລືອກຂໍ້ມູນກ່ອນ!',
        'name.required' => 'ກະລຸນາປ້ອນຂໍ້ມູນກ່ອນ!',
        'money.required' => 'ກະລຸນາປ້ອນຂໍ້ມູນກ່ອນ!',
        // 'name.unique'=>'ຂໍ້ມູນນີ້ມີໃນລະບົບເເລ້ວ!',
    ];
    // public function updated($propertyName)
    // {
    //     $this->validateOnly($propertyName);
    // }
    public function store()
    {
        $updateId = $this->ID;
        if ($updateId > 0) {
            $this->validate([
                'type' => 'required',
                'name' => 'required',
                'money' => 'required',
            ], [
                'type.required' => 'ເລືອກຂໍ້ມູນກ່ອນ!',
                'name.required' => 'ກະລຸນາປ້ອນຂໍ້ມູນກ່ອນ!',
                'money.required' => 'ກະລຸນາປ້ອນຂໍ້ມູນກ່ອນ!',
            ]);
            $data = NoteBook::find($updateId);
            $data->update([
                'type' => $this->type,
                'name' => $this->name,
                'money' => $this->money,
            ]);
            $this->dispatchBrowserEvent('swal', [
                'title' => 'ແກ້ໄຂຂໍ້ມູນສຳເລັດ !',
                'icon' => 'success',
                'iconColor' => 'green',
            ]);
            $this->resetform();
        } else //ເພີ່ມໃໝ່
        {
            $this->validate([
                'type' => 'required',
                'name' => 'required',
                'money' => 'required',
            ], [
                'type.required' => 'ເລືອກຂໍ້ມູນກ່ອນ!',
                'name.required' => 'ກະລຸນາປ້ອນຂໍ້ມູນກ່ອນ!',
                'money.required' => 'ກະລຸນາປ້ອນຂໍ້ມູນກ່ອນ!',
            ]);
            $employee_max = NoteBook::count('id');
            $count = $employee_max + 1;
            $data = new NoteBook();
            $data->code = 'NB-00' . $count;
            $data->type = $this->type;
            $data->name = $this->name;
            $data->money = $this->money;
            $data->save();
            $this->dispatchBrowserEvent('swal', [
                'title' => 'ເພີ່ມຂໍ້ມູນສຳເລັດ !',
                'icon' => 'success',
                'iconColor' => 'green',
            ]);
            $this->resetform();
        }
        // $this->emit('alert', ['type' => 'success', 'message' => 'ເພີ່ມຂໍ້ມູນສຳເລັດ!']);
    }
    public function edit($ids)
    {
        $data = NoteBook::find($ids);
        $this->type = $data->type;
        $this->name = $data->name;
        $this->money = $data->money;
        $this->ID = $data->id;
    }
    public function showDestroy($ids)
    {
        $this->dispatchBrowserEvent('show-modal-delete');
        $data = NoteBook::find($ids);
        $this->ID = $data->id;
        $this->name = $data->name;
    }

    public function destroy($ids)
    {
        $ids = $this->ID;
        $data = NoteBook::find($ids);
        $data->delete();
        $this->dispatchBrowserEvent('hide-modal-delete');
        // $this->emit('alert', ['type' => 'success', 'message' => 'ລຶບຂໍ້ມູນສຳເລັດ!']);
        $this->dispatchBrowserEvent('swal', [
            'title' => 'ລຶບຂໍ້ມູນສຳເລັດ !',
            'icon' => 'success',
            'iconColor' => 'green',
        ]);
        $this->resetform();
    }
    // public function destroy($ids)
    // {
    //     $ids = $this->ID;
    //     $product_type = NoteBook::find($ids);
    //         $product_type->del = 0;
    //         $product_type->save();
    //         $this->dispatchBrowserEvent('hide-modal-delete');
    //         $this->emit('alert', ['type' => 'success', 'message' => 'ລຶບຂໍ້ມູນສຳເລັດ!']);
    //         $this->dispatchBrowserEvent('swal', [
    //             'title' => 'ລຶບຂໍ້ມູນສຳເລັດ !',
    //             'icon'=>'success',
    //             'iconColor'=>'green',
    //         ]);
    //         $this->resetdata();
    // }
}

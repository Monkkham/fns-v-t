<?php

namespace App\Http\Livewire\Frontend;

use Livewire\Component;
use App\Models\Products;
use App\Models\Product_type;

class SearchCategory extends Component
{
    public $search, $product_type,$product_type_id;
    public function mount()
    {
        $this->product_type = 'Allproducts';
        $this->fill(request()->only('search', 'product_type', 'product_type_id'));
    }
    public function render()
    {
        $products = Products::orderBy('id', 'desc')
        ->where('product_type_id', $this->product_type_id)
            ->where('name', 'like', '%' . $this->search . '%')
            ->get();
        $product_types = Product_type::all();
        return view('livewire.frontend.search-category',compact('products','product_types'));
    }
}

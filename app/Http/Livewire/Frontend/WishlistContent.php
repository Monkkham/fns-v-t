<?php

namespace App\Http\Livewire\Frontend;

use Livewire\Component;
use Cart;
class WishlistContent extends Component
{
    public $listeners=['refreshComponent'=>'$refresh'];
    public function render()
    {
        return view('livewire.frontend.wishlist-content')->layout('layouts.front-end.base');
    }
    public function removeWishList($product_id){
        foreach(Cart::instance('wishlist')->content() as $witem){
            if($witem->id == $product_id){
                Cart::instance('wishlist')->remove($witem->rowId);
               $this->emitTo('frontend.wishlist-count-content','refreshComponent');
            }
        }
        $this->dispatchBrowserEvent('swal', [
            'title' => 'ລຶບອອກຈາກລາຍການທີ່ມັກເເລ້ວ!',
            'icon'=>'success',
            'iconColor'=>'green',
            ]);
    }
    public function deleteallwishlist(){
        Cart::instance('wishlist')->destroy();
    }
    public function moveProductFormwishListToCart($rowId){
        $item=Cart::instance('wishlist')->get($rowId);
        Cart::instance('wishlist')->remove($rowId);
        Cart::instance('cart')->add($item->id,$item->name,1,$item->price)->associate('App\Models\Products'); 
        $this->dispatchBrowserEvent('swal', [
            'title' => 'ຍ້າຍສິນຄ້າໃສ່ກະຕ່າເເລ້ວ!',
            'icon'=>'success',
            'iconColor'=>'green',
            ]);
            $this->emitTo('frontend.cart-count-content','refreshComponent');
            $this->emitTo('frontend.cart-list-content','refreshComponent');
            $this->emitTo('frontend.wishlist-count-content','refreshComponent');
       }
}

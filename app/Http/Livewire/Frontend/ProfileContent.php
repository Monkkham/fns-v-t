<?php

namespace App\Http\Livewire\Frontend;


use DB;
use App\Models\Sales;
use Livewire\Component;
use App\Models\Customer;
use App\Models\Villages;
use App\Models\Districts;
use App\Models\Provinces;
use Illuminate\Support\Facades\Auth;

class ProfileContent extends Component
{
    public 
    $customer_type_id,
    $roles_id,
    $village_id,
    $district_id,
    $province_id,
    $code,
    $image,
    $name,
    $lastname,
    $password,
    $phone,
    $email,
    $address,
    $gender,
    $created_at,
    $updated_at;
    public $village = [];
    public $district = [];
    public $old_password, $confirmpassword;
    public function mount()
    {
        $user = Customer::where('id', auth('web')->user()->id)->first();
        $this->village_id = $user->village_id;
        $this->district_id = $user->district_id;
        $this->province_id = $user->province_id;
        $this->code = $user->code;
        $this->image = $user->image;
        $this->name = $user->name;
        $this->lastname = $user->lastname;
        $this->phone = $user->phone;
        $this->email = $user->email;
        $this->address = $user->address;
        $this->gender = $user->gender;
    }
    public function render()
    {
        $province = Provinces::all();
        if(!empty($this->province_id)){
            $this->district = Districts::where('province_id',$this->province_id)->get();
        }
        if(!empty($this->district_id)){
            $this->village = Villages::where('district_id',$this->district_id)->get();
        }
        $order_history = Sales::where('customer_id', auth('web')->user()->id)->get();
        // $this->code = $order_history->code;
        return view('livewire.frontend.profile-content',compact('province','order_history'))->layout('layouts.front-end.base');
    }
    public function updateProfile()
    {
        $updateID = auth('web')->user()->id;
        // if($updateID > 0)
        // {
        //     $this->validate([
        //         'code'=>'required',
        //         'name'=>'required',
        //         'lastname'=>'required',
        //         'phone'=>'required',
        //     ],[
        //         'code.required'=>'ປ້ອນຂໍ້ມູນກ່ອນ!',
        //         'name.required'=>'ປ້ອນຂໍ້ມູນກ່ອນ!',
        //         'lastname.required'=>'ປ້ອນຂໍ້ມູນກ່ອນ!',
        //         'phone.required'=>'ປ້ອນຂໍ້ມູນກ່ອນ!',
        //     ]);
        // }
        if(!empty($this->password))
        {
           if(!empty($this->old_password)){
           $data = Customer::find(auth('web')->user()->id);
           $var = password_verify($this->old_password, $data->password);
           if($var){
            $user_data = [
                'village_id'=>$this->village_id,
                'district_id'=>$this->district_id,
                'province_id'=>$this->province_id,
                'code'=>$this->code,
                'name'=>$this->name,
                'lastname'=>$this->lastname,
                'phone'=>$this->phone,
                'email'=>$this->email,
                'address'=>$this->address,
                'gender'=>$this->gender,
                'password'=> bcrypt($this->password),
            ];
            DB::table('customer')->where('id', $updateID)->update($user_data);
            session()->flash('message', 'ແກ້ໄຂໂປຣຟາຍສຳເລັດເເລ້ວ');
            return redirect(route('frontend.profile',auth()->user()->id));
           }else{
            $this->dispatchBrowserEvent('swal', [
                'title' => 'ລະຫັດຜ່ານບໍ່ຖືກຕ້ອງ ກະລຸນາກວດຄືນ!',
                'icon'=>'error',
                'iconColor'=>'red',
            ]);
           }
        }else{
            $this->validate([
                'old_password'=>'required',
            ],[
                'old_password.required'=>'ກະລຸນາປ້ອນລະຫັດຜ່ານເກົ່າກ່ອນ',
            ]);
        }
        }else{
            if(!empty($this->old_password)){
                $data = Customer::find(auth('web')->user()->id);
                $var = password_verify($this->old_password, $data->password);
                if($var){
                 $user_data = [
                     'village_id'=>$this->village_id,
                     'district_id'=>$this->district_id,
                     'province_id'=>$this->province_id,
                     'code'=>$this->code,
                     'name'=>$this->name,
                     'lastname'=>$this->lastname,
                     'phone'=>$this->phone,
                     'email'=>$this->email,
                     'address'=>$this->address,
                     'gender'=>$this->gender,
                 ];
                 DB::table('customer')->where('id', $updateID)->update($user_data);
                 session()->flash('message', 'ແກ້ໄຂໂປຣຟາຍສຳເລັດເເລ້ວ');
                 return redirect(route('frontend.profile',auth()->user()->id));
                }else{
                    $this->dispatchBrowserEvent('swal', [
                        'title' => 'ລະຫັດຜ່ານບໍ່ຖືກຕ້ອງ ກະລຸນາກວດຄືນ!',
                        'icon'=>'error',
                        'iconColor'=>'red',
                    ]);
                }
             }else{
                 $this->validate([
                     'old_password'=>'required',
                 ],[
                     'old_password.required'=>'ປ້ອນລະຫັດຜ່ານເກົ່າກ່ອນ!',
                 ]);
             }
        }
    }
}

<?php

namespace App\Http\Livewire\Frontend;

use Livewire\Component;
use App\Models\PostPublic;
use Livewire\WithPagination;

class BlogContent extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    public function render()
    {
        $post_public = PostPublic::orderBy('id','desc')->paginate(12);
        return view('livewire.frontend.blog-content',compact('post_public'))->layout('layouts.front-end.base');
    }
}

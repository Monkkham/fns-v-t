<?php

namespace App\Http\Livewire\Frontend;

use Livewire\Component;
use App\Models\Products;
use Cart;
class ProductDetailContent extends Component
{
    public $listeners=['refreshComponent'=>'$refresh'];
    public $ID,$search,$slug_id,$image;
    public function mount($slug_id)
    {
        $this->product = Products::where('id', $this->slug_id)->first();
        $this->ID = $this->product->id;
        // $this->image = $this->product->image;
    }
    public function render()
    {
        $products = Products::where('id',$this->ID)->get();
        $product_type_food = Products::orderBy('id','desc')->where('product_type_id',1)->get(); // ປະເພດອາຫານ
        return view('livewire.frontend.product-detail-content',compact('products','product_type_food'))->layout('layouts.front-end.base');
    }
    public function ViewProductDetail($ids)
    {
        return redirect(route('frontend.product_detail',$ids));
    }
            //Qty +1
            public function increaseQty($rowId)
            {
                $product =Cart::instance('cart')->get($rowId);
                $qty = $product->qty + 1;
                Cart::instance('cart')->update($rowId,$qty);
                // return redirect()->route('cart');
                $this->emitTo('frontend.cart-count-content','refreshComponent');
                $this->emitTo('frontend.cart-list-content','refreshComponent');
            }
            //Qty -1
            public function decreaseQty($rowId)
            {
                $product =Cart::instance('cart')->get($rowId);
                $qty = $product->qty - 1;
                Cart::instance('cart')->update($rowId,$qty);
                //  return redirect()->route('cart');
                $this->emitTo('frontend.cart-count-content','refreshComponent');
                $this->emitTo('frontend.cart-list-content','refreshComponent');
            }
            // ======================= ເພີ່ມສິນຄ້າເຂົ້າກະຕ່າ ================= //
            public function addtoCart($product_id, $product_name, $product_price)
            {
                Cart::instance('cart')->add($product_id, $product_name, 1, $product_price)->associate('App\Models\Products');
                $this->dispatchBrowserEvent('swal', [
                    'title' => 'ເພີ່ມໃສ່ກະຕ່າສຳເລັດ!',
                    'icon'=>'success',
                    'iconColor'=>'green',
                    ]);
                // if(Auth::check()){
                //     if(Auth::user()->role_id ==10){
                //         return redirect()->route('customer.dailycart');
                //     }else{
                //         return redirect()->route('cart');
                //     }
                // }
                // else{
                //     return redirect()->route('cart');
                // }
                $this->emitTo('frontend.cart-count-content','refreshComponent');
                $this->emitTo('frontend.cart-list-content','refreshComponent');
            }
            // add to wishlist
            public function addToWishlist($product_id,$product_name,$product_price){
                Cart::instance('wishlist')->add($product_id,$product_name,1,$product_price)->associate('App\Models\Products');
                $this->dispatchBrowserEvent('swal', [
                    'title' => 'ເພີ່ມໃສ່ລາຍການທີ່ມັກສຳເລັດ!',
                    'icon'=>'success',
                    'iconColor'=>'green',
                    ]);
                    $this->emitTo('frontend.wishlist-count-content','refreshComponent');
           }
}

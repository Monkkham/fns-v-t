<?php

namespace App\Http\Livewire\Frontend;

use App\Models\Customer;
use App\Models\Districts;
use App\Models\Products;
use App\Models\Provinces;
use App\Models\Sales;
use App\Models\Sale_detail;
use App\Models\Villages;
use Carbon\Carbon;
use Cart;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;
use Livewire\WithFileUploads;
use DB;
class CheckoutContent extends Component
{
    use WithFileUploads;
    // use WithSweetAlert;
    public $village = [];
    public $district = [];
    public $note, $mode, $payment, $newimage, $onepay_image;
    public $tax,$address;
    public $moneyForChange;
    public function render()
    {
        $province = Provinces::all();
        if (!empty($this->province_id)) {
            $this->district = Districts::where('province_id', $this->province_id)->get();
        }
        if (!empty($this->district_id)) {
            $this->village = Villages::where('district_id', $this->district_id)->get();
        }
        return view('livewire.frontend.checkout-content', compact('province'))->layout('layouts.front-end.base');
    }
    public function mount()
    {
        if (Auth::check()) {
            $user = Customer::where('id', auth('web')->user()->id)->first();
            $this->village_id = $user->village_id;
            $this->district_id = $user->district_id;
            $this->province_id = $user->province_id;
            $this->code = $user->code;
            $this->image = $user->image;
            $this->name = $user->name;
            $this->lastname = $user->lastname;
            $this->phone = $user->phone;
            $this->email = $user->email;
            $this->address = $user->address;
            $this->gender = $user->gender;
            $this->moneyForChange = str_replace(",", "", Cart::instance('cart')->subtotal());
        }
    }
    public function PlaceOrder()
    {
        $this->validate([
            'name' => 'required',
            'phone' => 'required',
            'phone' => 'required|numeric|min:8',
            // 'email' => 'required|email',
            // 'paymentmode' => 'required',
            'village_id' => 'required',
            'district_id' => 'required',
            'province_id' => 'required',
            // 'image' => 'required|mimes:jpg,png,jpeg',
            'mode' => 'required',
        ], [
            'mode.required' => 'ເລືອກຊ່ອງທາງການຊຳລະກ່ອນ!',
        ]);
        // $sale_code = Sales::count('id');
        // $count = $sale_code + 1;
        $sales = new Sales();
        // if(!empty($sale_code)){
        // $sales->code = 'OD-00' . $count;
        $sales->code = 'OD' . rand(100000, 999999);
        $sales->note = $this->note;
        // }else{
        //     $sales->code = 'OD-00';
        // }

        $sales->customer_id = Auth::guard('web')->user()->id;
        // $sales->subtotal = intval(preg_replace('/[^\d.]/', '', Cart::instance('cart')->subtotal()));
        // $sales->total = intval(preg_replace('/[^\d.]/', '', Cart::instance('cart')->total()));
        $sales->subtotal = session()->get('checkout')['subtotal'];
        $sales->discount = session()->get('checkout')['discount'];
        $sales->tax = session()->get('checkout')['tax'];
        $sales->total = session()->get('checkout')['total'];
        $sales->status = 1;
        $sales->payment = 1;
        if ($this->mode == 'cod') {
            $sales->payment = 0;
        } elseif ($this->mode == 'onepay') {
            $sales->payment = 1;
        }
        $sales->mode = $this->mode;
        $sales->type_sales = 2;
        $sales->note = $this->note;
        $sales->address = $this->address;
        if ($this->mode == 'onepay') {
            if (!empty($this->onepay_image)) {
                $this->validate([
                    'onepay_image' => 'required|mimes:jpg,png,jpeg',
                ], [
                    'onepay_image.required' => 'ອັບໂຫລດຮູບ OnePay ກ່ອນ!',
                ]);
                $imageName = Carbon::now()->timestamp . '.' . $this->onepay_image->extension();
                $this->onepay_image->storeAs('upload/onepay', $imageName);
                $sales->onepay_image = 'upload/onepay' . '/' . $imageName;
            } else {
                $sales->onepay_image = '';
            }
        }
        $sales->save();
        foreach (Cart::instance('cart')->content() as $item) {
            $stock_product = Products::find($item->id);
            if ($stock_product->qty < $item->qty) {
                $this->dispatchBrowserEvent('swal', [
                    'title' => 'ຂໍອາໄພ! (' . $item->name . ') ໃກ້ຫມົດເເລ້ວ!',
                    'icon' => 'warning',
                    'iconColor' => 'warning',
                ]);
                return;
            } else {
                $stock_product->qty = $stock_product->qty - $item->qty;
                $stock_product->update();
            }
            $sale_detail = new Sale_detail();
            $sale_detail->sales_id = $sales->id;
            $sale_detail->product_id = $item->id;
            $sale_detail->quantity = $item->qty;
            $sale_detail->save();
        }

        session()->forget('checkout');
        Cart::instance('cart')->destroy();
        // $this->dispatchBrowserEvent('swal', [
        //     'title' => 'ສັ່ງຊື້ສິນຄ້າສຳເລັດເເລ້ວ!',
        //     'icon' => 'success',
        //     'iconColor' => 'green',
        // ]);
        $this->dispatchBrowserEvent('swal:confirm', [
            'type' => 'success',
            'message' => 'ການສັ່ງຊື້ສຳເລັດ!',
            'text' => 'ຂໍຂອບໃຈ'
        ]);
        return redirect()->route('frontend.thanks');
    }
}

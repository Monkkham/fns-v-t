<?php

namespace App\Http\Livewire\Frontend;

use App\Models\Coupon;
use Livewire\Component;

class PromotionContent extends Component
{
    public function render()
    {
        $promotion = Coupon::all();
        return view('livewire.frontend.promotion-content',compact('promotion'))->layout('layouts.front-end.base');
    }
}

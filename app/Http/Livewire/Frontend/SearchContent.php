<?php

namespace App\Http\Livewire\Frontend;

use App\Models\Products;
use Cart;
use Livewire\Component;
use Livewire\WithPagination;

class SearchContent extends Component
{
    public $cartData;
    public $search,$product_type_id;
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    public $product_type;
    public function mount()
    {
        $this->product_type = 'Allproducts';
        $this->fill(request()->only('search', 'product_type', 'product_type_id'));
    }
    public function render()
    {
        $this->cartData = Cart::content('cart');
        // dd($this->cartData);
        $product_all = Products::orderBy('id', 'desc')
        ->where('name','like','%' . $this->search . '%')
        // ->where('product_type_id',$this->product_type_id)
        ->paginate(10); // product all
        return view('livewire.frontend.search-content', compact('product_all'))->layout('layouts.front-end.base');
    }
    // ======================= ================= //
    public function ViewProductDetail($ids)
    {
        return redirect(route('frontend.product_detail', $ids));
    }
    // ======================= ເພີ່ມສິນຄ້າເຂົ້າກະຕ່າ ================= //
    public function addtoCart($product_id, $product_name, $product_price)
    {
        Cart::instance('cart')->add($product_id, $product_name, 1, $product_price)->associate('App\Models\Products');
        $this->dispatchBrowserEvent('swal', [
            'title' => 'ເພີ່ມໃສ່ກະຕ່າສຳເລັດ!',
            'icon' => 'success',
            'iconColor' => 'green',
        ]);
        // if(Auth::check()){
        //     if(Auth::user()->role_id ==10){
        //         return redirect()->route('customer.dailycart');
        //     }else{
        //         return redirect()->route('cart');
        //     }
        // }
        // else{
        //     return redirect()->route('cart');
        // }
        $this->emitTo('frontend.cart-count-content', 'refreshComponent');
        $this->emitTo('frontend.cart-list-content', 'refreshComponent');
    }
    // add to wishlist
    public function addToWishlist($product_id, $product_name, $product_price)
    {
        Cart::instance('wishlist')->add($product_id, $product_name, 1, $product_price)->associate('App\Models\Products');
        $this->dispatchBrowserEvent('swal', [
            'title' => 'ເພີ່ມໃສ່ລາຍການທີ່ມັກສຳເລັດ!',
            'icon' => 'success',
            'iconColor' => 'green',
        ]);
        $this->emitTo('frontend.wishlist-count-content', 'refreshComponent');
    }
}

<?php

namespace App\Http\Livewire\Frontend;

use Livewire\Component;
use App\Models\Employee;
use App\Models\AboutCompany;

class AboutUsContent extends Component
{
    public function render()
    {
        $about_us = AboutCompany::get();
        $employee = Employee::get();
        return view('livewire.frontend.about-us-content',compact('about_us','employee'))->layout('layouts.front-end.base');
    }
}

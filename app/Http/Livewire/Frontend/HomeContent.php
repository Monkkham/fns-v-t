<?php

namespace App\Http\Livewire\Frontend;

use DB;
use Auth;
use App\Models\Lands;
use Livewire\Component;
use App\Models\Products;
use App\Models\PostPublic;
use App\Models\SlidePhoto;
use Livewire\WithPagination;
use Gloudemans\Shoppingcart\Facades\Cart;

class HomeContent extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    // public $products;
    public $sell_price, $qty, $name;
    public $cartData;
    // public function mount()
    // {
    //     $this->products = Products::all();
    // }
    public $listeners=['refreshComponent'=>'$refresh'];
    public function render()
    {
        $this->cartData = Cart::content();
        // dd($this->cartData);
        $post_public = PostPublic::orderBy('id','desc')->get();
        $slider = SlidePhoto::orderBy('id','desc')->get();
        $product_all = Products::orderBy('id','desc')->paginate(12); // product all
        $product_type_food = Products::orderBy('id','desc')->where('product_type_id',1)->get(); // ປະເພດອາຫານ
        $product_type_use = Products::orderBy('id','desc')->where('product_type_id',2)->get(); // ປະເພດເຄື່ອງໃຊ້
        return view('livewire.frontend.home-content',compact('slider','product_all','product_type_food','product_type_use','post_public'))->layout('layouts.front-end.base');
    }
        // ======================= ================= //
        public function ViewProductDetail($ids)
        {
            return redirect(route('frontend.product_detail',$ids));
        }
        // ======================= ເພີ່ມສິນຄ້າເຂົ້າກະຕ່າ ================= //
    public function addtoCart($product_id, $product_name, $product_price)
    {
        Cart::instance('cart')->add($product_id, $product_name, 1, $product_price)->associate('App\Models\Products');
        $this->dispatchBrowserEvent('swal', [
            'title' => 'ເພີ່ມໃສ່ກະຕ່າສຳເລັດ!',
            'icon'=>'success',
            'iconColor'=>'green',
            ]);
        // if(Auth::check()){
        //     if(Auth::user()->role_id ==10){
        //         return redirect()->route('customer.dailycart');
        //     }else{
        //         return redirect()->route('cart');
        //     }
        // }
        // else{
        //     return redirect()->route('cart');
        // }
        $this->emitTo('frontend.cart-count-content','refreshComponent');
        $this->emitTo('frontend.cart-list-content','refreshComponent');
    }
    // add to wishlist
    public function addToWishlist($product_id,$product_name,$product_price){
        Cart::instance('wishlist')->add($product_id,$product_name,1,$product_price)->associate('App\Models\Products');
        $this->dispatchBrowserEvent('swal', [
            'title' => 'ເພີ່ມໃສ່ລາຍການທີ່ມັກສຳເລັດ!',
            'icon'=>'success',
            'iconColor'=>'green',
            ]);
            $this->emitTo('frontend.wishlist-count-content','refreshComponent');
   }
}

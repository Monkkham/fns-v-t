<?php

namespace App\Http\Livewire\Frontend;

use Cart;
use Carbon\Carbon;
use App\Models\Coupon;
use Livewire\Component;
use App\Models\Products;
use Illuminate\Support\Facades\Auth;

class CartContent extends Component
{
    protected $listeners=['refreshComponent'=>'$refresh'];
    public $sumqty;
    public $couponCode;
    public $discount;
    public $subtotalAfterDiscount;
    public $taxAfterDiscount;
    public $totalAfterDiscount;
    public function render()
    {
        if(session()->has('coupon')){
            if(intval(preg_replace('/[^\d.]/', '',Cart::instance('cart')->subtotal())) < session()->get('coupon')['money']){
                session()->forget('coupon');
            }else{
                $this->calculateDiscount();
            }
        }
        $this->sumqty = Cart::instance('cart')->content()->count();
        // dd($this->sumqty);
        $this->setAmountForCheckout();
        return view('livewire.frontend.cart-content')->layout('layouts.front-end.base');
    }
        //Qty +1
        public function increaseQty($rowId)
        {
            $product =Cart::instance('cart')->get($rowId);
            $stock_product = Products::find($product->id);
            if($stock_product->qty < $product->qty){
                $this->dispatchBrowserEvent('swal', [
                    'title' => 'ຂໍອາໄພ! (' . $product->name . ') ໃກ້ຫມົດເເລ້ວ!',
                    'icon'=>'warning',
                    'iconColor'=>'warning',
                    ]);
            }else{
                $qty = $product->qty + 1;
                Cart::instance('cart')->update($rowId,$qty);
                // return redirect()->route('cart');
                $this->emitTo('frontend.cart-count-content','refreshComponent');
                $this->emitTo('frontend.cart-list-content','refreshComponent');
            }
        }
        //Qty -1
        public function decreaseQty($rowId)
        {
            $product =Cart::instance('cart')->get($rowId);
            $qty = $product->qty - 1;
            Cart::instance('cart')->update($rowId,$qty);
            //  return redirect()->route('cart');
            $this->emitTo('frontend.cart-count-content','refreshComponent');
            $this->emitTo('frontend.cart-list-content','refreshComponent');
        }
// ========================================= //
        public function destroy($rowId)
        {
            $cart = Cart::instance('cart')->content()->where('rowId',$rowId);
            if($cart->isNotEmpty()){
                Cart::instance('cart')->remove($rowId);
            }
           $this->dispatchBrowserEvent('swal', [
            'title' => 'ລຶບອອກກະຕ່າເເລ້ວ!',
            'icon'=>'success',
            'iconColor'=>'green',
            ]);
            // return redirect()->route('frontend.cart');
            $this->emitTo('frontend.cart-count-content','refreshComponent');
            $this->emitTo('frontend.cart-list-content','refreshComponent');
        }
 // ========================================= //
        public function deleteallcart(){
            Cart::instance('cart')->destroy();
            $this->dispatchBrowserEvent('swal', [
                'title' => 'ລຶບອອກທັງຫມົດເເລ້ວ!',
                'icon'=>'success',
                'iconColor'=>'green',
                ]);
                $this->emitTo('frontend.cart-count-content','refreshComponent');
                $this->emitTo('frontend.cart-list-content','refreshComponent');
        }
        public function checkout(){
            if(Auth::check()){
                return redirect()->route('frontend.checkout');
            }else{
                return redirect()->route('frontend.login');
            }
        }
        // =================== Apply Coupon ====================== //
        public function ApplyCouponCode(){
            $this->validate([ 
                'couponCode' =>'required',
           ]);
            $coupon=Coupon::where('code',$this->couponCode)->first();
            if(!$coupon){
                $this->dispatchBrowserEvent('swal', [
                    'title' => 'ບໍ່ມີລະຫັດສ່ວນຫລຸດນີ້!',
                    'icon'=>'error',
                    'iconColor'=>'danger',
                    ]);
            }
            elseif(!Coupon::where('code',$this->couponCode)->where('expire_date','>=',Carbon::today())->first()){
                $this->dispatchBrowserEvent('swal', [
                    'title' => 'ລະຫັດຫມົດກຳນົດວັນທີ່ເເລ້ວ!',
                    'icon'=>'error',
                    'iconColor'=>'danger',
                    ]);
            }
            else{
              $coupon = Coupon::where('code',$this->couponCode)->where('expire_date','>=',Carbon::today())->where('money','<=',intval(preg_replace('/[^\d.]/', '',Cart::instance('cart')->subtotal())))->first();
            //   $this->money= $coupon->money;
              if(!$coupon){
                $this->dispatchBrowserEvent('swal', [
                    'title' => 'ລະຫັດນີ້ໃຊ້ບໍ່ໄດ້ລາຄາທີ່ຕ່ຳກວ່າ!',
                    'icon'=>'error',
                    'iconColor'=>'danger',
                    ]);
                        return;
                    } 
                    session()->put('coupon',[
                        'code' => $coupon->code,
                        'type' => $coupon->type,
                        'value' => $coupon->value,
                        'money' => $coupon->money,
                    ]);
                    $this->couponCode='';
           }
        }
        public function removeCoupon(){
            session()->forget('coupon');
            $this->subtotalAfterDiscount = 0;
            $this->taxAfterDiscount = 0;
            $this->totalAfterDiscount = 0;
        }
        // =================== Caculate Coupon ====================== //
        public function calculateDiscount(){
            if(session()->has('coupon')){
                $this->discount = (intval(preg_replace('/[^\d.]/', '',Cart::instance('cart')->subtotal())) * session()->get('coupon')['value'])/100;
                $this->subtotalAfterDiscount = intval(preg_replace('/[^\d.]/', '',Cart::instance('cart')->subtotal())) - $this->discount;
                $this->taxAfterDiscount = ($this->subtotalAfterDiscount * config('cart.tax'))/100;
                $this->totalAfterDiscount = $this->subtotalAfterDiscount + $this->totalAfterDiscount;
                // dd($this->taxAfterDiscount);
            }
        }
         // ========================================= //
         public function setAmountForCheckout(){
            if(!Cart::instance('cart')->count() > 0){
                session()->forget('checkout');
                return;
            }
            if(session()->has('coupon'))
            {
                session()->put('checkout', [
                    'discount' => $this->discount,
                    'subtotal' => intval(preg_replace('/[^\d.]/', '',Cart::instance('cart')->subtotal())),
                    'tax' => $this->taxAfterDiscount,
                    'total' => $this->totalAfterDiscount,
                    // 'amount_divid' => 0,
                ]);
            }
            else
            {
                session()->put('checkout', [
                    'discount' => 0,
                    'subtotal' => intval(preg_replace('/[^\d.]/', '',Cart::instance('cart')->subtotal())),
                    'tax' => intval(preg_replace('/[^\d.]/', '',Cart::instance('cart')->tax())),
                    'total' => intval(preg_replace('/[^\d.]/', '',Cart::instance('cart')->total())),
                    // 'amount_divid' => 0,
                ]);
            }
        }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Districts extends Model
{
    use HasFactory;
    protected $table = 'district';
    protected $fillable = ['id','name_la','name_en','province_id','created_at','updated_at'];

    public function province()
    {
        return $this->belongsTo('App\Models\Provinces','province_id','id');
    }
}

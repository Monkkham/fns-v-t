<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
    use HasFactory;
    protected $table = 'products';
    protected $fillable =
    [
        'id',
        'product_type_id',
        'unit_id',
        'code',
        'name',
        'image',
        'buy_price',
        'sell_price',
        'promotion_price',
        'qty',
        'status',
        'status_sell', // 1 = ໃຫມ່ 2 = ຂາຍດີ 3 = ວ່າງ
        'note',
        'created_at',
        'updated_at'
    ];
    public function product_type()
    {
        return $this->belongsTo('App\Models\Product_type', 'product_type_id', 'id');
    }
    public function units()
    {
        return $this->belongsTo('App\Models\Unit', 'unit_id', 'id');
    }
}
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order_detail extends Model
{
    use HasFactory;
    protected $table = 'order_detail';
    protected $fillable =
    [
        'id',
        'order_id',
        'product_id',
        'amount',
        'total',
        'created_at',
        'updated_at'
    ];
    public function orders()
    {
        return $this->belongsTo('App\Models\Orders', 'order_id', 'id');
    }
    public function products()
    {
        return $this->belongsTo('App\Models\Products', 'product_id', 'id');
    }
}
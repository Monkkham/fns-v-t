<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SlidePhoto extends Model
{
    use HasFactory;
    protected $table = "slide_photo";
    protected $fillable = ['id','name','image','created_at','updated_at'];
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AboutCompany extends Model
{
    protected $table = "about_company";
    protected $fillable = [
        'id',
        'name',
        'address',
        'phone',
        'email',
        'note',
        'role',
        'latitude',
        'longitude',
        'created_at',
        'updated_at'
    ];
}

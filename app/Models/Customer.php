<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\Customer as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class Customer extends Authenticatable implements MustVerifyEmail
{
    use HasApiTokens, HasFactory, Notifiable;
    protected $table = 'customer';
    protected $fillable = ['id', 'customer_type_id', 'roles_id', 'village_id', 'district_id', 'province_id', 'code', 'name', 'lastname', 'phone', 'password', 'image', 'gender', 'address', 'email', 'created_at', 'updated_at'];
    public function customer_type()
    {
        return $this->belongsTo('App\Models\Customer_type', 'customer_type_id', 'id');
    }
    public function roles()
    {
        return $this->belongsTo('App\Models\Roles', 'roles_id', 'id');
    }
    public function village()
    {
        return $this->belongsTo('App\Models\Villages', 'village_id', 'id');
    }
    public function district()
    {
        return $this->belongsTo('App\Models\Districts', 'district_id', 'id');
    }
    public function province()
    {
        return $this->belongsTo('App\Models\Provinces', 'province_id', 'id');
    }
}
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Provinces extends Model
{
    use HasFactory;
    protected $table = 'province';
    protected $fillable = ['id','name_la','name_en','created_at','updated_at'];
}

<?php

namespace App\Providers;

use Cart;
use View;
use App\Models\Roles;
use App\Models\Sales;
use App\Models\Orders;
use Livewire\Livewire;
use App\Models\Contact;
use Livewire\Component;
use App\Models\RolePermission;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\ServiceProvider;
use App\Http\Livewire\Frontend\CartContent;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public $contact_count;
    public function register()
    {

    }
    public function refress()
    {
      // return;
      dd();
    }
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
      Livewire::component('cart-content', CartContent::class);
        View::composer('*', function($view)
     {
        if (Auth::check()){
          //  $user_id_shared = Auth::user()->id;
           $roles = Roles::find(auth()->user()->roles_id);
           $rolepermissions = RolePermission::where('role_id', auth()->user()->roles_id)->orderBy('id','desc')->get();
          $contact = Contact::all();
          $contact_count = Contact::count('id');
          $neworders = Sales::orderBy('id','desc')->where('status',1)->get();
          $neworder_count = Sales::where('status',1)->count('id');
           View::share([
            'neworder_count'=>$neworder_count,
            'neworders'=>$neworders,
            'contact_count'=>$contact_count,
            'contact'=>$contact,'rolepermissions'=> $rolepermissions,'roles'=> $roles]);
            
     }
    });
  }
}

<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Contracts\Session\Session;
use App\Http\Livewire\Backend\RoleContent;
use App\Http\Livewire\Backend\LoginContent;
use App\Http\Livewire\Backend\UsersContent;
use App\Http\Livewire\Frontend\BlogContent;
use App\Http\Livewire\Frontend\CartContent;
use App\Http\Livewire\Frontend\HomeContent;
use App\Http\Livewire\Frontend\ShopContent;
use App\Http\Livewire\Backend\LogoutContent;
use App\Http\Livewire\Frontend\SearchContent;
use App\Http\Livewire\Frontend\ThanksContent;
use App\Http\Livewire\Backend\RoleEditContent;
use App\Http\Livewire\Frontend\AboutUsContent;
use App\Http\Livewire\Frontend\ContactContent;
use App\Http\Livewire\Frontend\ProfileContent;
use App\Http\Livewire\Backend\DashboardContent;
use App\Http\Livewire\Frontend\CartListContent;
use App\Http\Livewire\Frontend\CheckoutContent;
use App\Http\Livewire\Frontend\RegisterContent;
use App\Http\Livewire\Frontend\WishlistContent;
use App\Http\Livewire\Backend\OrderPrintContent;
use App\Http\Livewire\Backend\RoleCreateContent;
use App\Http\Livewire\Backend\Store\PostContent;
use App\Http\Livewire\Frontend\CartCountContent;
use App\Http\Livewire\Frontend\PromotionContent;
use App\Http\Livewire\Backend\Store\AboutContent;
use App\Http\Livewire\Backend\Store\SlideContent;
use App\Http\Livewire\Backend\DataStore\TaxContent;
use App\Http\Livewire\Frontend\ProductDetailContent;
use App\Http\Livewire\Backend\DataStore\UnitsContent;
use App\Http\Livewire\Frontend\SearchCategoryContent;
use App\Http\Livewire\Backend\DataStore\CouponContent;
use App\Http\Livewire\Backend\DataStore\ProductContent;
use App\Http\Livewire\Backend\DataStore\CustomerContent;
use App\Http\Livewire\Backend\DataStore\EmployeeContent;
use App\Http\Livewire\Backend\DataStore\NotebookContent;
use App\Http\Livewire\Backend\DataStore\SupplierContent;
use App\Http\Livewire\Backend\Reports\OrderReportContent;
use App\Http\Livewire\Backend\Reports\SalesReportContent;
use App\Http\Livewire\Backend\Reports\ExpendReportContent;
use App\Http\Livewire\Backend\Reports\IncomeReportContent;
use App\Http\Livewire\Backend\DataStore\ProductTypeContent;
use App\Http\Livewire\Backend\Reports\ProductReportContent;
use App\Http\Livewire\Backend\DataStore\CustomerTypeContent;
use App\Http\Livewire\Backend\ProfileContent as BackendProfileContent;
use App\Http\Livewire\Backend\Componnent\SalePendings as ComponnentSalePendings;
use App\Http\Livewire\Backend\Componnent\SaleProducts as ComponnentSaleProducts;
use App\Http\Livewire\Backend\Componnent\IcomeProducts as ComponnentIcomeProducts;
use App\Http\Livewire\Backend\Componnent\OrderProducts as ComponnentOrderProducts;
use App\Http\Livewire\Backend\Componnent\ReportSaleProducts as ComponnentReportSaleProducts;
use App\Http\Livewire\Backend\Componnent\ReportIcomeProducts as ComponnentReportIcomeProducts;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

// ============ Route Frontend ============ //
Route::get('/register', RegisterContent::class)->name('frontend.register');
Route::get('/login', App\Http\Livewire\Frontend\LoginContent::class)->name('frontend.login');
Route::get('/searchs', SearchContent::class)->name('frontend.search');
Route::get('/category-search', SearchCategoryContent::class)->name('frontend.category_search');
Route::get('/', HomeContent::class)->name('home');
Route::get('/blogs', BlogContent::class)->name('frontend.blog');
Route::get('/promotions', PromotionContent::class)->name('frontend.promotion');
Route::get('/about-us', AboutUsContent::class)->name('frontend.about');
Route::get('/contact-us', ContactContent::class)->name('frontend.contact');
Route::get('/cart', CartContent::class)->name('frontend.cart');
Route::get('/wishlist', WishlistContent::class)->name('frontend.wishlist');
// =========products ========== //
Route::get('/shops', ShopContent::class)->name('frontend.shop');
Route::get('/products-detail/{slug_id}', ProductDetailContent::class)->name('frontend.product_detail');
Route::middleware('auth.frontend:web')->group(function () { // Roles
    Route::get('/profiles/{id}', ProfileContent::class)->name('frontend.profile');
    Route::get('/customer-logout', [App\Http\Livewire\Frontend\LoginContent::class, 'logout'])->name('frontend.logout');
    Route::get('/checkout', CheckoutContent::class)->name('frontend.checkout');
    Route::get('/thanks', ThanksContent::class)->name('frontend.thanks');
});


// ========== Route Backend =========== //
Route::get('/login-admin', LoginContent::class)->name('login');
Route::group(['middleware' => 'auth.backend:admin'], function () { //Roles
    Route::get('/logout', [LogoutContent::class, 'logout'])->name('logout');
    Route::get('/admin/profiles/{id}', BackendProfileContent::class)->name('backend.profile');
    Route::get('/dashboard', DashboardContent::class)->name('dashboard');
    Route::get('/employees', EmployeeContent::class)->name('backend.employee');
    Route::get('/customers', CustomerContent::class)->name('backend.customer');
    Route::get('/customer-type', CustomerTypeContent::class)->name('backend.customerType');
    Route::get('/suppliers', SupplierContent::class)->name('backend.supplier');
    Route::get('/products', ProductContent::class)->name('backend.product');
    Route::get('/product-type', ProductTypeContent::class)->name('backend.productType');
    Route::get('/units', UnitsContent::class)->name('backend.Unit');
    Route::get('/coupons', CouponContent::class)->name('backend.coupon');
    Route::get('/notebooks', NotebookContent::class)->name('backend.notebook');
    Route::get('/taxs', TaxContent::class)->name('backend.tax');
    Route::get('/users', UsersContent::class)->name('backend.user');
    Route::get('/about-companys', AboutContent::class)->name('about-company');
    Route::get('/slides', SlideContent::class)->name('backend.slide');
    Route::get('/post-public', PostContent::class)->name('backend.post');
    Route::get('/print-order/{slug_id}', OrderPrintContent::class)->name('backend.printorder');
    Route::get('/admin-order-products', ComponnentOrderProducts::class)->name('backend.orderProducts');
    Route::get('/admin-income-products', ComponnentIcomeProducts::class)->name('backend.icomeProducts');
    Route::get('/admin-sale-products', ComponnentSaleProducts::class)->name('backend.saleProducts');
    Route::get('/admin-sale-pending', ComponnentSalePendings::class)->name('backend.salePendings');
    // =====================
    Route::get('/admin-report-icome-products', ComponnentReportIcomeProducts::class)->name('backend.reportIcomeProducts');
    Route::get('/admin-report-sale-products', ComponnentReportSaleProducts::class)->name('backend.reportSaleProducts');
// =================== Report ================= //
// ======================= reports ===================== //
Route::get('/report-orders', OrderReportContent::class)->name('backend.report-order');
Route::get('/report-sales', SalesReportContent::class)->name('backend.report-sale');
Route::get('/report-products', ProductReportContent::class)->name('backend.report-product');
Route::get('/report-incomes', IncomeReportContent::class)->name('backend.report-income');
Route::get('/report-expends', ExpendReportContent::class)->name('backend.report-expend');


Route::get('/roles', RoleContent::class)->name('backend.role');
Route::get('/roles/create', RoleCreateContent::class)->name('backend.create_role');
Route::get('/roles/edit/{id}', RoleEditContent::class)->name('backend.edit_role');
});